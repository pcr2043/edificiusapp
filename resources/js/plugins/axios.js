import axios from 'axios'

let token = document.head.querySelector('meta[name="csrf-token"]').content;
window.process = {
  env: {
    NODE_ENV: '',
    baseUrl: process.env.NODE_ENV === 'development' ? 'https://edificius.test/' : 'https://edificius.ch/'
  }
}

export default axios.create({
  baseURL: process.env.baseUrl,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
  }
})