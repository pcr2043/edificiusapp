export class MenuItem {
  constructor(name = '', icon = '', url = '', create = false, ajax = true) {
    this.name = name
    this.icon = icon
    this.url = url
    this.isActive = false
    this.create = create
    this.ajax = ajax

  }
}

export class Menu {
  constructor() {
    this.items = []
    this.activeItem = null
  }

  bindCommunicate() {
    this.items = [
      // new MenuItem('dashboard', ['fas', 'tachometer-alt'], '/dashboard'),
      new MenuItem('communications', ['fas', 'inbox'], '/communications', true),
      new MenuItem('firms', ['fas', 'map-marked'], '/firms', true),
      new MenuItem('contacts', ['fas', 'headset'], '/contacts', true),
      new MenuItem('edificis', ['fas', 'user-shield'], '/users', false, false),
      new MenuItem('website', ['fab', 'chrome'], '/', false, false),
      new MenuItem('logout', ['fas', 'sign-out-alt'], '/logout', false, false),
    ]
  }

  bind() {
    this.items = [
      new MenuItem('home', null, '/', true, true),
      new MenuItem('about us', null, '/about', true, true),
      new MenuItem('services', null, '/services', true, true),
      new MenuItem('properties', null, '/search', false, false),
      new MenuItem('faq', null, '/faq', true, true),
      new MenuItem('contact', null, '/contact', true, true),
      new MenuItem('login', null, '/login', false, false),
      new MenuItem('logout', null, '/logout', false, false)

    ]
  }

  bindAdmin() {
    this.items = [
      // new MenuItem('dashboard', ['fas', 'tachometer-alt'], '/dashboard', true),
      new MenuItem('users', ['fas', 'users'], '/users', true, true),
      new MenuItem('properties', ['fas', 'map-marked'], '/properties', true),
      new MenuItem('faqs', ['fas', 'question-circle'], '/faqs', true, true),
      new MenuItem('words', ['fas', 'language'], '/words', true, true),
      new MenuItem('communication', ['fas', 'headset'], '/communications', false, false),
      new MenuItem('website', ['fab', 'chrome'], '/', false, false),
      new MenuItem('logout', ['fas', 'sign-out-alt'], '/logout', false, false)
    ]
  }

  setActiveByName(name) {
    this.activeItem = this.items.find(item => item.name == name)
    this.items[this.items.indexOf(this.activeItem)].isActive = true;
  }

  getUrl(index, ref, create = false) {

    this.activeItem = this.items[index]

    if (!this.activeItem.ajax)
      window.location.href = this.activeItem.url

    let url = (create) ? `${this.activeItem.url}/create` : `${this.activeItem.url}/`
    console.log(url)
    if (url == '//')
      url = ''

    axios.get(url).then(response => {


      let DComponent = Vue.extend({
        template: response.data
      });


      let container = document.querySelector(ref).firstChild;
      console.log(container)
      let comp = new DComponent();

      comp.$parent = window.App

      comp.$mount(container)

      this.items.forEach((item, index) => {
        this.items[index].isActive = false
      });

      this.items[index].isActive = true;

      window.history.pushState(null, this.activeItem.name, this.activeItem.url)


      Bus.$emit("close-menu");
    });
  }
}
