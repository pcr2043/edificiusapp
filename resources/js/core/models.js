export class MysqlField {
  constructor() {
    this.Field = "";
    this.Type = "";
    this.Length = 255;
    this.Unsigned = 0;
    this.Key = 0;
    this.Default = null;
    this.Extra = null
    this.Nullable = null
  }
}