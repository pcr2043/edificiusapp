import axios from '../plugins/axios'

export default class Form {
  constructor(endpoint, model = null, create = false, loaded = false) {
    this.endpoint = endpoint
    this.model = model
    this.create = create
    this.loaded = loaded
    this.isSaving = false
    this.errors = []
  }

  submit(notify = true) {
    this.isSaving = true

    return new Promise((resolve, reject) => {
      axios[this.verb()](this.customEndpoint(), this.model)
        .then(response => {
          console.log("response", response)
          this.isSaving = false
          resolve(response.data)

        })
        .catch(error => {
          window.err = error
          this.isSaving = false
          if (error.response.status === 422) {
            this.setErrors(error.response.data.errors)
            if (notify) window.toastr.error('Please Check the Form')
          } else if (error.response.status === 412)
            window.toastr.error(error.response.data.msg)
          reject(error)
        })
    })
  }

  clearErrorByTarget(target) {
    const level0Name = target.name
    const level1Name = target.parentNode.name
    const level2Name = target.parentNode.parentNode.name
    const level3Name = target.parentNode.parentNode.name

    delete this.errors[level0Name]
    delete this.errors[level1Name]
    delete this.errors[level2Name]
    delete this.errors[level3Name]

    this.errors = Object.assign({}, this.errors)
  }

  clearErrors() {
    this.errors = []
  }

  clearError(key) {
    if (this.errors[key] !== undefined) delete this.errors[key]
  }
  getError(key) {
    return this.errors[key] !== undefined ? this.errors[key][0] : ''
  }
  setErrors(errors) {
    this.errors = errors
  }

  verb() {
    return this.create ? 'post' : 'put'
  }

  customEndpoint() {
    return this.create ? this.endpoint : this.endpoint + '/' + this.model.id
  }
}
