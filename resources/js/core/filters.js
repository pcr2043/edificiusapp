import { DateUtils } from '../core/utils'

Vue.filter('dateremain', value => {
  const moment = window.$nuxt.$moment
  const date = window.$nuxt.$moment(value)

  if (isNaN(date)) return

  const now = window.$nuxt.$moment()
  const duration = moment.duration(date.diff(now))

  const minutes = Math.abs(parseInt(duration.asMinutes()))
  const hours = Math.abs(parseInt(duration.asHours()))
  const days = Math.abs(parseInt(duration.asDays()))

  let suffix = ' ago'
  let prefix = ''
  if (date > now) {
    suffix = ''
    prefix = 'in '
  }
  if (days > 0 && days < 8) return `${prefix}${days} Days${suffix}`
  else if (days > 7) return DateUtils.format(date)

  if (hours > 0) return `${prefix}${hours} Hours${suffix}`

  if (minutes > 0) return `${prefix}${minutes} Minutes${suffix}`

  // if(parseInt(duration.asHours()))
})

Vue.filter('lang', function (value) {
  // eslint-disable-next-line no-undef
  return window !== undefined ? window.lang(value) : value
})
Vue.filter('capitalize', function (value) {
  console.log(window.$nuxt.$moment())
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})




Vue.filter('htmlText', function (value, size) {
  var template = document.createElement('template');

  template.innerHTML = value.trim();

  let text = template.content.textContent

  return text.substr(0, size) + '...';

})

Vue.filter('replace', function (value, reference, newValue) {
  return value.replace(reference, newValue);
})



Vue.filter('paging', function (value, table) {
  if (table.total == 0)
    return 'No results Found (0 records)'

  return value.replace("#total", table.total).replace("#start", table.from).replace("#end", table.to);
})


Vue.filter('translate', function (key) {

  return window.translate(key)
})

Vue.filter('socialDate', function (value) {

  return Utils.dateFormatTimePassed(value)
})


Vue.filter('currency', function (value, currency) {

  return window.numeral(value).format("0,0.00") + ' ' + currency
})

Vue.filter('currency2', function (value, currency) {

  return window.numeral(value).format("0,0") + ' ' + currency
})

Vue.filter('perc', function (value, places, symbol = true) {
  if (value != null)
    value = parseFloat(value).toFixed(2)

  if (symbol)
    return value + ' %';

  return value;
})


Vue.filter('decimal', function (value, places, symbol) {
  if (value != null)
    value = parseFloat(value).toFixed(2)
  else
    value = 0.00;

  return (symbol) ? value + ' ' + symbol : value;
})


Vue.filter('million', function (value, currency) {

  return window.numeral(value).format("0,0") + ' ' + currency
})

Vue.filter('substr', function (value, start, end) {
  if (value != null && value.toString().length >= end - 4)
    return value.toString().substr(start, end) + '...'

  if (value == null)
    return ''

  return value.toString()
})



Vue.filter('fixed', function (value, units) {
  return parseFloat(value).toFixed(units)
})


Vue.filter('date', function (value) {
  return moment(value).format('DD MMM YYYY  HH:mm')
})
