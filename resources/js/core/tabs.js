export class Tabs {
  constructor(items = []) {
    this.items = items
  }

  setActive(item) {
    this.setInactive()
    item.setActive()
  }

  setInactive() {
    this.items.forEach(item => {
      item.setInactive()
      console.log(item.isActive)
    })
  }
}

export class TabItem {
  constructor(name = '', isActive = false, icon = null) {
    this.name = name
    this.isActive = isActive
    this.icon = icon
  }

  setActive() {
    this.isActive = true
  }

  setInactive() {
    this.isActive = false
  }
}
