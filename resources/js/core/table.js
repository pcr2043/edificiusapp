import axios from '../plugins/axios'
import Form from './form'

export default class Table {
  constructor(endpoint, sorts = {}, setModelEvent = null, params = {}, paginateSeg = false) {
    // params
    this.endpointOriginal = endpoint
    this.endpoint = endpoint
    this.setModelEvent = setModelEvent
    // data Object
    this.paginate = {
      data: [],
      prev_page_url: null,
      next_page_url: null
    }

    this.paginateSeg = paginateSeg;

    // mode
    // 1 - list
    // 2- edit
    this.mode = 1
    this.form = { id: -1 }
    this.selectecAll = 0

    // Set sort Fields
    this.sorts = sorts

    this.indexChange = true
    this.currentIndex = null
    // window.table = this

    this.fullTrash = false

    // set the params
    this.params = params
    this.params.search = ''

    // set paging options
    this.defaultPaging = 20
    this.paging = this.defaultPaging
    this.sizes = [20, 50, 100, 200]

    this.isLoading = false

    window.tablez = this
  }

  setLoading(value) {
    this.isLoading = value
  }

  setPaging(size) {
    this.paging = size
    this.bind()
  }

  unSelect() {
    this.currentIndex = null
  }
  selectIndex(index) {
    if (!this.indexChange) return
    let nextIndex = this.currentIndex + index

    if (this.currentIndex === null && index === -1)
      nextIndex = this.paginate.data.length - 1

    if (this.currentIndex === null && index === 1) nextIndex = 0

    if (this.paginate.data.length === 1) this.currentIndex = 0

    if (nextIndex >= 0 && nextIndex < this.paginate.data.length)
      this.currentIndex = nextIndex

    if (this.paginate.data.length > 0) {
      this.unSelectRecords(false)
      this.form = this.paginate.data[this.currentIndex]
      // eslint-disable-next-line no-undef
      const model = window._.clone(this.form)

      // eslint-disable-next-line no-undef
      Bus.$emit(this.setModelEvent, model)
      this.paginate.data[this.currentIndex].selected = 1
    }
  }

  unSelectRecords(resetIndex = true) {
    if (resetIndex) this.currentIndex = null
    this.paginate.data.forEach((row, index) => {
      this.paginate.data[index].selected = 0
    })
  }
  selectByIndex(index, target = null) {
    window.t = target
    if (
      (target != null && target.hasAttribute('type')) ||
      target.tagName === 'BUTTON' ||
      target.tagName === 'path'
    )
      return

    const statusIndex = this.paginate.data[index].selected
    this.unSelectRecords()
    this.paginate.data[index].selected = statusIndex === 1 ? 0 : 1

    this.currentIndex = index
    this.form = this.paginate.data[this.currentIndex]
    // eslint-disable-next-line no-undef
    const model = window._.clone(this.form)

    // eslint-disable-next-line no-undef
    Bus.$emit(this.setModelEvent, model)
  }

  sortValue(field) {
    if (this.sorts[field] !== undefined)
      return this.sorts[field]

    return ''
  }
  sort(field, del = true) {
    if (this.sorts[field] === undefined)
      this.sorts[field] = '';


    let sort = this.sorts[field]
    console.log(`sorting: ${sort}`)
    switch (sort) {
      case '':
        sort = 'asc'
        this.sorts[field] = sort
        break
      case 'asc':
        sort = 'desc'
        this.sorts[field] = sort
        break
      case 'desc':
        this.sorts[field] = ''
        if (del === true)
          delete this.sorts[field]
        break
    }



    console.log(this.sorts)


    this.bind()
  }

  resetform() {
    this.form = { id: -1 }
  }

  key(event) {

    console.log(event)
  }

  getEndpoint() {
    if (this.endpoint.indexOf('paginate') < 0)
      return this.paginateSeg ? this.endpoint + '/paginate' : this.endpoint
    else
      return this.endpoint;
  }
  bind() {
    this.isLoading = true
    if (this.fullTrash && this.paginate.current_page > 1) {
      this.endpoint = this.paginate.prev_page_url
    }
    return new Promise((resolve, reject) => {
      axios
        .get(this.getEndpoint(), {
          params: {
            sorts: this.sorts,
            params: this.params,
            paging: this.paging,

          }
        })
        .then(response => {
          this.paginate = response.data

          if (
            this.currentIndex != null &&
            this.currentIndex < this.paginate.data.length
          )
            this.selectByIndex(this.currentIndex)

          this.isLoading = false
          resolve(this.paginate.data)
        })
        .catch(error => {
          this.isLoading = false
          console.log(error)
        })
    })
  }

  next() {
    if (this.paginate.next_page_url === null) return
    this.endpoint = this.paginate.next_page_url
    this.bind()
  }

  exportData(type) {
    const records = this.paginate.data.filter(record => {
      return record.selected
    })
    if (records.length > 0)
      window.fileManager.exportData(
        this.endpointOriginal + '/export',
        records.map(row => row.id),
        type
      )
  }

  prev() {
    if (this.paginate.prev_page_url === null) return

    this.endpoint = this.paginate.prev_page_url
    this.bind()
  }
  submit() {

    return this.form.submit().then(data => {
      if (data.result) {
        toastr.success(data.msg);
        this.form = new Form(this.endpointOriginal.replace('paginate', ''), data.model, true)
      }
      else
        toastr.error(data.msg);

    })
  }
  create() {
    this.isLoading = true
    return new Promise((resolve, reject) => {
      axios.get(this.endpointOriginal + '/create').then(response => {

        this.form = new Form(this.endpointOriginal, response.data, true)
        this.isLoading = false;
        resolve(this.form)

      }).catch(error => {
        reject(error)
      })
    });

  }

  edit(row) {

    return new Promise((resolve, reject) => {

      this.form = new Form(this.endpointOriginal.replace('/paginate', ''), row)
      resolve(this.form)
    })

  }

  remove(ids) {

    if (ids.length == 0)
      return
    this.isLoading = true
    return axios.delete(`${this.endpointOriginal}/${ids}`).then(response => {

      if (response.data.result)
        toastr.success(response.data.msg)
      else
        toastr.error(response.data.msg)

      this.bind()

    }).catch(error => {
      toastr.error(error.response.data.msg)
    })
  }


  listMode() {
    this.mode = this.mode === 1 ? 2 : 1

    if (this.mode === 1)
      // eslint-disable-next-line no-undef
      Bus.$emit(this.setModelEvent, null, null)
  }

  customMode(mode) {
    this.mode = mode
    if (this.mode === 1)
      // eslint-disable-next-line no-undef
      Bus.$emit(this.setModelEvent, null, null)
  }

  select(value) {
    this.selectecAll = value
    this.paginate.data.forEach((row, index) => {
      this.paginate.data[index].selected = this.selectecAll
    })
  }
}
