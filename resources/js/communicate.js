/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
window.Vue = require('vue');
require('./bootstrap');
require('./assets');
require('./core/filters')


// import Vue from 'vue'
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


import { Tabs, TabItem } from './core/tabs';


window.Bus = new Vue();
window.Table = require('./core/table').default;
window.Form = require('./core/form').default;
window.Tabs = Tabs;
window.TabItem = TabItem;

import { Menu } from './core/menu';

Vue.component('communication', require('./views/Communication').default);
Vue.component('companies', require('./views/Companies').default);
Vue.component('contacts', require('./views/Contacts').default);
Vue.component('navb', require('./components/Nav').default);



// Global Registation Components
Vue.component('upload', require('./components/Upload').default);
Vue.component('checkbox', require('./components/VCheckbox').default);
Vue.component('active', require('./components/Active').default);
Vue.component('countries', require('./components/Countries').default);
Vue.component('roles', require('./components/Roles').default);
Vue.component('checklist', require('./components/CheckList').default);
Vue.component('gender', require('./components/Gender').default);
Vue.component('vinput', require('./components/VInput').default);
Vue.component('navb', require('./components/Nav').default);
Vue.component('minput', require('./components/Minput').default);
Vue.component('meditor', require('./components/MEditor').default);
Vue.component('vselect', require('./components/Select').default);

window.App = new Vue({
  el: '#app',
  data() {
    return {
      menu: new Menu(),
      mode: 0
    }
  },
  methods: {
    setMode(value) {
      this.mode = value
      Bus.$emit("set-mode", value)
    }
  },
  created() {

    this.menu.bindCommunicate();
  },
  mounted() {

    Bus.$on("set-mode", (mode) => {
      console.log('seetinmode', mode)
      this.mode = mode
    });

    // GET PAGE ATTRIBUTE FOR MENU
    this.page = this.$el.getAttribute('page')

    // SET ACTIVE ITEM ON MENU USIN PAGE
    this.menu.setActiveByName(this.page)

  }
});

