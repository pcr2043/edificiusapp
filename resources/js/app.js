/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
window.$ = window.jQuery = require('jquery');

window.Vue = require('vue');
require('./bootstrap');
require('./assets');
require('./core/filters')

require('./plugins/jquery-ui/jquery-ui')


// import Vue from 'vue'
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

window.Bus = new Vue();
window.Table = require('./core/table').default;
window.Form = require('./core/form').default;

import { Menu } from './core/menu';
import { Tabs, TabItem } from './core/tabs';

window.Tabs = Tabs;
window.TabItem = TabItem;




// VIEWS

Vue.component('faq-list', require('./views/FaqList').default);
Vue.component('profile-form', require('./views/ProfileForm').default);

// COMPONENTS
Vue.component('auth', require('./components/Auth').default);
Vue.component('navb', require('./components/Nav').default);
Vue.component('carousel', require('./components/Carousel').default);
Vue.component('property', require('./components/Property').default);
Vue.component('property-v', require('./components/PropertyV').default);
Vue.component('property-two', require('./components/PropertyTwo').default);
Vue.component('property-list', require('./components/PropertyList').default);
Vue.component('search', require('./views/Search').default);
Vue.component('vinput', require('./components/VInput').default);
Vue.component('register', require('./components/Register').default);
Vue.component('contact-form', require('./views/ContactForm').default);
Vue.component('checkbox', require('./components/VCheckbox').default);
Vue.component('gender', require('./components/Gender').default);
Vue.component('active', require('./components/Active').default);
Vue.component('countries', require('./components/Countries').default);
Vue.component('roles', require('./components/Roles').default);

window.App = new Vue({
    el: '#app',
    computed: {


        navbarExclude() {
            if (this.menu.activeItem != null && this.menu.activeItem.name === 'properties'
                || this.menu.activeItem != null && this.menu.activeItem.name === 'login')
                return false;

            return true;

        }

    },
    data() {
        return {
            language: 'en',
            menu: new Menu(true),
            mode: 0,
            container: '#container',
            faqs: [],
            page: null,
            slides: [
                { image: '/images/slides/slide-1.jpg', isActive: true, text: 'Are you real state Investor ?', btn: { text: "REGISTER NOW", click: this.showRegister } },
                { image: '/images/slides/slide-2.jpg', isActive: false, text: 'Checkout our Investements ( Stores, Office, Galleries )', btn: { text: "REGISTER NOW", click: this.showRegister } },
                { image: '/images/slides/slide-3.jpg', isActive: false, text: 'Register and get Access to all analytics  for each investement for FREE', btn: { text: "REGISTER NOW", click: this.showRegister } }]

        }
    },
    methods: {
        showRegister() {
            window.showRegister()
        },
        setMode(value) {
            this.mode = value
            Bus.$emit("set-mode", value)
        }
        ,
        setFaq(index) {
            this.faqs.forEach((faq, index) => {
                this.faqs[index].selected = false
            })

            this.faqs[index].selected = true;
        },
        setFaqs() {
            this.faqs = JSON.parse(this.$el.getAttribute('faqs'))
            if (this.faqs.length > 0)
                this.faqs[0].selected = true;
        }
    },
    created() {
        this.menu.bind();
    },
    mounted() {

        let local = this;

        Bus.$on("set-mode", (mode) => {
            console.log('seetinmode', mode)
            this.mode = mode
        });
        window.app = this;

        // GET PAGE ATTRIBUTE FOR MENU

        this.page = this.$el.getAttribute('page')

        // SET ACTIVE ITEM ON MENU USIN PAGE
        if (this.page !== '-1')
            this.menu.setActiveByName(this.page)


        // GET LANGUAGE ATTRIBUTES
        this.language = this.$el.getAttribute('language')

        // GET LANGUAGE ATTRIBUTES
        this.defaultLanguage = this.$el.getAttribute('default-language')

        //Share Languages
        window.language = this.language;
        window.defaultLanguage = this.defaultLanguage;


    }
});

