window._ = require('lodash');
// window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    // window.$ = window.jQuery = require('jquery');

    // require('bootstrap');
} catch (e) { }

window.numeral = require('numeral')

window.moment = require('moment');

window.toastr = require('toastr');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
import Axios from "./plugins/axios.js"
window.axios = Axios;



toastr.options = {
    debug: true,
    positionClass: 'toast-top-right'
    // "onclick": null,
    // "fadeIn": 300,
    // "fadeOut": 1000,
    //  "timeOut": 15000,
    //  "extendedTimeOut": 11000
    // hideMethod: function() {
    //  // return false
    // }
}

// toastr extended functions to create  confirm toast with buttons

window.removeToast = el => {
    el.parentElement.parentElement.parentElement.remove()
}

window.confirmToast = (el, evenFN) => {
    // eslint-disable-next-line no-eval
    eval(evenFN)
    el.parentElement.parentElement.parentElement.remove()
}

// toastr.info('INFO')
// toastr.warning('WARNING')
// toastr.error('ERROR')

toastr.confirm = (
    message,
    confirmCallBack = '',
    cancelCallBack = 'removeToast(this)',
    btnConfirmText = 'CONFIRM',
    btnCancelText = 'CANCEL'
) => {
    console.log(confirmCallBack)
    const template = `<div>${message}</div>
                  <div class="mt-1">
                    <button onclick="${cancelCallBack}" class="button is-dark is-small is-rounded">${btnCancelText}</button>
                    <button onclick="confirmToast(this,${confirmCallBack})" class="button is-primary is-small is-rounded">${btnConfirmText}</button>
                  </div>`

    const toast = toastr.warning(template, {
        type: 'confirm',
        iconClass: 'iconClass'
    })
    toast.addClass('confirm')
}


// Dictionary and util functions  to be shared alown the site for frontend vue and javascript
window.dictionary = [];
window.language = null;


window.translate = function (key) {
    key = key.toLowerCase();
    let value = window.dictionary[key][window.language];

    if (value == null)
        value = window.dictionary[key][window.defaultLocale];

    if (value == null)
        value = 'No key value for ' + key;

    return value;
}

// File manager Object with functions to download and to
window.fileManager = {
    download(entity, id, field, index) {
        const url = window.process.env.baseUrl
        console.log(entity, id, field, index)
        window.open(`/storage/${entity}/${id}/${field}/${index}`, '_blank')
    },
    exportData(endpoint, records, type) {
        const url = window.process.env.baseUrl
        const query = records.map(row => row.id).join(',')
        window.open(`${url}/${endpoint}?ids=${query}&type=${type}`)
    }
}





// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
