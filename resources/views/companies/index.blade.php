@if(request()->ajax() === false)
@extends('communicate.master')
@section('content')
@endif
<companies inline-template :mode-data="{{ $mode }}">
  <div class="columns is-multiple">
    <div class="column" :class="{ 
      'is-full' : mode === 0 || mode === -1, 
      'is-two-thirds' : mode === 1,
      'is-half' :mode === 2,
      'is-one-third' : mode === 3,
      'is-hidden' : mode === 4
    }">
      <div class="panel">
        {{-- <div class="tabs is-centered">
          <ul>
            <li class="is-active">Communications</li>
          </ul>
        </div> --}}
        @include('companies.table')
      </div>
    </div>


    <div class="column" :class="{ 
      'is-hidden' : mode === 0, 
      'is-one-third' : mode === 1,
      'is-half' : mode === 2,
      'is-two-thirds' : mode === 3,
      'is-full' : mode === 4,
    }">
      <div class="panel" v-show="mode > 0">
        @include('companies.edit')
      </div>
    </div>
  </div>
</companies>
@if(request()->ajax() === false)
@endsection
@endif