<section>
  {{-- <div v-if="table.form.isLoading && table.form.model != null"> --}}
  <div v-if="!table.form.isLoading && table.form.model != null">
    <form @submit.prevent="submit()" @keypress="table.form.clearError($event.target.name)">

      <div class="tabs is-centered">
        <ul>
          <li v-for="(tab, index) in tabs.items" :key="index" :class="{ 'is-active': tab.isActive }"
            @click="tabs.setActive(tab)">
            <a v-text="tab.name"></a>
          </li>
        </ul>
      </div>


      <!-- TABS    @click="table.form.clearErrorByTarget($event.target)" -->
      <div v-if="tabs.items[0].isActive" class="columns is-multiline">
        <div class="column is-three-quarters">
          <vinput v-model="table.form.model.name" label="Name" name="name" placeholder="Enter the Name"
            :error="table.form.getError('name')" />
        </div>

        <div class="column is-one-quarter">
          <active v-model="table.form.model.isActive" label="Active" name="active"
            :error="table.form.getError('isActive')" />
        </div>


        <div class="column is-three-quarters">
          <vinput v-model="table.form.model.status" label="Status" name="status" placeholder="Enter the Status"
            :error="table.form.getError('status')" />
        </div>

        <div class="column is-one-quarter">
          <active v-model="table.form.model.meta.isOpen" active-text="OPEN" inactive-text="CLOSE" label="Is Open"
            name="active" :error="table.form.getError('meta.isOpen')" />
        </div>

        <div class="column is-full">
          <vinput :multiline="true" v-model="table.form.model.meta.comments" label="Comments" name="meta.comments"
            placeholder="Enter Comments" :error="table.form.getError('meta.comments')" />
        </div>

      </div>

      <div v-if="tabs.items[0].isActive" class="columns is-multiline">

        <div class="column is-half">
          <vinput v-model="table.form.model.meta.mobile" label="Mobile" name="meta.mobile"
            placeholder="Enter Mobile ..." :error="table.form.getError('meta.mobile')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.meta.phone" label="Phone" name="meta.phone" placeholder="Enter Phone ..."
            :error="table.form.getError('meta.phone')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.meta.fax" label="Fax" name="meta.fax" placeholder="Enter Fax ..."
            :error="table.form.getError('meta.fax')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.meta.website" label="Website" name="meta.website"
            placeholder="Enter Website ..." :error="table.form.getError('meta.website')" />
        </div>

        <div class="column is-two-thirds">
          <vinput v-model="table.form.model.meta.address.street" label="Street, place" name="meta.address.street"
            placeholder="Enter Addresss, Place ..." :error="table.form.getError('meta.address.street')" />
        </div>
        <div class="column is-one-third">
          <vinput v-model="table.form.model.meta.address.number" label="Number, floor" name="meta.address.number"
            placeholder="Enter Number, Floor ..." :error="table.form.getError('meta.address.number')" />
        </div>
        <div class="column is-half">
          <countries v-model="table.form.model.meta.address.country" label="Country" name="meta.address.country"
            placeholder="Pick Country" :error="table.form.getError('meta.address.country')" />
        </div>
        <div class="column is-one-quarter">
          <vinput v-model="table.form.model.meta.address.postcode" label="Postcode" name="meta.address.postcode"
            placeholder="Enter Postcode" :error="table.form.getError('meta.address.postcode')" />
        </div>
        <div class="column is-one-quarter">
          <vinput v-model="table.form.model.meta.address.town" label="Town" name="meta.address.town"
            placeholder="Enter Town" :error="table.form.getError('meta.address.town')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.meta.email" label="Company Email | Second Email" name="meta.email2"
            placeholder="Enter Email ..." :error="table.form.getError('meta.email')" />
        </div>
      </div>


      <div v-if="tabs.items[1].isActive" class="columns is-multiline">

        <div class="column is-full">
          <div class="field">
            <label class="label is-small">Contacts</label>

            <div class="control">
              <div class="list is-hoverable is-small is-files">
                <a v-for="(contact, index) in table.form.model.contacts" :key="index" class="list-item is-small">
                  <span class="w-30" v-text="contact.name"></span>
                  <span class="ml-3" v-text="contact.meta.phone"></span>
                  <span class="tag is-info is-small ir-rounded ml-3" v-text="table.form.model.name"></span>
                  <span @click="removeContact(contact, index)" class="icon has-background-danger has-text-white">
                    <icon :icon="['fas', 'trash']" />


                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div v-if="tabs.items[2].isActive" class="columns is-multiline">

        <!-- UPLOAD CONTROL -->
        <div class="column is-full">
          <upload v-model="table.form.model.documents" label="Click button to upload files" name="documents"
            event="user-document-upload" :extensions="['doc', 'docx', 'pdf', 'xls', 'xlsx']"
            placeholder="Upload Documents" :error="table.form.getError('meta.documents')" />
        </div>

        <!-- UPLOAD LIST -->
        <div class="column is-full">
          <div class="field">
            <label class="label is-small">Documents</label>

            <div class="control">
              <div class="list is-hoverable is-small is-files">
                <a v-for="(file, index) in table.form.model.documents" :key="index" class="list-item is-small">
                  @{{ file.name }}
                  <span class="icon has-background-primary has-text-white">
                    <icon :icon="['fas', 'cloud-download-alt']" @click="
                                    fileManager.download(
                                      'companies',
                                      table.form.model.id,
                                      'documents',
                                      index
                                    )
                                  " />
                  </span>
                  <span class="icon has-background-danger has-text-white"
                    @click="table.form.model.documents.splice(index, 1)">
                    <icon :icon="['fas', 'trash']" />
                  </span>
                </a>
              </div>
            </div>

          </div>
        </div>
      </div>

      <div class="column is-full">
        <div class="field">
          <button :disabled="table.form.isSaving" type="submit"
            class="button is-primary is-small is-rounded is-pulled-right">
            <span v-if="table.form.isSaving" class="icon is-loading is-small"></span>
            SAVE
          </button>
          <button type="button" class="button is-danger is-small is-rounded is-pulled-right mr-half" @click="cancel()">
            CANCEL
          </button>
        </div>
      </div>

    </form>
  </div>
</section>