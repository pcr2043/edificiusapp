<table class="table is-fullwidth" @keypress="table.key($event)">
  <thead>
    <tr>
      <td colspan="5">
        <button class="button is-info is-small is-rounded ml-5px" @click="create()">
          <icon :icon="['fas', 'plus']" />

        </button>

        <button v-if="trash.length > 0" class="button is-danger is-small is-rounded"
          @click="toastr.confirm('ARE YOU SURE ABOUT DELETING THIS COMPANIES',`Bus.$emit('on-companies-delete')`)">
          <icon :icon="['fas', 'trash']" />

        </button>

        <a v-if="table.isLoading" class="is-loading"></a>
      </td>
      <td colspan="4" v-show="mode !== 3">
        <vinput v-model="table.params.search" :event="search" name="search" placeholder="Type something" />
      </td>
    </tr>
    <tr class="no-select">
      <th class="w-5">
        <span class="start">
          <Checkbox v-model="table.selectAll" @click="table.select($event)" />
        </span>
      </th>
      <th class="w-10" v-show="mode < 1">
        <span class="sort start" :class="table.sortValue('id')" @click="table.sort('id')">Id</span>
      </th>
      <th class="w-25">
        <span class="sort" :class="table.sortValue('name')" @click="table.sort('name')">Name</span>
      </th>
      <th v-show="mode !== 3" class="w-10">
        <span class="sort" :class="table.sortValue('postcode')" @click="table.sort('postcode')">Postcode</span>
      </th>
      <th class="w-10">
        <span class="sort" :class="table.sortValue('town')" @click="table.sort('town')">Town</span>
      </th>
      <th v-show="mode !== 3" class="w-10">
        <span class="sort" :class="table.sortValue('status')" @click="table.sort('status')">Status</span>
      </th>
      <th v-show="mode !== 3" class="w-10">
        <span class="sort" :class="table.sortValue('open')" @click="table.sort('open')">Is Open</span>
      </th>
      <th class="w-20">
        <span>Action</span>
      </th>
    </tr>
  </thead>

  <tbody>
    <tr v-for="(row, index) in table.paginate.data" :key="row.id" :class="{ 'is-selected': row.selected }">
      <td>
        <Checkbox v-model="row.selected" />
      </td>
      <td v-show="mode < 1" v-text="row.id"></td>
      <td v-text="row.name"></td>
      <td v-show="mode !== 3" v-text="row.meta.address.postcode"></td>
      <td v-text="row.meta.address.town"></td>
      <td v-show="mode !== 3" v-text="row.status"></td>
      <td v-show="mode !== 3">
        <div class="tags are-small is-centered">
          <span v-if="row.meta.isOpen" class="tag is-primary">OPEN</span>
          <span v-if="!row.meta.isOpen" class="tag is-danger">CLOSED</span>
        </div>

      </td>
      <td>
        <button class="button is-primary is-small is-pulled-right is-rounded ml-5px" @click="edit(row, index)">
          <icon :icon="['fas', 'pencil-alt']" />
        </button>

        <button class="button is-danger is-small is-pulled-right is-rounded"
          @click="toastr.confirm('ARE YOU SURE ABOUT DELETING THIS COMPANY',`Bus.$emit('on-company-delete', ${row.id})`)">
          <icon :icon="['fas', 'trash']" />
        </button>

      </td>
    </tr>
  </tbody>
  </tbody>
  <tfoot>
    <tr>
      <th colspan="4">
        <nav class="pagination is-rounded is-small" role="navigation" aria-label="pagination">
          <ul class="pagination-list">
            <li v-for="size in table.sizes" @click="table.setPaging(size)"><a class="pagination-link"
                :class="{'is-current' : table.paging == size }" v-text="size"></a>
            </li>
          </ul>

        </nav>

      </th>
      <th class="tac" colspan="1" v-show="mode !== 3">
        <p class="results" v-show="table.paginate.total !== undefined">
          @{{ '@cnt('paging')' | paging(table.paginate)}}
        </p>
      </th>
      <th class="tar" colspan="4" v-show="mode !== 3">


        <a @click="table.prev()" :disabled=" 1 == table.paginate.current_page "
          class="pagination-previous button is-small is-rounded  has-text-dark"
          :class="{'is-info' :  table.paginate.current_page > 1 }">Previous</a>
        <a @click="table.next()" class="pagination-next  is-rounded button is-small has-text-dark"
          :disabled=" table.paginate.last_page == table.paginate.current_page "
          :class="{'is-info' :  table.paginate.last_page > table.paginate.current_page }">Next</a>

      </th>
    </tr>
  </tfoot>

</table>