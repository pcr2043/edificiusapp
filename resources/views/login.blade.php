@if(request()->ajax() === false)
@extends('master')
@section('content')
@endif

@section('content')
<div class="columns is-multiline">

  <div class="column is-offset-one-quarter is-half">
    <auth inline-template>
      <div class="panel">
        <div class="panel-heading primary tac">
          @cnt('login')
        </div>
        <div class="body">
          <form @submit.prevent="submit">
            <div class="columns is-multiline">
              <div class="column is-full">
                <vinput v-model="formLogin.model.email" label="Email" name="email" type="email"
                  placeholder="Enter the Email" :error="formLogin.getError('email')" />
              </div>
              <div class="column is-full">
                <vinput v-model="formLogin.model.password" label="Password" name="password" type="password"
                  placeholder="Enter the Password" :error="formLogin.getError('password')" />
              </div>
            </div>
            <div class="columns">
              <div class="column is-full">
                <div class="field">
                  <button :disabled="formLogin.isSaving" type="submit"
                    class="button is-primary is-small is-rounded is-pulled-right">
                    <span v-if="formLogin.isSaving" class="icon is-loading is-small"></span>
                    LOGIN
                  </button>
                  <button type="button" class="button is-danger is-small is-rounded is-pulled-right mr-half"
                    @click="cancel()">
                    CANCEL
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </auth>
  </div>
</div>
@if(request()->ajax() === false)
@endsection
@endif