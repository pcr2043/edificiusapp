<section>
  {{-- <div v-if="table.form.isLoading && table.form.model != null"> --}}
  <div v-if="!table.form.isLoading && table.form.model != null">
    <form @submit.prevent="submit()" @keypress="table.form.clearError($event.target.name)">
      <!-- TABS    @click="table.form.clearErrorByTarget($event.target)" -->
      <br>
      <div class="buttons has-addons languages">
        <button @click="changeLanguage(lang)" type="button" class="button is-primary is-small is-rounded"
          v-for="(lang, index) in languages" :key="index" v-text="lang" :class="{ 'is-active' :  language == lang }" />
      </div>

      <div class="columns">
        <div class="column is-full">
          <minput :unique="true" :multiline="true" label="Question" :error="table.form.getError('meta.question')"
            v-model="table.form.model.meta[language].question" />
        </div>
      </div>

      <div class="columns">
        <div class="column is-full">
          <meditor :unique="true" label="Awnser" event="awnser" :error="table.form.getError('meta.awnser')"
            v-model="table.form.model.meta[language].answer" />
        </div>
      </div>


      <div class="columns">
        <div class="column is-full">
          <div class="field">
            <button :disabled="table.form.isSaving" type="submit"
              class="button is-primary is-small is-rounded is-pulled-right">
              <span v-if="table.form.isSaving" class="icon is-loading is-small"></span>
              SAVE
            </button>
            <button type="button" class="button is-danger is-small is-rounded is-pulled-right mr-half"
              @click="cancel()">
              CANCEL
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>