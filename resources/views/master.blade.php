@hasSection('content')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ env('APP_NAME')}}</title>
  <!-- CSS -->
  <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
  <div id="app" language="{{ $utils->language() }}" default-language="{{ $utils->languageDefault() }}"
    page="{{ isset($page) ? $page : -1 }}">
    <section class="container master">
      <NavB container="#container" :frontend="true" :user="{{ $utils->authUser() }}" :brand=" true" :menu="menu"
        :set-mode="setMode">
      </NavB>
      <Carousel v-if="navbarExclude" :slides="slides"></Carousel>
      <div v-if="menu.activeItem != null || page == -1" id="container"
        :class="(menu.activeItem != null) ? menu.activeItem.name : ''">
        @yield('content')
      </div>
      @include('partials.footer')
      @include('partials.register')
    </section>

  </div>
  <script type="application/javascript" src="{{ mix('js/app.js') }}"></script>
</body>

</html>
@endif