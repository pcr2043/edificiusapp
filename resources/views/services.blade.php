@if(request()->ajax() === false)
@extends('master')
@section('content')
@endif

@section('content')
<div class="columns is-multiline  services">
  <div class="column is-full">
    <div class="centered-title">@cnt('services')</div>
  </div>
  <div class="column is-half service">
    <i class="fas fa-lock has-text-info"></i>
    <span>@cnt('services.confidentiality')</span>
    <div class="text">@cnt('services.confidentiality.text')</div>
  </div>
  <div class="column is-half service">
    <div class="columns is-multiline">
      <div class="column is-full">
        <i class="fas fa-leaf has-text-info"></i>
        <span>@cnt('services.competence')</span>
        <div class="text"> @cnt('services.competence.text')</div>
      </div>

      <div class="column is-full">
        <i class="fas fa-handshake has-text-info"></i>
        <span>@cnt('services.trust')</span>
        <div class="text"> @cnt('services.trust.text')</div>
      </div>
    </div>
  </div>

</div>
@if(request()->ajax() === false)
@endsection
@endif