@hasSection('content')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ env('APP_NAME')}}</title>
  <!-- CSS -->
  <link href="{{ mix('css/communicate.css')}}" rel="stylesheet">
</head>

<body>
  <div id="app" page="{{ $page }}">
    <section class="hero is-primary is-bold">
      <div class="hero-body is-centered">
        <div class="container">
          <navb :menu="menu" :user="{{ $utils->authUser() }}" :set-mode="setMode"></navb>
          @include('partials.controls', ['cssClass' => 'is-primary'])
        </div>
      </div>
    </section>

    <section class="section">
      <div class="app">
        <div id="container">
          @yield('content')
        </div>
      </div>
    </section>
  </div>
  <script type="application/javascript" src="{{ mix('js/communicate.js') }}"></script>
</body>

</html>
@endif