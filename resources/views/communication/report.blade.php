<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>COMMUNICATION REPORT</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
</head>

<style>
  body {
    font-size: 12px;
  }

  table thead tr {
    background-color: #eeeddd;
  }

  table thead tr th {
    text-transform: uppercase;

  }

  .tag {
    font-size: 10px !important;
  }
</style>

<body>
  <table class="table is-striped is-fullwidth">
    <thead>
      <tr>
        <th>Date</th>
        <th>Employee</th>
        <th>Firm</th>
        <th>Person</th>
        <th>Contact Type</th>
        <th>Resume</th>
        <th>Description</th>
      </tr>
    </thead>

    <tbody>
      @foreach($data as $row)
      <tr>
        <td>{{ $row->schedule_at }}</td>
        <td>{{ $row->Employee->name  }}</td>
        <td>{{ $row->Company->name  }}</td>
        <td>{{ $row->Contact->name  }}</td>
        <td class="is-uppercase">
          @if($row->contact_type=='phone')
          <span class="tag is-primary">{{ $row->contact_type  }}</span>
          @endif
          @if($row->contact_type=='email')
          <span class="tag is-warning">{{ $row->contact_type  }}</span>
          @endif
          @if($row->contact_type== 'meeting')
          <span class="tag is-info">{{ $row->contact_type  }}</span>
          @endif
          @if($row->contact_type === 'other')
          <span class="tag is-black">{{ $row->contact_type  }}</span>
          @endif

        </td>
        <td>{{ $row->resume  }}</th>
        <td>{{ $row->content  }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</body>

</html>