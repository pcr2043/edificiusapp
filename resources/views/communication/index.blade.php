@if(request()->ajax() === false)
@extends('communicate.master')
@section('content')
@endif
<communication inline-template :mode-data="{{ $mode }}"
  :companies="{{ App\Company::with('Contacts')->get()->toJson() }}">
  <div class="columns is-multiple">
    <div class="column" :class="{ 
      'is-full' : mode === 0 || mode === -1, 
      'is-two-thirds' : mode === 1,
      'is-half' :mode === 2,
      'is-one-third' : mode === 3,
      'is-hidden' : mode === 4
    }">
      <div class="panel">
        {{-- <div class="tabs is-centered">
          <ul>
            <li class="is-active">Communications</li>
          </ul>
        </div> --}}
        @include('communication.table')
      </div>
    </div>


    <div class="column" :class="{ 
      'is-hidden' : mode === 0, 
      'is-one-third' : mode === 1,
      'is-half' : mode === 2,
      'is-two-thirds' : mode === 3,
      'is-full' : mode === 4,
    }">
      <div class="panel" v-show="mode > 0">
        @include('communication.edit')
      </div>
    </div>
  </div>
</communication>
@if(request()->ajax() === false)
@endsection
@endif