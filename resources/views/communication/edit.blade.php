<section>
  {{-- <div v-if="table.form.isLoading && table.form.model != null"> --}}
  <div v-if="!table.form.isLoading && table.form.model != null">
    <form @submit.prevent="submit()" @keypress="table.form.clearError($event.target.name)">
      <!-- TABS    @click="table.form.clearErrorByTarget($event.target)" -->
      <div class="columns is-multiline">
        <div class="column is-one-third">
          <vselect placeholder="Choose a Employee" v-model="table.form.model.employee_id" field="name"
            :items="{{ App\User::ByRoles(['call-center'])->toJson() }}" label="Employee" name="employee_id"
            :error="table.form.getError('employee_id')" />
        </div>

        <div class="column is-one-third">
          <vselect placeholder="Choose a Company" :event="companyChange" v-model="table.form.model.company_id"
            field="name" :items="companies" label="Company" name="company_id"
            :error="table.form.getError('company_id')" />
        </div>

        <div class="column is-one-third" v-if="company != null && company.contacts.length > 0">
          <vselect placeholder="Choose an Contact" v-model="table.form.model.contact_id" field="name"
            :items="company.contacts" label="Contact" name="contact_id" :error="table.form.getError('contact_id')" />
        </div>
      </div>
      <div class="columns is-multiline">

        <div class="column is-one-third">
          <vselect placeholder="Choose an type of Contact" v-model="table.form.model.contact_type" field="contact_type"
            :items="{{ $utils->ContactTypes(true) }}" label="Contact Type" name="contact_type"
            :error="table.form.getError('contact_type')" />
        </div>

        <div class="column is-one-third">
          <vinput v-model="schedule.date" label="Date" type="date" name="date"
            :error="table.form.getError('schedule_at')" />
        </div>
        <div class="column is-one-third">
          <vinput v-model="schedule.time" label="Time" type="time" name="time"
            :error="table.form.getError('schedule_at')" />
        </div>
      </div>
      <div class="columns is-multiline">

        <div class="column is-full">
          <vinput v-model="table.form.model.resume" label="Resume" name="resume" placeholder="Enter Resume"
            :error="table.form.getError('resume')" />
        </div>

        <div class="column is-full">
          <vinput :multiline="true" v-model="table.form.model.content" label="Content" name="content"
            placeholder="Enter Content" :error="table.form.getError('content')" />
        </div>

      </div>

      <div class="columns is-multiline">
        <!-- UPLOAD CONTROL -->
        <div class="column is-full">
          <upload v-model="table.form.model.documents" label="Click button to upload files" name="documents"
            event="user-document-upload" :extensions="['doc', 'docx', 'pdf', 'xls', 'xlsx']"
            placeholder="Upload Documents" :error="table.form.getError('meta.documents')" />
        </div>

        <!-- UPLOAD LIST -->
        <div class="column is-full">
          <div class="field">
            <label class="label is-small">Documents</label>

            <div class="control">
              <div class="list is-hoverable is-small is-files">
                <a v-for="(file, index) in table.form.model.documents" :key="index" class="list-item is-small">
                  @{{ file.name }}
                  <span class="icon has-background-primary has-text-white">
                    <icon :icon="['fas', 'cloud-download-alt']" @click="
                              fileManager.download(
                                'communications',
                                table.form.model.id,
                                'documents',
                                index
                              )
                            " />
                  </span>
                  <span class="icon has-background-danger has-text-white"
                    @click="table.form.model.documents.splice(index, 1)">
                    <icon :icon="['fas', 'trash']" />
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="columns">
        <div class="column is-full">
          <div class="field">
            <button :disabled="table.form.isSaving" type="submit"
              class="button is-primary is-small is-rounded is-pulled-right">
              <span v-if="table.form.isSaving" class="icon is-loading is-small"></span>
              SAVE
            </button>
            <button type="button" class="button is-danger is-small is-rounded is-pulled-right mr-half"
              @click="cancel()">
              CANCEL
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>