<section>
  {{-- <div v-if="table.form.isLoading && table.form.model != null"> --}}
  <div v-if="!table.form.isLoading && table.form.model != null">
    <form @keypress.enter="submit()" @submit.prevent="submit()" @keypress="table.form.clearError($event.target.name)">
      <!-- TABS    @click="table.form.clearErrorByTarget($event.target)" -->
      <br>
      <div class="buttons has-addons languages">
        <button @click="changeLanguage(lang)" type="button" class="button is-primary is-small is-rounded"
          v-for="(lang, index) in languages" :key="index" v-text="lang" :class="{ 'is-active' :  language == lang }" />
      </div>



      <div class="columns is-multiline">

        <div class="column is-one-third">
          <vinput v-model="table.form.model.reference" label="Reference" name="reference"
            placeholder="Enter the Reference" :error="table.form.getError('reference')" />
        </div>

        {{-- <div class="column is-one-third is-centered">
          <active v-model="table.form.model.isActive" label="Active" name="active"
            :error="table.form.getError('isActive')" />
        </div> --}}

        <div class="column is-one-third is-centered">
          <active v-model="table.form.model.type" active-text="TEXT" inactive-text="HTML" label="Type" name="type"
            :error="table.form.getError('type')" />
        </div>

        <div v-show="table.form.model.type === 1" class="column is-full">
          <minput :unique="true" :multiline="true" label="Value" :error="table.form.getError(`meta.${language}`)"
            v-model="table.form.model.meta[language]" />
        </div>

        <div v-show="table.form.model.type === 0" class="column is-full">
          <meditor :unique="true" event="meta" label="Value" v-model="table.form.model.meta[language]"
            :error="table.form.getError(`meta.${language}`)" />
        </div>



        <div class="column is-full">
          <minput :unique="true" :multiline="true" label="PlaceHolder"
            v-model="table.form.model.placeholder[language]" />
        </div>
      </div>


      <div class="columns">
        <div class="column is-full">
          <div class="field">
            <button :disabled="table.form.isSaving" type="submit"
              class="button is-primary is-small is-rounded is-pulled-right">
              <span v-if="table.form.isSaving" class="icon is-loading is-small"></span>
              SAVE
            </button>
            <button type="button" class="button is-danger is-small is-rounded is-pulled-right mr-half"
              @click="cancel()">
              CANCEL
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>