@if(request()->ajax() === false)
@extends('master')
@section('content')
@endif

@section('content')
<div class="columns is-multiline">
  <div class="column is-full">
    <div class="centered-title">@cnt('contact')</div>
  </div>
  <div class="column is-full">
    <contact-form inline-template>
      <form @submit.prevent="submit">
        <div class="columns">
          <div class="column is-four-thirds">
            <div class="columns is-multiline">
              <div class="column is-half">
                <vinput v-model="form.model.name" label="@cnt('form.name')" name="name" placeholder="@ph('form.name')"
                  :error="form.getError('name')" />
              </div>
              <div class="column is-half">
                <vinput v-model="form.model.company" label="@cnt('form.company')" name="name"
                  placeholder="@ph('form.company')" :error="form.getError('company')" />
              </div>
              <div class="column is-half">
                <vinput v-model="form.model.email" label="@cnt('form.email')" name="email"
                  placeholder="@ph('form.email')" :error="form.getError('email')" />
              </div>
              <div class="column is-half">
                <vinput v-model="form.model.phone" label="@cnt('form.phone.mobile')" name="phone"
                  placeholder="@ph('form.phone.mobile')" :error="form.getError('phone')" />
              </div>
              <div class="column is-half">
                <div class="field timeslot-field">
                  <label class="label is-small">@cnt('form.best_time_to_call')</label>
                  <div class="buttons has-addons">
                    <span @click="setTimeToCall('8:00')" :class="{ 'is-info' : hasTimeSlot('8:00') }"
                      class="button is-primary is-small is-rounded" role="button">&nbsp;8:00</span>
                    <span @click="setTimeToCall('9:00')" :class="{ 'is-info' : hasTimeSlot('9:00') }"
                      class="button is-primary is-small is-rounded" role="button">&nbsp;9:00</span>
                    <span @click="setTimeToCall('10:00')" :class="{ 'is-info' : hasTimeSlot('10:00') }"
                      class="button is-primary is-small is-rounded" role="button">10:00</span>
                    <span @click="setTimeToCall('11:00')" :class="{ 'is-info' : hasTimeSlot('11:00') }"
                      class="button is-primary is-small is-rounded" role="button">11:00</span>
                    <span @click="setTimeToCall('12:00')" :class="{ 'is-info' : hasTimeSlot('12:00') }"
                      class="button is-primary is-small is-rounded" role="button">12:00</span>
                    <span @click="setTimeToCall('13:00')" :class="{ 'is-info' : hasTimeSlot('13:00') }"
                      class="button is-primary is-small is-rounded" role="button">13:00</span>
                  </div>
                  <div class="buttons has-addons">
                    <span @click="setTimeToCall('14:00')" :class="{ 'is-info' : hasTimeSlot('14:00') }"
                      class="button is-primary is-small is-rounded" role="button">14:00</span>
                    <span @click="setTimeToCall('15:00')" :class="{ 'is-info' : hasTimeSlot('15:00') }"
                      class="button is-primary is-small is-rounded" role="button">15:00</span>
                    <span @click="setTimeToCall('16:00')" :class="{ 'is-info' : hasTimeSlot('16:00') }"
                      class="button is-primary is-small is-rounded" role="button">16:00</span>
                    <span @click="setTimeToCall('17:00')" :class="{ 'is-info' : hasTimeSlot('17:00') }"
                      class="button is-primary is-small is-rounded" role="button">17:00</span>
                    <span @click="setTimeToCall('18:00')" :class="{ 'is-info' : hasTimeSlot('18:00') }"
                      class="button is-primary is-small is-rounded" role="button">18:00</span>
                    <span @click="setTimeToCall('19:00')" :class="{ 'is-info' : hasTimeSlot('19:00') }"
                      class="button is-primary is-small is-rounded" role="button">19:00</span>
                  </div>
                </div>
                <p class="help is-danger" v-text="form.getError('bestTimeToCall')"></p>
              </div>
              <div class="column is-half">
                <vinput :multiline="true" :rows="4" v-model="form.model.message" label="@cnt('form.message')"
                  name="message" type="text" placeholder="Enter the Message" :error="form.getError('message')" />
              </div>
              <div class="column is-full">
                <button class="button is-small is-rounded is-primary is-pulled-right">
                  <span v-if="form.isSaving" class="icon is-loading is-small"></span>
                  @cnt('send')
                </button>
                <button @click="reset()" class="button is-small is-rounded is-danger is-pulled-right mr-5px">
                  @cnt('reset')
                </button>

              </div>
            </div>
          </div>
          <div class="column is-one-third">
            <div class="field">
              <label class="label is-small">@cnt('address')</label>
              @cnt('contact.details')
            </div>
          </div>
        </div>

      </form>
    </contact-form>
  </div>
</div>
@if(request()->ajax() === false)
@endsection
@endif