<section v-if="table.form.model !== undefined">
  <form @submit.prevent="submit()" @change="formChange($event.target)">
    <div class="tabs is-centered">
      <ul>
        <li :class="{'is-active' :  tab.isActive }" @click="tabs.setActive(tab)" v-for="(tab, index) in tabs.items">
          <a v-text="tab.name"></a>
        </li>
      </ul>
    </div>
    <div class="buttons has-addons languages">
      <button @click="changeLanguage(lang)" type="button" class="button is-primary is-small is-rounded"
        v-for="(lang, index) in languages" :key="index" v-text="lang" :class="{ 'is-active' :  language == lang }" />
    </div>

    {{-- GENERAL --}}
    <div class="columns is-multiline" v-show="table.form.model != null && tabs.items[0].isActive">

      <div class="column is-one-third is-centered">
        <active label="Is Active" v-model="table.form.model.isActive" />
      </div>
      <div class="column is-one-third is-centered">
        <active label="Is Published" v-model="table.form.model.isPublished" />
      </div>
      <div class="column is-one-third is-centered">
        <active label="Is isFeatured" v-model="table.form.model.isFeatured" />
      </div>
      <div class="column is-half">
        <minput :unique="true" :event="titleChange" label="Title" v-model="table.form.model.title[language]"
          :error="table.form.getError(`title.${language}`)" />
      </div>
      <div class="column is-half">
        <minput :unique="true" label="Alias" v-model="table.form.model.alias[language]" />
      </div>
      <div class="column is-full">
        <meditor :unique="true" event="description" label="Description" v-model="table.form.model.description[language]"
          :languages="{{ json_encode($utils->getBOLanguages()) }}"
          :error="table.form.getError(`description.${language}`)" />
      </div>

      <div class="column is-full">
        <minput :unique="true" :multiline="true" label="Alias" textarea="true"
          v-model="table.form.model.notes[language]" />
      </div>
    </div>

    {{-- LOCATION --}}
    <div class="columns is-multiline" v-show="table.form.model != null && tabs.items[1].isActive">

      <div class="column is-one-third">
        <vinput v-model="table.form.model.meta.proprietary.name" label="Proprietary" name="meta.proprietary"
          placeholder="Enter Proprietary Name ..." :error="table.form.getError('meta.proprietary.name')" />
      </div>

      <div class="column is-one-third">
        <vinput v-model="table.form.model.meta.proprietary.mobile" label="Proprietary Mobile | Phone" name="meta.mobile"
          placeholder="Enter Mobile, Phone ..." :error="table.form.getError('meta.proprietary.contact')" />
      </div>

      <div class="column is-one-third">
        <vinput v-model="table.form.model.meta.proprietary.email" label="Proprietary Email" name="meta.email"
          placeholder="Enter Email ..." :error="table.form.getError('meta.proprietary.email')" />
      </div>

      <div class="column is-two-thirds">
        <vinput v-model="table.form.model.meta.address.street" label="Street, place" name="meta.address.street"
          placeholder="Enter Addresss, Place ..." :error="table.form.getError('meta.address.street')" />
      </div>
      <div class="column is-one-third">
        <vinput v-model="table.form.model.meta.address.number" label="Number, floor" name="meta.address.number"
          placeholder="Enter Number, Floor ..." :error="table.form.getError('meta.address.number')" />
      </div>
      <div class="column is-half">
        <Countries v-model="table.form.model.meta.address.country" label="Country" name="meta.address.country"
          placeholder="Pick Country" :error="table.form.getError('meta.address.country')" />
      </div>
      <div class="column is-one-quarter">
        <vinput v-model="table.form.model.meta.address.postcode" label="Postcode" name="meta.address.postcode"
          placeholder="Enter Postcode" :error="table.form.getError('meta.address.postcode')" />
      </div>
      <div class="column is-one-quarter">
        <vinput v-model="table.form.model.meta.address.town" label="Town" name="meta.address.town"
          placeholder="Enter Town" :error="table.form.getError('meta.address.town')" />
      </div>

      <div class="column is-one-third">
        <Checkbox v-for="index in Object.keys(cantons).splice(0,10)" :key="index" :inline="true" :val="index"
          :label="cantons[index]" v-model="table.form.model.meta.address.canton">
      </div>

      <div class="column is-one-third">
        <Checkbox v-for="index in  Object.keys(cantons).splice(10,10)" :key="index" :inline="true" :val="index"
          :label="cantons[index]" v-model="table.form.model.meta.address.canton">
      </div>

      <div class="column is-one-third">
        <Checkbox v-for="index in  Object.keys(cantons).splice(20,10)" :key="index" :inline="true" :val="index"
          :label="cantons[index]" v-model="table.form.model.meta.address.canton">
      </div>


      <div class="column is-full">
        <p class="help is-danger" v-text="table.form.getError('meta.address.canton')"></p>
      </div>



    </div>

    {{-- DETAILS --}}
    <div class="columns is-multiline" v-show="table.form.model != null && tabs.items[2].isActive">
      <div class="column is-one-third">
        <vselect v-model="table.form.model.typeProperty" :items="{{ json_encode($utils->PropertyTypes()) }}"
          label="Type" name="typeProperty" :error="table.form.getError('typeProperty')" />
      </div>
      <div class="column is-one-third">
        <vselect v-model="table.form.model.typeEnvironment" :items="{{ json_encode($utils->EnvPlaces()) }}"
          label="Environment" name="typeEnvironment" :error="table.form.getError('typeEnvironment')" />
      </div>
      <div class="column is-one-third">
        <vselect v-model="table.form.model.typeOwner" :items="{{ json_encode($utils->TypeRents()) }}" label="Type Owner"
          name="typeOwner" :error="table.form.getError('typeOwner')" />
      </div>

      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.apartments" type="number" label="Apartament" name="apartments"
          placeholder="Enter  a value" :error="table.form.getError('apartments')" />
      </div>

      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.stores" type="number" label="Stores" name="stores" placeholder="Enter a value"
          :error="table.form.getError('stores')" />
      </div>

      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.offices" type="number" label="Offices" name="offices"
          placeholder="Enter a value" :error="table.form.getError('offices')" />
      </div>

      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.garages" type="number" label="Garages" name="garages"
          placeholder="Enter a value" :error="table.form.getError('garages')" />
      </div>


      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.car_places" type="number" label="Car Places" name="car_places"
          placeholder="Enter a value" :error="table.form.getError('car_places')" />
      </div>


      <hr>

      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.apartments_m2" type="number" label="apartments sqm²" name="apartments_m2"
          placeholder="Enter a value" :error="table.form.getError('apartments_m2')" />
      </div>

      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.stores_m2" type="number" label="Stores sqm²" name="stores_m2"
          placeholder="Enter a value" :error="table.form.getError('stores_m2')" />
      </div>

      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.offices_m2" type="number" label="Offices sqm²" name="offices_m2"
          placeholder="Enter a value" :error="table.form.getError('offices_m2')" />
      </div>
    </div>

    {{-- RENT & COSTS --}}
    <div class="columns is-multiline" v-show="table.form.model != null && tabs.items[3].isActive">

      <div class="column is-one-fifth  has-background-info">
        <vinput v-model="table.form.model.price" label="Price" name="price" placeholder="Enter the price"
          :error="table.form.getError('price')" />
      </div>

      <div class="column is-offset-three-fifths is-one-fifth  has-background-info">
        <vinput v-model="table.form.model.meta.property.year" label="Year Construction" name="meta.property.year"
          placeholder="Enter a year" :error="table.form.getError('meta.property.year')" />
      </div>


      {{-- RENT CHARGES --}}

      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.private_rent" label="Private Rent" name="private_rent" placeholder=""
          :error="table.form.getError('private_rent')" />
      </div>

      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.galleries_rent" label="Galleries Rent" name="galleries_rent" placeholder=""
          :error="table.form.getError('galleries_rent')" />
      </div>

      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.professional_rent" label="Professional Rent" name="professional_rent"
          placeholder="" :error="table.form.getError('professional_rent')" />
      </div>


      <div class="column is-one-fifth">
        <vinput v-model="table.form.model.annual_charges" label="Annual Charges" name="annual_charges" placeholder=""
          :error="table.form.getError('annual_charges')" />
      </div>

      <div class="column is-one-fifth">
        <vinput :disabled="true" v-model="table.form.model.total_m2" label="Total sqm²" name="total_m2" placeholder=""
          :error="table.form.getError('total_m2')" />
      </div>



      {{-- GROSS INCOME --}}
      <div class="column is-one-fifth has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.gross_income" label="Gross Income" name="gross_income"
          placeholder="" :error="table.form.getError('gross_income')" />
      </div>

      <div class="column is-one-fifth has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.gross_yield" label="Gross Yield %" name="gross_yield"
          placeholder="" :error="table.form.getError('gross_yield')" />
      </div>

      <div class="column is-one-fifth has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.gross_income_m2" label="Gross Income / sqm²"
          name="gross_income_m2" placeholder="" :error="table.form.getError('gross_income_m2')" />
      </div>

      <div class="column is-one-fifth has-background-warning">
        <vinput v-model="table.form.model.gross_income_manual" label="Gross Income Manual" name="gross_income_manual"
          placeholder="" :error="table.form.getError('gross_income_manual')" />
      </div>

      <div class="column is-one-fifth has-background-warning">
        <vinput v-model="table.form.model.gross_yield_manual" label="Gross Yield Manual" name="gross_yield_manual"
          placeholder="" :error="table.form.getError('gross_yield_manual')" />
      </div>



      {{-- CHARGES --}}
      <div class="column is-one-fifth has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.charges" label="Charges" name="charges" placeholder=""
          :error="table.form.getError('charges')" />
      </div>

      <div class="column is-one-fifth has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.charges_yield" label="Charges Yield %" name="charges_yield"
          placeholder="" :error="table.form.getError('charges_yield')" />
      </div>

      <div class="column is-one-fifth has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.charges_m2" label="Charges / sqm²" name="charges_m2"
          placeholder="" :error="table.form.getError('charges_m2')" />
      </div>

      <div class="column is-one-fifth has-background-warning">
        <vinput v-model="table.form.model.charges_manual" label="Charges Manual" name="charges_manual" placeholder=""
          :error="table.form.getError('charges_manual')" />
      </div>

      <div class="column is-one-fifth has-background-warning">
        <vinput v-model="table.form.model.charges_yield_manual" label="Charges Yield Manual" name="charges_yield_manual"
          placeholder="" :error="table.form.getError('charge_yield_manual')" />
      </div>


      {{-- NET INCOME --}}
      <div class="column is-one-fifth has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.net_income" label="Net Income" name="net_income"
          placeholder="" :error="table.form.getError('net_income')" />
      </div>

      <div class="column is-one-fifth has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.net_yield" label="Net Yield %" name="net_yield"
          placeholder="" :error="table.form.getError('net_yield')" />
      </div>

      <div class="column is-one-fifth has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.net_income_m2" label="Net Income / sqm²" name="net_income_m2"
          placeholder="" :error="table.form.getError('net_income_m2')" />
      </div>

      <div class="column is-one-fifth has-background-warning">
        <vinput v-model="table.form.model.net_income_manual" label="Net Income Manual" name="net_income_manual"
          placeholder="" :error="table.form.getError('net_income_manual')" />
      </div>

      <div class="column is-one-fifth has-background-warning">
        <vinput v-model="table.form.model.net_yield_manual" label="Net Yield Manual" name="net_yield_manual"
          placeholder="" :error="table.form.getError('net_yield_manual')" />
      </div>


      {{-- SPACES DETAILS --}}
      <div class="column is-one-third">
        <vinput :disabled="true" v-model="table.form.model.private_rent" label="Private Rent" name="private_rent"
          placeholder="" :error="table.form.getError('private_rent')" />
      </div>

      <div class="column is-one-third">
        <vinput :disabled="true" v-model="table.form.model.galleries_rent" label="Galleries Rent" name="galleries_rent"
          placeholder="" :error="table.form.getError('galleries_rent')" />
      </div>

      <div class="column is-one-third">
        <vinput :disabled="true" v-model="table.form.model.professional_rent" label="Professional Rent"
          name="professional_rent" placeholder="" :error="table.form.getError('professional_rent')" />
      </div>


      <div class="column is-one-third">
        <vinput v-model="table.form.model.apartments" type="number" label="Apartments" name="apartments"
          placeholder="Enter  a value" :error="table.form.getError('apartments')" />
      </div>

      <div class="column is-one-third">
        <vinput v-model="table.form.model.stores" type="number" label="Stores" name="stores" placeholder="Enter a value"
          :error="table.form.getError('stores')" />
      </div>

      <div class="column is-one-third">
        <vinput v-model="table.form.model.offices" type="number" label="Offices" name="offices"
          placeholder="Enter a value" :error="table.form.getError('offices')" />
      </div>


      <div class="column is-one-third">
        <vinput v-model="table.form.model.apartments_m2" type="number" label="Apartments sqm²" name="apartments_m2"
          placeholder="Enter a value" :error="table.form.getError('apartments_m2')" />
      </div>

      <div class="column is-one-third">
        <vinput v-model="table.form.model.stores_m2" type="number" label="Stores sqm²" name="stores_m2"
          placeholder="Enter a value" :error="table.form.getError('stores_m2')" />
      </div>

      <div class="column is-one-third">
        <vinput v-model="table.form.model.offices_m2" type="number" label="Offices sqm²" name="offices_m2"
          placeholder="Enter a value" :error="table.form.getError('offices_m2')" />
      </div>

      {{-- APART, STORE, OFFCIE INCOME PERC --}}

      <div class="column is-one-third has-background-info">
        <vinput :disabled="true" v-model="table.form.model.meta.property.apartments.income_perc" type="number"
          label="Apartments Income %" name="apartments_m2" placeholder="Enter a value" />
      </div>

      <div class="column is-one-third has-background-info">
        <vinput :disabled="true" v-model="table.form.model.meta.property.stores.income_perc" type="number"
          label="Stores Income %" name="stores_m2" placeholder="Enter a value" />
      </div>

      <div class="column is-one-third has-background-info">
        <vinput :disabled="true" v-model="table.form.model.meta.property.offices.income_perc" type="number"
          label="Offices Income %" name="offices_m2" placeholder="Enter a value" />
      </div>

      {{-- APART, STORE, OFFICE PRICE YEAR --}}

      <div class="column is-one-third has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.meta.property.apartments.price_year" type="number"
          label="Apartments Price Year" name="meta.property.apartments.price_year" placeholder="Enter a value" />
      </div>

      <div class="column is-one-third has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.meta.property.stores.price_year" type="number"
          label="Stores Price Year" name="meta.property.stores.price_year" placeholder="Enter a value" />
      </div>

      <div class="column is-one-third has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.meta.property.offices.price_year" type="number"
          label="Offices Price Year" name="meta.property.offices.price_year" placeholder="Enter a value" />
      </div>

      {{-- APART, STORE, OFFICE PRICE MONTH --}}

      <div class="column is-one-third has-background-warning">
        <vinput :disabled="true" v-model="table.form.model.meta.property.apartments.price_month" type="number"
          label="Apartments Price Month" name="meta.property.apartments.price_month" placeholder="Enter a value" />
      </div>

      <div class="column is-one-third has-background-warning">
        <vinput :disabled="true" v-model="table.form.model.meta.property.stores.price_month" type="number"
          label="Stores Price Month" name="meta.property.stores.price_month" placeholder="Enter a value" />
      </div>

      <div class="column is-one-third has-background-warning">
        <vinput :disabled="true" v-model="table.form.model.meta.property.offices.price_month" type="number"
          label="Offices Price Month" name="meta.property.offices.price_month" placeholder="Enter a value" />
      </div>


      {{-- APART, STORE, OFFICE PRICE YEAR --}}

      <div class="column is-one-third has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.meta.property.apartments.price_m2_year" type="number"
          label="Apartments price/sqm² Year" name="meta.property.apartments.price_m2_year"
          placeholder="Enter a value" />
      </div>

      <div class="column is-one-third has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.meta.property.stores.price_m2_year" type="number"
          label="Stores price/sqm² Year" name="meta.property.stores.price_m2_year" placeholder="Enter a value" />
      </div>

      <div class="column is-one-third has-background-primary">
        <vinput :disabled="true" v-model="table.form.model.meta.property.offices.price_m2_year" type="number"
          label="Offices price/sqm² Year" name="meta.property.offices.price_m2_year" placeholder="Enter a value" />
      </div>


      {{-- APART, STORE, OFFCIE PRICE MONTH --}}

      <div class="column is-one-third has-background-warning">
        <vinput :disabled="true" v-model="table.form.model.meta.property.apartments.price_m2_month" type="number"
          label="Apartments price/sqm² Month" name="meta.property.apartments.price_m2_month"
          placeholder="Enter a value" />
      </div>

      <div class="column is-one-third has-background-warning">
        <vinput :disabled="true" v-model="table.form.model.meta.property.stores.price_m2_month" type="number"
          label="Stores  price/sqm² Month" name="meta.property.stores.price_m2_month" placeholder="Enter a value" />
      </div>

      <div class="column is-one-third has-background-warning">
        <vinput :disabled="true" v-model="table.form.model.meta.property.offices.price_m2_month" type="number"
          label="Offices price/sqm² Month" name="meta.property.offices.price_m2_month" placeholder="Enter a value" />
      </div>



    </div>




    {{-- DOCUMENTS --}}
    <div class="columns is-multiline" v-show="table.form.model != null && tabs.items[4].isActive">

      <!-- UPLOAD CONTROL -->
      <div class="column is-full">
        <Upload v-model="table.form.model.documents" label="Click button to upload files" name="documents"
          event="property-document-upload" :extensions="['doc', 'docx', 'pdf', 'xls', 'xlsx']"
          placeholder="Upload Documents" :error="table.form.getError('documents')" />
      </div>

      <!-- UPLOAD LIST -->
      <div class="column is-full">
        <div class="field">
          <label class="label is-small">Documents</label>

          <div class="control">
            <div class="list is-hoverable is-small is-files">
              <a v-for="(file, index) in table.form.model.documents" :key="index" class="list-item is-small">
                @{{ file.name }}
                <span class="icon has-background-primary has-text-white">
                  <icon :icon="['fas', 'cloud-download-alt']" @click="
                            fileManager.download(
                              'properties',
                              table.form.model.id,
                              'documents',
                              index
                            )
                          " />
                </span>
                <span class="icon has-background-danger has-text-white"
                  @click="table.form.model.documents.splice(index, 1)">
                  <icon :icon="['fas', 'trash']" />
                </span>
              </a>
            </div>
          </div>
        </div>

      </div>


      <!-- UPLOAD CONTROL -->
      <div class="column is-full">
        <Upload v-model="table.form.model.shared" label="Click button to upload files" name="shared"
          event="property-shared-upload" :extensions="['doc', 'docx', 'pdf', 'xls', 'xlsx']"
          placeholder="Upload Shared Documents" :error="table.form.getError('shared')" />
      </div>

      <!-- UPLOAD LIST -->
      <div class="column is-full">
        <div class="field">
          <label class="label is-small">Shared</label>

          <div class="control">
            <div class="list is-hoverable is-small is-files">
              <a v-for="(file, index) in table.form.model.shared" :key="index" class="list-item is-small">
                @{{ file.name }}
                <span class="icon has-background-primary has-text-white">
                  <icon :icon="['fas', 'cloud-download-alt']" @click="
                                fileManager.download(
                                  'properties',
                                  table.form.model.id,
                                  'shared',
                                  index
                                )
                              " />
                </span>
                <span class="icon has-background-danger has-text-white"
                  @click="table.form.model.shared.splice(index, 1)">
                  <icon :icon="['fas', 'trash']" />
                </span>
              </a>
            </div>
          </div>
        </div>

      </div>

    </div>

    {{-- IMAGES --}}
    <div class="columns is-multiline" v-show="table.form.model != null && tabs.items[5].isActive">

      <!-- UPLOAD CONTROL -->
      <div class="column is-full">
        <Upload v-model="table.form.model.images" label="Click button to images" name="images"
          event="property-image-upload" :extensions="['jpg', 'jpeg', 'png']" placeholder="Upload Images"
          :error="table.form.getError('images')" />
      </div>
      {{-- @{{ file.name }} --}}
      <!-- UPLOAD LIST -->
      <div class="column is-full">
        <div class="columns is-multiline">
          <div class="column is-one-fifth image-column" v-for="(image, index) in table.form.model.images" :key="index"
            class="list-item is-small">
            <img v-if="image.data !== null" :src="image.data" alt="">
            <img v-if="image.data === null" :src="`/data/properties/${image.path}`" alt="">
            <span class="icon has-background-primary has-text-white">
              <icon :icon="['fas', 'cloud-download-alt']" @click="
                      fileManager.download(
                        'properties',
                        table.form.model.id,
                        'images',
                        index
                      )
                    " />
            </span>
            <span class="icon has-background-danger has-text-white" @click="table.form.model.images.splice(index, 1)">
              <icon :icon="['fas', 'trash']" />
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="columns">
      <div class="column is-full">
        <div class="field">
          <button :disabled="uploadingDocuments || table.form.isSaving" type="submit"
            class="button is-primary is-small is-rounded is-pulled-right">
            <span v-if="table.form.isSaving" class="icon is-loading is-small"></span>
            SAVE
          </button>
          <button type="button" class="button is-danger is-small is-rounded is-pulled-right mr-half" @click="cancel()">
            CANCEL
          </button>
        </div>
      </div>
    </div>
  </form>
</section>