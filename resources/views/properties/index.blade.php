@if(request()->ajax() === false)
@extends('admin.master')
@section('content')
@endif
<properties inline-template :mode-data="{{ $mode }}" :languages="{{ $utils->getBOLanguages(true) }}"
  :cantons="{{ $utils->ListCantonsSimple(true) }}">
  <div class="columns is-multiple">
    <div class="column" :class="{ 
      'is-full' : mode === 0 || mode === -1, 
      'is-two-thirds' : mode === 1,
      'is-half' :mode === 2,
      'is-one-third' : mode === 3,
      'is-hidden' : mode === 4
    }">
      <div class="panel">
        {{-- <div class="tabs is-centered">
          <ul>
            <li class="is-active">Communications</li>
          </ul>
        </div> --}}
        @include('properties.table')
      </div>
    </div>


    <div class="column" :class="{ 
      'is-hidden' : mode === 0, 
      'is-one-third' : mode === 1,
      'is-half' : mode === 2,
      'is-two-thirds' : mode === 3,
      'is-full' : mode === 4,
    }">
      <div class="panel" v-show="mode > 0">
        @include('properties.edit')
      </div>
    </div>
  </div>
</properties>
@if(request()->ajax() === false)
@endsection
@endif