<table class="table is-fullwidth" @keypress="table.key($event)">
  <thead>
    <tr>
      <td colspan="9">
        <button class="button is-info is-small is-rounded ml-5px" @click="create()">
          <icon :icon="['fas', 'plus']" />

        </button>

        <button v-if="trash.length > 0" class="button is-danger is-small is-rounded"
          @click="toastr.confirm('ARE YOU SURE ABOUT DELETING THIS USERS',`Bus.$emit('on-users-delete')`)">
          <icon :icon="['fas', 'trash']" />

        </button>

        <a v-if="table.isLoading" class="is-loading"></a>
      </td>
    </tr>
    <tr class="no-select">
      <th class="w-5">
        <span class="start">
          <Checkbox v-model="table.selectAll" @click="table.select($event)" />
        </span>
      </th>
      <th class="w-10" v-show="mode < 1">
        <span class="sort start" :class="table.sortValue('id')" @click="table.sort('id')">Id</span>
      </th>
      <th class="w-25">
        <span class="sort" :class="table.sortValue('title')" @click="table.sort('title')">Title</span>
      </th>
      <th class="w-15">
        <span class="sort" :class="table.sortValue('canton')" @click="table.sort('canton')">Canton</span>
      </th>
      <th class="w-10" v-show="mode < 1">
        <span class="sort" :class="table.sortValue('isActive')" @click="table.sort('isActive')">Is Active</span>
      </th>
      <th class="w-10" v-show="mode < 1">
        <span class="sort" :class="table.sortValue('isPublished')" @click="table.sort('isPublished')">Is
          Published</span>
      </th>
      <th class="w-10" v-show="mode < 1">
        <span class="sort" :class="table.sortValue('isFeatured')" @click="table.sort('isFeatured')">Is Featured</span>
      </th>
      <th class="w-15">
        <span>Action</span>
      </th>
    </tr>
  </thead>

  <tbody>
    <tr v-for="(row, index) in table.paginate.data" :key="row.id" :class="{ 'is-selected': row.selected }">

      <td>
        <Checkbox v-model="row.selected" />
      </td>
      <td v-show="mode < 1" v-text="row.id"></td>
      <td v-text="row.title[language]"></td>
      <td>
        <div class="tags are-small is-centered">
          <span class="tag is-primary" v-text="cantons[row.meta.address.canton]"></span>
        </div>
      </td>

      <td v-show="mode < 1">
        <div class="tags are-small is-centered">
          <span v-if="row.isActive" class="tag is-success">ACTIVE</span>
          <span v-if="!row.isActive" class="tag is-danger">INACTIVE</span>
        </div>
      </td>
      <td v-show="mode < 1">
        <div class="tags are-small is-centered">
          <span v-if="row.isPublished" class="tag is-success">PUBLISHED</span>
          <span v-if="!row.isPublished" class="tag is-danger">NOT PUBLISHED</span>
        </div>
      </td>
      <td v-show="mode < 1">
        <div class="tags are-small is-centered">
          <span v-if="row.isFeatured" class="tag is-success">FEATURED</span>
          <span v-if="!row.isFeatured" class="tag is-danger">NOT INACTIVE</span>
        </div>
      </td>

      <td>
        <button class="button is-primary is-small is-pulled-right is-rounded ml-5px" @click="edit(row, index)">
          <icon :icon="['fas', 'pencil-alt']" />
        </button>

        <button class="button is-danger is-small is-pulled-right is-rounded"
          @click="toastr.confirm('ARE YOU SURE ABOUT DELETING THIS USER',`Bus.$emit('on-user-delete', ${row.id})`)">
          <icon :icon="['fas', 'trash']" />
        </button>

      </td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <th colspan="3">
        <nav class="pagination is-rounded is-small" role="navigation" aria-label="pagination">
          <ul class="pagination-list">
            <li v-for="size in table.sizes" @click="table.setPaging(size)"><a class="pagination-link"
                :class="{'is-current' : table.paging == size }" v-text="size"></a>
            </li>
          </ul>

        </nav>

      </th>
      <th class="tac" colspan="3">
        <p class="results" v-show="table.paginate.total !== undefined">
          @{{ '@cnt('paging')' | paging(table.paginate)}}
        </p>
      </th>
      <th class="tar" colspan="2" v-show="mode !== 3">


        <a @click="table.prev()" :disabled=" 1 == table.paginate.current_page "
          class="pagination-previous button is-small is-rounded  has-text-dark"
          :class="{'is-info' :  table.paginate.current_page > 1 }">Previous</a>
        <a @click="table.next()" class="pagination-next  is-rounded button is-small has-text-dark"
          :disabled=" table.paginate.last_page == table.paginate.current_page "
          :class="{'is-info' :  table.paginate.last_page > table.paginate.current_page }">Next</a>

      </th>
    </tr>
  </tfoot>
</table>