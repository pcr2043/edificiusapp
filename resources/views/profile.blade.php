@if(request()->ajax() === false)
@extends('master')
@section('content')
@endif
@section('content')
<div class="columns is-multiline">
  <div class="column is-full">
    <div class="centered-title">@cnt('profile')</div>
  </div>
  <profile-form inline-template :user-data="{{ $utils->authUser () }}">
    <div class="column is-full">
      <form @submit.prevent="submit()" @keypress="table.form.clearError($event.target.name)">
        <!-- TABS    @click="table.form.clearErrorByTarget($event.target)" -->
        <div class="tabs is-centered">
          <ul>
            <li v-for="(tab, index) in tabs.items" :key="index" :class="{ 'is-active': tab.isActive }"
              @click="tabs.setActive(tab)">
              <a v-text="tab.name"></a>
            </li>
          </ul>
        </div>

        <div class="columns is-multiline" :class="{ 'is-hidden': !tabs.items[0].isActive }">
          <div class="column is-half">
            <vinput v-model="form.model.firstname" label="FirstName" name="firstname" placeholder="Enter the Firstname"
              :error="form.getError('firstname')" />
          </div>
          <div class="column is-half">
            <vinput v-model="form.model.lastname" label="Lastname" name="lastname" placeholder="Enter the Lastname"
              :error="form.getError('lastname')" />
          </div>
          <div class="column is-half">
            <vinput v-model="form.model.email" label="Email" name="email" type="email" placeholder="Enter the Email"
              :error="form.getError('email')" />
          </div>
          <div class="column is-half">
            <vinput v-model="form.model.password" label="Password" name="password" type="password"
              placeholder="Enter the Password" :error="form.getError('password')" />
          </div>

          <div class="column is-half">
            <gender v-model="form.model.meta.gender" label="Gender" name="meta.gender"
              :error="form.getError('meta.gender')" />
          </div>
          <div class="column is-half">
            <vinput v-model="form.model.meta.birthdate" label="Birthdate" name="meta.birthdate" type="date"
              placeholder="Enter the Birthdate" :error="form.getError('meta.birthdate')" />
          </div>
          <div class="column is-two-quarters">
            <roles v-model="form.model.roles" :disabled="!form.model.isAdmin" label="Roles" name="roles"
              :error="form.getError('roles')" />
          </div>

          <div class="column is-one-quarter">
            <active v-model="form.model.isActive" label="Active" name="active" :error="form.getError('isActive')" />
          </div>
        </div>

        <!-- CONTACT -->
        <div class="columns is-multiline" :class="{ 'is-hidden': !tabs.items[1].isActive }">
          <div class="column is-half">
            <vinput v-model="form.model.meta.position" label="Position" name="meta.position"
              placeholder="Enter Position ..." :error="form.getError('meta.position')" />
          </div>

          <div class="column is-half">
            <vinput v-model="form.model.meta.company" label="Company" name="meta.company"
              placeholder="Enter Company ..." :error="form.getError('meta.company')" />
          </div>

          <div class="column is-half">
            <vinput v-model="form.model.meta.mobile" label="Mobile" name="meta.mobile" placeholder="Enter Mobile ..."
              :error="form.getError('meta.mobile')" />
          </div>
          <div class="column is-half">
            <vinput v-model="form.model.meta.phone" label="Phone" name="meta.phone" placeholder="Enter Phone ..."
              :error="form.getError('meta.phone')" />
          </div>
          <div class="column is-half">
            <vinput v-model="form.model.meta.fax" label="Fax" name="meta.fax" placeholder="Enter Fax ..."
              :error="form.getError('meta.fax')" />
          </div>
          <div class="column is-half">
            <vinput v-model="form.model.meta.website" label="Website" name="meta.website"
              placeholder="Enter Website ..." :error="form.getError('meta.website')" />
          </div>

          <div class="column is-two-thirds">
            <vinput v-model="form.model.meta.address.street" label="Street, place" name="meta.address.street"
              placeholder="Enter Addresss, Place ..." :error="form.getError('meta.address.street')" />
          </div>
          <div class="column is-one-third">
            <vinput v-model="form.model.meta.address.number" label="Number, floor" name="meta.address.number"
              placeholder="Enter Number, Floor ..." :error="form.getError('meta.address.number')" />
          </div>
          <div class="column is-half">
            <countries v-model="form.model.meta.address.country" label="Country" name="meta.address.country"
              placeholder="Pick Country" :error="form.getError('meta.address.country')" />
          </div>
          <div class="column is-one-quarter">
            <vinput v-model="form.model.meta.address.postcode" label="Postcode" name="meta.address.postcode"
              placeholder="Enter Postcode" :error="form.getError('meta.address.postcode')" />
          </div>
          <div class="column is-one-quarter">
            <vinput v-model="form.model.meta.address.town" label="Town" name="meta.address.town"
              placeholder="Enter Town" :error="form.getError('meta.address.town')" />
          </div>
          <div class="column is-half">
            <vinput v-model="form.model.meta.email" label="Company Email | Second Email" name="meta.email2"
              placeholder="Enter Email ..." :error="form.getError('meta.email')" />
          </div>

          <div class="column is-half">
            <div class="field timeslot-field">
              <label class="label is-small">@cnt('form.best_time_to_call')</label>
              <div class="buttons has-addons timeslots">
                <span @click="setTimeToCall('8:00')" :class="{ 'is-info' : hasTimeSlot('8:00') }"
                  class="button is-primary is-small is-rounded" role="button">&nbsp;8:00</span>
                <span @click="setTimeToCall('9:00')" :class="{ 'is-info' : hasTimeSlot('9:00') }"
                  class="button is-primary is-small is-rounded" role="button">&nbsp;9:00</span>
                <span @click="setTimeToCall('10:00')" :class="{ 'is-info' : hasTimeSlot('10:00') }"
                  class="button is-primary is-small is-rounded" role="button">10:00</span>
                <span @click="setTimeToCall('11:00')" :class="{ 'is-info' : hasTimeSlot('11:00') }"
                  class="button is-primary is-small is-rounded" role="button">11:00</span>
                <span @click="setTimeToCall('12:00')" :class="{ 'is-info' : hasTimeSlot('12:00') }"
                  class="button is-primary is-small is-rounded" role="button">12:00</span>
                <span @click="setTimeToCall('13:00')" :class="{ 'is-info' : hasTimeSlot('13:00') }"
                  class="button is-primary is-small is-rounded" role="button">13:00</span>
              </div>

              <div class="buttons has-addons timeslots">
                <span @click="setTimeToCall('14:00')" :class="{ 'is-info' : hasTimeSlot('14:00') }"
                  class="button is-primary is-small is-rounded" role="button">14:00</span>
                <span @click="setTimeToCall('15:00')" :class="{ 'is-info' : hasTimeSlot('15:00') }"
                  class="button is-primary is-small is-rounded" role="button">15:00</span>
                <span @click="setTimeToCall('16:00')" :class="{ 'is-info' : hasTimeSlot('16:00') }"
                  class="button is-primary is-small is-rounded" role="button">16:00</span>
                <span @click="setTimeToCall('17:00')" :class="{ 'is-info' : hasTimeSlot('17:00') }"
                  class="button is-primary is-small is-rounded" role="button">17:00</span>
                <span @click="setTimeToCall('18:00')" :class="{ 'is-info' : hasTimeSlot('18:00') }"
                  class="button is-primary is-small is-rounded" role="button">18:00</span>
                <span @click="setTimeToCall('19:00')" :class="{ 'is-info' : hasTimeSlot('19:00') }"
                  class="button is-primary is-small is-rounded" role="button">19:00</span>
              </div>
            </div>
            <vinput :multiline="true" :rows="4" v-model="form.model.meta.motivation" label="@cnt('form.message')"
              name="meta.motivation" type="text" placeholder="Enter the Message"
              :error="form.getError('meta.motivation')" />
          </div>

        </div>

        <div class="columns">
          <div class="column is-full">
            <div class="field">
              <button :disabled="uploadingDocuments || form.isSaving" type="submit"
                class="button is-primary is-small is-rounded is-pulled-right">
                <span v-if="form.isSaving" class="icon is-loading is-small"></span>
                SAVE
              </button>
              <button type="button" class="button is-danger is-small is-rounded is-pulled-right mr-half"
                @click="cancel()">
                CANCEL
              </button>
            </div>
          </div>
        </div>
      </form>


    </div>
  </profile-form>
</div>
@endsection
@if(request()->ajax() === false)
@endsection
@endif