<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ env('APP_NAME')}}</title>
  <!-- CSS -->
  <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
  <section class="hero is-medium is-primary is-bold is-fullheight" style="background: url(images/edificius.jpg);
  background-size: cover;">

    <div class="container" style="width:100%; margin-top: 3rem;">
      <h1 class="title  is-size-4">
        This site is in construction.
      </h1>
      <h2 class="subtitle">
        Are you a real estate investor ? you want to buy or sell a commercial or residential building ? take contact
        with us. Please contact us on : info@edificius.ch.
      </h2>
      <br>
      <br>
      <h1 class="title is-size-4">
        Ce site est en construction.
      </h1>
      <h2 class="subtitle">
        Vous êtes un investisseur en immobilier ? vous souhaitez acheter ou vendre un immeuble commercial ou
        résidentiel ? prenez contact avec nous : info@edificius.ch.
      </h2>
      <br>
      <br>
      <div class="has-text-centered">
        <a href="/users" class="button is-blue is-rounded">ADMIN HOSTPOINT</a>
      </div>
    </div>


  </section>

</body>

</html>