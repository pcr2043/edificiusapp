@component('mail::message')


@if($admin !== null)
<h2 class="tac">@cnt('hello')&nbsp;{{ $admin->name}},</h2>
<br>
@cnt('register.mail.admin')

<table class="data">
  <tbody>
    <tr>
      <td>Name</td>
      <td>{{ $user->name }}</td>
    </tr>

    @if(strlen($user->meta['company']))
    <tr>
      <td>Company</td>
      <td>{{ $user->meta['company'] }}</td>
    </tr>
    @endif
    <tr>
      <td>Email</td>
      <td>{{ $user->email }}</td>
    </tr>
    <tr>
      <td>Mobile</td>
      <td>{{ $user->meta['mobile'] }}</td>
    </tr>
    <tr>
      <td>Slots</td>
      <td>@foreach($user->meta['bestTimeToCall'] as $slot)
        <div class="badge badge-slot"> {{ $slot}}</div>
        @endforeach
      </td>
    </tr>
    <tr>
      <td>Message</td>
      <td>
        {{ $user->meta['message'] }}
      </td>
    </tr>
  <tbody>
</table>
@else
<h2 class="tac">@cnt('hello')&nbsp;{{ $user->name}},</h2>
<br>
@cnt('register.mail')
@endif
<br>
<h5>@cnt('best.regards')<h5>
<h5>@cnt('edificius.team')</h5>


@endcomponent