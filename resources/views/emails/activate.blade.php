@component('mail::message')
<h2 class="tac">@cnt('hello')&nbsp;{{ $user->name}},</h2>
<br>
@cnt('activate.mail')
@if($password !== null)
<h5 class="tac">
@cnt('your.password.is'):&nbsp;{{ $password}}
</h5>
@cnt('password.activate.info')
<br>
@endif
@component('mail::button', ['url' => $user->authUrl()])
Login
@endcomponent
<br>
<h5>@cnt('best.regards')<h5>
<h5>@cnt('signature')</h5>

@endcomponent