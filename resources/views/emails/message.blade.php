@component('mail::message')

@if(isset($data['admin']))
<h3>Dear, Admin</h3>
<p>He receive a message from <strong>{{ $data['name'] }}</strong> thougth the website
</p>
@else
<h3>Dear, {{ $data["name"] }}</h3>
<p>
    You send a message thougth our website.
</p>
@endif

<table class="data">
    <tbody>
        <tr>
            <td>Name</td>
            <td>{{ $data['name'] }}</td>
        </tr>

        @if(strlen($data['company']))
        <tr>
            <td>Company</td>
            <td>{{$data['company'] }}</td>
        </tr>
        @endif
        <tr>
            <td>Email</td>
            <td>{{$data['email'] }}</td>
        </tr>
        <tr>
            <td>Phone</td>
            <td>{{ $data['phone'] }}</td>
        </tr>
        <tr>
            <td>Slots</td>
            <td>@foreach($data['bestTimeToCall'] as $slot)
                <div class="badge badge-slot"> {{ $slot}}</div>
                @endforeach
            </td>
        </tr>
        <tr>
            <td>Message</td>
            <td>
                {{ $data['message'] }}
            </td>
        </tr>
    <tbody>
</table>

<br>
<h5>@cnt('best.regards')<h5>
        <h5>@cnt('edificius.team')</h5>
        @endcomponent