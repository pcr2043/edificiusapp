@component('mail::message')
<h2 class="tac">@cnt('hello')</h2>
<br>
<h2 class="tac">>An error as been produced on the app {{ env('APP_NAME') }}</h2>
<br>
<h3 class="tac">{{ $message }}</h3>
<br>
<blockquote>
    {{ json_encode($dump) }}
</blockquote>
<br>
<br>
<br>
<h2 class="tac">
    @cnt('thank.you.for.chosing.us'),<br></h2>
<h2 class="tac">
    {{ config('app.name') }}</h2>

@endcomponent