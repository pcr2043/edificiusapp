@component('mail::message')
<h3>Dear, {{ $user->name }}</h3>
<p>
    You have requested the password recover instrunctions thougth our website.
</p>
<p>
    Please click in the follow link to access your profile and set a new password
</p>
@component('mail::button', ['url' => $user->recoverUrl()])
Reset Password
@endcomponent 

@component('mail::greetings')
@endcomponent

@endcomponent