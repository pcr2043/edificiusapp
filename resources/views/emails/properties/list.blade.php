@component('mail::message')
<h2><span class="badge badge-primary">{{ count($properties) }}</span> @cnt('new.properties.found') <span class="float-right"><h2>{{ Carbon::now()->format('d F Y')}}</h2></span></h2>
<br>
<h2 class="tac">@cnt('hello'), {{ $subscriber->email }}</h2> 
<h2 class="tac">@cnt('list.properties.today.quote')</h2>
<br>


@if($help)
<blockquote class="blockquote">
    <a href="{{ $utils->getPropertyMailPreview($properties, $subscriber->id, $documents) }}">
        @cnt('message.email.download.help')
    </a>
</blockquote>
@endif

@foreach($properties as $property)
@component('mail::property', ['property' => $property, 'documents' => (isset($documents) ? $documents : false)]) @endcomponent<br><br>
@endforeach

@if($help)
<blockquote class="blockquote">
    <a href="{{ $utils->getPropertyMailPreview($properties, $subscriber->id, $documents) }}">
        @cnt('message.email.download.help')
    </a>
</blockquote>
@endif
 
<h2 class="tac">
@cnt('thank.you.for.chosing.us'),<br></h2><h2 class="tac">
{{ config('app.name') }}</h2>

@endcomponent
