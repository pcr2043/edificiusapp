<table class="table is-fullwidth" @keypress="table.key($event)">
  <thead>
    <tr>
      <td colspan="5">
        <button class="button is-info is-small is-rounded ml-5px" @click="create()">
          <icon :icon="['fas', 'plus']" />

        </button>

        <button v-if="trash.length > 0" class="button is-danger is-small is-rounded"
          @click="toastr.confirm('ARE YOU SURE ABOUT DELETING THIS COMPANIES',`Bus.$emit('on-contacts-delete')`)">
          <icon :icon="['fas', 'trash']" />

        </button>

        <a v-if="table.isLoading" class="is-loading"></a>
      </td>
      <td colspan="4" v-show="mode !== 3">
        <vinput v-model="table.params.search" :event="search" name="search" placeholder="Type something" />
      </td>
    </tr>
    <tr class="no-select">
      <th class="w-5">
        <span class="start">
          <Checkbox v-model="table.selectAll" @click="table.select($event)" />
        </span>
      </th>
      <th class="w-10" v-show="mode < 1">
        <span class="sort start" :class="table.sortValue('id')" @click="table.sort('id')">Id</span>
      </th>
      <th class="w-15">
        <span class="sort" :class="table.sortValue('lastname')" @click="table.sort('lastname')">Familly Name</span>
      </th>
      <th class="w-15">
        <span class="sort" :class="table.sortValue('firstname')" @click="table.sort('firstname')">Firstname</span>
      </th>
      <th class="w-20">
        <span class="sort" :class="table.sortValue('company_id')" @click="table.sort('company_id')">Firm</span>
      </th>
      <th v-show="mode !== 3" class="w-20">
        <span class="sort" :class="table.sortValue('phone')" @click="table.sort('phone')">Phone</span>
      </th>

      <th class="w-20">
        <span>Action</span>
      </th>
    </tr>
  </thead>

  <tbody>
    <tr v-for="(row, index) in table.paginate.data" :key="row.id" :class="{ 'is-selected': row.selected }">
      <td>
        <Checkbox v-model="row.selected" />
      </td>
      <td v-show="mode < 1" v-text="row.id"></td>
      <td class="tac" v-text="row.lastname"></td>
      <td class="tac" v-text="row.firstname"></td>
      <td>
        <div class="tags are-small is-centered">
          <span class="tag is-info" v-if="row.company_id" v-text="row.company.name"></span>
        </div>
      </td>
      <td v-show="mode !== 3" class="tac" v-text="row.meta.phone"></td>


      <td>
        <button class="button is-primary is-small is-pulled-right is-rounded ml-5px" @click="edit(row, index)">
          <icon :icon="['fas', 'pencil-alt']" />
        </button>

        <button class="button is-danger is-small is-pulled-right is-rounded"
          @click="toastr.confirm('ARE YOU SURE ABOUT DELETING THIS COMPANY',`Bus.$emit('on-contact-delete', ${row.id})`)">
          <icon :icon="['fas', 'trash']" />
        </button>

      </td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <th colspan="3">
        <nav class="pagination is-rounded is-small" role="navigation" aria-label="pagination">
          <ul class="pagination-list">
            <li v-for="size in table.sizes" @click="table.setPaging(size)"><a class="pagination-link"
                :class="{'is-current' : table.paging == size }" v-text="size"></a>
            </li>
          </ul>

        </nav>

      </th>
      <th class="tac" colspan="2">
        <p class="results" v-show="table.paginate.total !== undefined">
          @{{ '@cnt('paging')' | paging(table.paginate)}}
        </p>
      </th>
      <th class="tar" colspan="2" v-show="mode !== 3">


        <a @click="table.prev()" :disabled=" 1 == table.paginate.current_page "
          class="pagination-previous button is-small is-rounded  has-text-dark"
          :class="{'is-info' :  table.paginate.current_page > 1 }">Previous</a>
        <a @click="table.next()" class="pagination-next  is-rounded button is-small has-text-dark"
          :disabled=" table.paginate.last_page == table.paginate.current_page "
          :class="{'is-info' :  table.paginate.last_page > table.paginate.current_page }">Next</a>

      </th>
    </tr>
  </tfoot>
</table>