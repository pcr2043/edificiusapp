<section>
  {{-- <div v-if="table.form.isLoading && table.form.model != null"> --}}
  <div v-if="!table.form.isLoading && table.form.model != null">
    <form @submit.prevent="submit()" @keypress="table.form.clearError($event.target.name)">
      <!-- TABS    @click="table.form.clearErrorByTarget($event.target)" -->
      <div class="columns is-multiline">
        <div class="column is-half">
          <vselect placeholder="Choose a Company" v-model="table.form.model.company_id" field="name"
            :items="{{ App\Company::all()->toJson() }}" label="Company" name="company_id"
            :error="table.form.getError('company_id')" />
        </div>

        <div class="column is-half">
          <Active v-model="table.form.model.status" label="status" name="status"
            :error="table.form.getError('status')" />
        </div>

        <div class="column is-half">
          <vinput v-model="table.form.model.firstname" label="Firstname" name="firstname"
            placeholder="Enter the Firstname" :error="table.form.getError('firstname')" />
        </div>

        <div class="column is-half">
          <vinput v-model="table.form.model.lastname" label="Lastname" name="lastname" placeholder="Enter the Lastname"
            :error="table.form.getError('lastname')" />
        </div>

        <div class="column is-half">
          <vinput v-model="table.form.model.email" label="Email" name="email" placeholder="Enter the Email"
            :error="table.form.getError('email')" />
        </div>

        <div class="column is-half">
          <vinput v-model="table.form.model.meta.phone" label="Phone" name=meta.phone placeholder="Enter the Phone"
            :error="table.form.getError('meta.phone')" />
        </div>

        <div class="column is-half">
          <vinput v-model="table.form.model.meta.phone_2" label="Phone 2" name="meta.phone_2"
            placeholder="Enter the Phone 2" :error="table.form.getError('meta.phone_2')" />
        </div>

        <div class="column is-half">
          <vinput v-model="table.form.model.meta.email" label="Email" name="meta.email" placeholder="Enter the Email"
            :error="table.form.getError('meta.email')" />
        </div>

        <div class="column is-full">
          <vinput :multiline="true" v-model="table.form.model.meta.comments" label="Comments" name="meta.comments"
            placeholder="Enter Comments" :error="table.form.getError('meta.comments')" />
        </div>
      </div>

      <div class="columns">
        <div class="column is-full">
          <div class="field">
            <button :disabled="table.form.isSaving" type="submit"
              class="button is-primary is-small is-rounded is-pulled-right">
              <span v-if="table.form.isSaving" class="icon is-loading is-small"></span>
              SAVE
            </button>
            <button type="button" class="button is-danger is-small is-rounded is-pulled-right mr-half"
              @click="cancel()">
              CANCEL
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>