<table class="w-100">
    <tr>
        <td>

            @if(array_key_exists(app()->getLocale(), $property->title) && strlen($property->title[app()->getLocale()] )> 0)
            <h3 class="text-center mb-0">{{ $property->title[app()->getLocale()] }}</h3>
            @else
            <h3 class="text-center mb-0">{{ $property->title[$utils->defaultLocale()] }}</h3>
            @endif
        </td>
    </tr>
    <tr>
        <td class="text-center">
            <a href="{{ $property->url }}">
                {{-- IMAGE WRAPPER --}}
                <div class="image-wrapper">
                        <div class="badge badge-primary badge-price">{{ $utils->million($property->price).' '.env('CURRENCY_DEFAULT') }}</div>
                        <div class="badge badge-gross text-justify">
                            <span class="badge-65">
                            <span class="label">
                                {{  $utils->translate('annual.income') }} 
                            </span> 
                            <span class="value">{{ $utils->million($property->gross_income).' '.env('CURRENCY_DEFAULT') }}
                            </span>
                            </span>
                                
                            <span class="badge-35">
                            <span class="label">{{  $utils->translate('gross.yield') }} </span> <span class="value">{{ $utils->decimal($property->gross_yield, 2) }}%</span>
                            </span>
                          </div>
                        
                
                     
                    <img src="{{$property->thumbnail }}" alt="image" />
                    <div class="badge-data">
                            @if($property->location != null && count($property->location['cantons'])> 0)
                            <span class="badge badge-success bagde-canton">{{ $utils->cantonsList()[$property->location['cantons'][0]]}}</span>@endif

                            
                            @if($property->local_env == 0)
                            <span class="badge badge-dark badge-env">{{ $utils->translate($utils->environmentPositions()[$property->local_env])}}</span>                        @endif @if($property->local_env == 1)
                            <span class="badge badge-dark badge-env">{{ $utils->translate($utils->environmentPositions()[$property->local_env])}}</span>                        @endif @if($property->local_env == 2)
                            <span class="badge badge-info badge-env">{{ $utils->translate($utils->environmentPositions()[$property->local_env])}}</span>                        @endif 
                            
                        </div>
                       
                      
                        @if(count($property->shared) > 0 && $property->documents_status['ready'] && isset($documents) && $documents == true)
                        <div>

                       
                            <h3 class="text-center mt-1">Downloads</h3>
                            <ul class="downloads">
                                @foreach($property->shared  as  $file)
                                        <li><a href="{{ env('APP_URL').'/s3/properties/shared/'.$property->id.'/'.$file['id'] }}"> <span class="name">{{ $file['name'] }} </span><span class="downl">@cnt('download')</span></a></li>
            @endforeach
            </ul>
            </div>
            @endif

            </div>
            </a>
        </td>
    </tr>
</table>