<section>
  {{-- <div v-if="table.form.isLoading && table.form.model != null"> --}}
  <div v-if="!table.form.isLoading && table.form.model != null">
    <form @submit.prevent="submit()" @keypress="table.form.clearError($event.target.name)">
      <!-- TABS    @click="table.form.clearErrorByTarget($event.target)" -->
      <div class="tabs is-centered">
        <ul>
          <li v-for="(tab, index) in tabs.items" :key="index" :class="{ 'is-active': tab.isActive }"
            @click="tabs.setActive(tab)">
            <a v-text="tab.name"></a>
          </li>
        </ul>
      </div>

      <!-- PROFILES -->
      <div class="columns is-multiline" :class="{ 'is-hidden': !tabs.items[0].isActive }">
        <div class="column is-half">
          <vinput v-model="table.form.model.firstname" label="FirstName" name="firstname"
            placeholder="Enter the Firstname" :error="table.form.getError('firstname')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.lastname" label="Lastname" name="lastname" placeholder="Enter the Lastname"
            :error="table.form.getError('lastname')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.email" label="Email" name="email" type="email" placeholder="Enter the Email"
            :error="table.form.getError('email')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.password" label="Password" name="password" type="password"
            placeholder="Enter the Password" :error="table.form.getError('password')" />
        </div>

        <div class="column is-half">
          <gender v-model="table.form.model.meta.gender" label="Gender" name="meta.gender"
            :error="table.form.getError('meta.gender')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.meta.birthdate" label="Birthdate" name="meta.birthdate" type="date"
            placeholder="Enter the Birthdate" :error="table.form.getError('meta.birthdate')" />
        </div>
        <div class="column is-two-quarters">
          <roles v-model="table.form.model.roles" label="Roles" name="roles" :error="table.form.getError('roles')" />
        </div>

        <div class="column is-one-quarter">
          <active v-model="table.form.model.isActive" label="Active" name="active"
            :error="table.form.getError('isActive')" />
        </div>
      </div>

      <!-- CONTACT -->
      <div class="columns is-multiline" :class="{ 'is-hidden': !tabs.items[1].isActive }">
        <div class="column is-half">
          <vinput v-model="table.form.model.meta.position" label="Position" name="meta.position"
            placeholder="Enter Position ..." :error="table.form.getError('meta.position')" />
        </div>

        <div class="column is-half">
          <vinput v-model="table.form.model.meta.company" label="Company" name="meta.company"
            placeholder="Enter Company ..." :error="table.form.getError('meta.company')" />
        </div>

        <div class="column is-half">
          <vinput v-model="table.form.model.meta.mobile" label="Mobile" name="meta.mobile"
            placeholder="Enter Mobile ..." :error="table.form.getError('meta.mobile')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.meta.phone" label="Phone" name="meta.phone" placeholder="Enter Phone ..."
            :error="table.form.getError('meta.phone')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.meta.fax" label="Fax" name="meta.fax" placeholder="Enter Fax ..."
            :error="table.form.getError('meta.fax')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.meta.website" label="Website" name="meta.website"
            placeholder="Enter Website ..." :error="table.form.getError('meta.website')" />
        </div>

        <div class="column is-two-thirds">
          <vinput v-model="table.form.model.meta.address.street" label="Street, place" name="meta.address.street"
            placeholder="Enter Addresss, Place ..." :error="table.form.getError('meta.address.street')" />
        </div>
        <div class="column is-one-third">
          <vinput v-model="table.form.model.meta.address.number" label="Number, floor" name="meta.address.number"
            placeholder="Enter Number, Floor ..." :error="table.form.getError('meta.address.number')" />
        </div>
        <div class="column is-half">
          <countries v-model="table.form.model.meta.address.country" label="Country" name="meta.address.country"
            placeholder="Pick Country" :error="table.form.getError('meta.address.country')" />
        </div>
        <div class="column is-one-quarter">
          <vinput v-model="table.form.model.meta.address.postcode" label="Postcode" name="meta.address.postcode"
            placeholder="Enter Postcode" :error="table.form.getError('meta.address.postcode')" />
        </div>
        <div class="column is-one-quarter">
          <vinput v-model="table.form.model.meta.address.town" label="Town" name="meta.address.town"
            placeholder="Enter Town" :error="table.form.getError('meta.address.town')" />
        </div>
        <div class="column is-half">
          <vinput v-model="table.form.model.meta.email" label="Company Email | Second Email" name="meta.email2"
            placeholder="Enter Email ..." :error="table.form.getError('meta.email')" />
        </div>

        <div class="column is-half">
          <div class="field timeslot-field">
            <label class="label is-small">@cnt('form.best_time_to_call')</label>
            <div class="buttons has-addons timeslots">
              <span @click="setTimeToCall('8:00')" :class="{ 'is-info' : hasTimeSlot('8:00') }"
                class="button is-primary is-small is-rounded" role="button">&nbsp;8:00</span>
              <span @click="setTimeToCall('9:00')" :class="{ 'is-info' : hasTimeSlot('9:00') }"
                class="button is-primary is-small is-rounded" role="button">&nbsp;9:00</span>
              <span @click="setTimeToCall('10:00')" :class="{ 'is-info' : hasTimeSlot('10:00') }"
                class="button is-primary is-small is-rounded" role="button">10:00</span>
              <span @click="setTimeToCall('11:00')" :class="{ 'is-info' : hasTimeSlot('11:00') }"
                class="button is-primary is-small is-rounded" role="button">11:00</span>
              <span @click="setTimeToCall('12:00')" :class="{ 'is-info' : hasTimeSlot('12:00') }"
                class="button is-primary is-small is-rounded" role="button">12:00</span>
              <span @click="setTimeToCall('13:00')" :class="{ 'is-info' : hasTimeSlot('13:00') }"
                class="button is-primary is-small is-rounded" role="button">13:00</span>
            </div>
            <div class="buttons has-addons timeslots">
              <span @click="setTimeToCall('14:00')" :class="{ 'is-info' : hasTimeSlot('14:00') }"
                class="button is-primary is-small is-rounded" role="button">14:00</span>
              <span @click="setTimeToCall('15:00')" :class="{ 'is-info' : hasTimeSlot('15:00') }"
                class="button is-primary is-small is-rounded" role="button">15:00</span>
              <span @click="setTimeToCall('16:00')" :class="{ 'is-info' : hasTimeSlot('16:00') }"
                class="button is-primary is-small is-rounded" role="button">16:00</span>
              <span @click="setTimeToCall('17:00')" :class="{ 'is-info' : hasTimeSlot('17:00') }"
                class="button is-primary is-small is-rounded" role="button">17:00</span>
              <span @click="setTimeToCall('18:00')" :class="{ 'is-info' : hasTimeSlot('18:00') }"
                class="button is-primary is-small is-rounded" role="button">18:00</span>
              <span @click="setTimeToCall('19:00')" :class="{ 'is-info' : hasTimeSlot('19:00') }"
                class="button is-primary is-small is-rounded" role="button">19:00</span>
            </div>
          </div>
          <vinput :multiline="true" :rows="4" v-model="table.form.model.meta.motivation" label="@cnt('form.message')"
            name="meta.motivation" type="text" placeholder="Enter the Message"
            :error="table.form.getError('meta.motivation')" />
        </div>

      </div>

      <!-- DOCUMENTS -->
      <div class="columns is-multiline" :class="{ 'is-hidden': !tabs.items[2].isActive }">
        <!-- UPLOAD CONTROL -->
        <div class="column is-full">
          <upload v-model="table.form.model.documents" label="Click button to upload files" name="documents"
            event="user-document-upload" :extensions="['doc', 'docx', 'pdf', 'xls', 'xlsx']"
            placeholder="Upload Documents" :error="table.form.getError('meta.documents')" />
        </div>

        <!-- UPLOAD LIST -->
        <div class="column is-full">
          <div class="field">
            <label class="label is-small">Documents</label>

            <div class="control">
              <div class="list is-hoverable is-small is-files">
                <a v-for="(file, index) in table.form.model.documents" :key="index" class="list-item is-small">
                  @{{ file.name }}
                  <span class="icon has-background-primary has-text-white">
                    <icon :icon="['fas', 'cloud-download-alt']" @click="
                            fileManager.download(
                              'users',
                              table.form.model.id,
                              'documents',
                              index
                            )
                          " />
                  </span>
                  <span class="icon has-background-danger has-text-white"
                    @click="table.form.model.documents.splice(index, 1)">
                    <icon :icon="['fas', 'trash']" />
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="columns">
        <div class="column is-full">
          <div class="field">
            <button :disabled="uploadingDocuments || table.form.isSaving" type="submit"
              class="button is-primary is-small is-rounded is-pulled-right">
              <span v-if="table.form.isSaving" class="icon is-loading is-small"></span>
              SAVE
            </button>
            <button type="button" class="button is-danger is-small is-rounded is-pulled-right mr-half"
              @click="cancel()">
              CANCEL
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>