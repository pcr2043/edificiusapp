<ul class="nav">
  <a class="nav-link" href="/">{{  $utils->translate('home') }}<span class="sr-only">(current)</span></a>
  <a class="nav-link" href="/services">{{  $utils->translate('services') }}</a>
  <a class="nav-link" href="/properties">{{  $utils->translate('properties') }}</a>
  <a class="nav-link" href="/faq">{{  $utils->translate('faq') }}</a>
  <a class="nav-link" href="/contact">{{  $utils->translate('contact') }}</a>
  @if(auth()->check())
  <a class="nav-link" href="/administration">@cnt('administration')</a>
  <a class="nav-link" href="/logout"> @cnt('logout')</a>
  @else
  <a href="/login" class="nav-link"> @cnt('login')</a>
  @endif
</ul>

@include('partials.languages')