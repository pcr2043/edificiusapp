<div class="buttons has-addons languages">

  @foreach($utils->languages() as $language)
  <a class="button is-info is-small is-rounded {{ app()->getLocale() === $language ? 'is-active' : ''}}"
    href="/language/{{ $language }}" role="button">{{ $language }}</a>
  @endforeach

</div>