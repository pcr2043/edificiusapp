<Register inline-template>
  <div class="modal register-modal" :class="{ 'is-active' : isActive }">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header>
        <span>@cnt('register')</span>
        <span class="close" @click="cancel()">
          <i class="fas fa-times-circle"></i>
        </span>
      </header>
      <section class="modal-card-body">
        <div class="text">
          @cnt('register.text')
        </div>
        <form @keypress.enter="submit">
          <div class="columns is-multiline">
            <div class="column is-half">
              <vinput v-model="form.model.firstname" label="FirstName" name="firstname"
                placeholder="Enter the Firstname" :error="form.getError('firstname')" />
            </div>
            <div class="column is-half">
              <vinput v-model="form.model.lastname" label="Lastname" name="lastname" placeholder="Enter the Lastname"
                :error="form.getError('lastname')" />
            </div>
            <div class="column is-half">
              <vinput v-model="form.model.meta.mobile" label="Phone or Mobile" name="mobile"
                placeholder="Enter the Phone or Mobile" :error="form.getError('meta.mobile')" />
            </div>

            <div class="column is-half">
              <vinput v-model="form.model.meta.company" label="company" name="Company" placeholder="Enter the Company"
                :error="form.getError('meta.company')" />
            </div>
          </div>

          <div class="columns is-multiline">
            <div class="column is-half">
              <div class="columns is-multiline">
                <div class="column is-full">
                  <vinput v-model="form.model.email" label="Email" name="email" type="email"
                    placeholder="Enter the Email" :error="form.getError('email')" />
                </div>
                <div class="column is-full">
                  <vinput v-model="form.model.password" label="Password" name="password" type="password"
                    placeholder="Enter the Password" :error="form.getError('password')" />
                </div>
                <div class="column is-full">
                  @{{form.model.confirmPassword}}
                  <vinput v-model="form.model.password_confirmation" label="Password Confirm" name="confirmPassword"
                    type="password" placeholder="Enter the Password Confirm"
                    :error="form.getError('confirmPassword')" />
                </div>
              </div>
            </div>

            <div class="column is-half">
              <div class="columns is-multiline">

                <div class="column is-full">
                  <div class="field timeslot-field">
                    <label class="label is-small">@cnt('form.best_time_to_call')</label>
                    <div class="buttons has-addons timeslots">
                      <span @click="setTimeToCall('8:00')" :class="{ 'is-info' : hasTimeSlot('8:00') }"
                        class="button is-primary is-small is-rounded" role="button">&nbsp;8:00</span>
                      <span @click="setTimeToCall('9:00')" :class="{ 'is-info' : hasTimeSlot('9:00') }"
                        class="button is-primary is-small is-rounded" role="button">&nbsp;9:00</span>
                      <span @click="setTimeToCall('10:00')" :class="{ 'is-info' : hasTimeSlot('10:00') }"
                        class="button is-primary is-small is-rounded" role="button">10:00</span>
                      <span @click="setTimeToCall('11:00')" :class="{ 'is-info' : hasTimeSlot('11:00') }"
                        class="button is-primary is-small is-rounded" role="button">11:00</span>
                      <span @click="setTimeToCall('12:00')" :class="{ 'is-info' : hasTimeSlot('12:00') }"
                        class="button is-primary is-small is-rounded" role="button">12:00</span>
                      <span @click="setTimeToCall('13:00')" :class="{ 'is-info' : hasTimeSlot('13:00') }"
                        class="button is-primary is-small is-rounded" role="button">13:00</span>
                    </div>
                    <div class="buttons has-addons timeslots">
                      <span @click="setTimeToCall('14:00')" :class="{ 'is-info' : hasTimeSlot('14:00') }"
                        class="button is-primary is-small is-rounded" role="button">14:00</span>
                      <span @click="setTimeToCall('15:00')" :class="{ 'is-info' : hasTimeSlot('15:00') }"
                        class="button is-primary is-small is-rounded" role="button">15:00</span>
                      <span @click="setTimeToCall('16:00')" :class="{ 'is-info' : hasTimeSlot('16:00') }"
                        class="button is-primary is-small is-rounded" role="button">16:00</span>
                      <span @click="setTimeToCall('17:00')" :class="{ 'is-info' : hasTimeSlot('17:00') }"
                        class="button is-primary is-small is-rounded" role="button">17:00</span>
                      <span @click="setTimeToCall('18:00')" :class="{ 'is-info' : hasTimeSlot('18:00') }"
                        class="button is-primary is-small is-rounded" role="button">18:00</span>
                      <span @click="setTimeToCall('19:00')" :class="{ 'is-info' : hasTimeSlot('19:00') }"
                        class="button is-primary is-small is-rounded" role="button">19:00</span>
                    </div>
                  </div>
                  <vinput :multiline="true" :rows="4" v-model="form.model.meta.motivation" label="@cnt('form.message')"
                    name="meta.motivation" type="text" placeholder="Enter the Message"
                    :error="form.getError('meta.motivation')" />
                </div>



              </div>
            </div>


        </form>
      </section>
      <footer>
        <button type="submit" @click="submit()" class="button is-default is-small  is-rounded is-pulled-right">
          <span v-if="form.isSaving" class="icon is-loading is-small"></span>
          @cnt('register')</button>
        <button type="button" class="button is-danger is-small is-rounded is-pulled-right mr-5px" @click="cancel()"
          class="button">@cnt('cancel')</button>
      </footer>
    </div>
  </div>
</Register>