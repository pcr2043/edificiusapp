<div class="columns is-multiline property-details">
  <div class="column is-one-quarter">
    <img v-for="image in property.images" :src="`/data/properties/${image.path}`" :alt="`preview-${image.path}`">
  </div>
  <div class="column is-third-quarters">


    <div class="columns is-multiline ">

      {{-- TITLE  --}}
      <div class="column is-three-quarters tal">
        <div class="title" v-text="property.title[language]"></div>
      </div>
      <div class="column is-one-quarter tar">
        <button @click="back()" class="button is-primary is-small is-rounded is-uppercase">@cnt('back.results')</button>
      </div>

      {{-- DESCRIPTION --}}
      <div class="column is-full description" v-html="property.description[language]">

      </div>

      {{-- LOCATION --}}
      <div class="column is-full location-price">
        <div class="columns is-multiline">
          <div class="column is-half tal">
            <i class="fas fa-map-marker-alt property-marker"></i>
            <span v-text="cantons[property.meta.address.canton]"></span>
          </div>
          <div class="column is-half tar">
            <div class="field is-grouped is-grouped-multiline is-rounded is-pulled-right">
              <div class="control">
                <div class="tags has-addons">
                  <span class="tag is-rounded is-primary">@cnt('price')</span>
                  <span class="tag is-rounded is-info has-text-dark price">
                    @{{ property.price | currency('CHF')}}
                  </span>
                </div>
              </div>

            </div>
          </div>
        </div>

      </div>


      {{-- INFO --}}

      <div class="column is-half">
        <div class="columns is-multiline no-margin">
          <div class=" column is-full column-title">
            @cnt('info')
          </div>
          {{-- TYPE --}}
          <div class="column is-half column-key"> @cnt('type')</div>
          <div class="column is-half column-value" v-text="property.typeProperty"></div>

          {{-- ENVIRONMENT --}}
          <div class="column is-half column-key"> @cnt('environment')</div>
          <div class="column is-half column-value" v-text="property.typeEnvironment"></div>

          {{-- OWNER --}}
          <div class="column is-half column-key"> @cnt('rent.owner')</div>
          <div class="column is-half column-value" v-text="property.typeOwner"></div>


        </div>
      </div>

      {{-- AMMENITIES --}}
      <div class="column is-half">
        <div class="columns is-multiline no-margin">
          <div class="column is-full column-title">
            @cnt('property.amenities')
          </div>
          {{-- APARTMENTS --}}
          <div class="column is-half column-key"> @cnt('apartments')</div>
          <div class="column is-half column-value" v-text="property.apartments"></div>

          {{-- STORES --}}
          <div class="column is-half column-key"> @cnt('stores')</div>
          <div class="column is-half column-value" v-text="property.stores"></div>


          {{-- OFFICES --}}
          <div class="column is-half column-key"> @cnt('offices')</div>
          <div class="column is-half column-value" v-text="property.offices"></div>
        </div>
      </div>



      {{-- FIGURES --}}

      <div class="column is-half">
        <div class="columns is-multiline no-margin">
          <div class=" column is-full column-title">
            @cnt('figures')
          </div>
          {{-- GROSS INCOME --}}
          <div class="column is-half column-key"> @cnt('gross.income')</div>
          <div class="column is-half column-value">

            @{{ property.gross_income | million('CHF') }}

          </div>

          {{-- ANNUAL CHARGES --}}
          <div class="column is-half column-key">@cnt('annual.charges')</div>
          <div class="column is-half column-value">
            @{{ property.annual_charges |  million('CHF') }}
          </div>

          {{-- NET INCOME --}}
          <div class="column is-half column-key">@cnt('net.income')</div>
          <div class="column is-half column-value">
            @{{ property.net_income | million('CHF') }}
          </div>


        </div>
      </div>


      {{-- INCOME BREAK DOWN --}}

      <div class="column is-half">
        <div class="columns is-multiline no-margin">
          <div class=" column is-full column-title">
            @cnt('income.break.down')
          </div>
          {{-- PRIVATE RENT --}}
          <div class="column is-half column-key"> @cnt('private.rent')</div>
          <div class="column is-half column-value">

            @{{ property.private_rent | million('CHF')}}

          </div>

          {{-- GALLERIES RENT --}}
          <div class="column is-half column-key">@cnt('galleries.rent')</div>
          <div class="column is-half column-value">
            @{{ property.galleries_rent | million('CHF')}}
          </div>

          {{-- OFFICES RENT --}}
          <div class="column is-half column-key">@cnt('professional.rent')</div>
          <div class="column is-half column-value">
            @{{ property.professional_rent | million('CHF') }}
          </div>


        </div>
      </div>


      {{-- INCOME BREAK DOWN --}}

      <div class="column is-full">
        <div class="columns is-multiline no-margin">
          <div class=" column is-full column-title">
            @cnt('analytics')
          </div>
          {{-- GROSS YIELD  --}}
          <div class="column is-half column-key">@cnt('gross.yield') %</div>
          <div class="column is-half column-value">

            @{{ property.gross_yield | decimal(2,'%')}}

          </div>

          {{-- NET YEILD --}}
          <div class="column is-half column-key">@cnt('net.yield')</div>
          <div class="column is-half column-value">
            @{{ property.net_yield | decimal(2,'%')}}
          </div>

          {{-- CHARGES INCOME --}}
          <div class="column is-half column-key">@cnt('charges') / @cnt('income') %</div>
          <div class="column is-half column-value">
            @{{ (property.annual_charges / property.gross_income * 100) | decimal(2,'%') }}
          </div>



          {{-- INCOME SQM --}}
          <div class="column is-half column-key">@cnt('income') / @cnt('sqm')</div>
          <div class="column is-half column-value">
            @{{ property.gross_income_m2 | decimal(2,'CHF') }}
          </div>


          {{-- INCOME APPARTEMENTS --}}
          <div class="column is-half column-key">@cnt('income') / @cnt('apartments') %</div>
          <div class="column is-half column-value">
            @{{ property.meta.property.apartments.income_perc | decimal(2,'%')}}
          </div>

          {{-- INCOME  STORES--}}
          <div class="column is-half column-key">@cnt('income') / @cnt('stores') %</div>
          <div class="column is-half column-value">
            @{{ property.meta.property.stores.income_perc |decimal(2,'%')}}
          </div>

          {{-- INCOME OFFICES--}}
          <div class="column is-half column-key">@cnt('income') / @cnt('offices') %</div>
          <div class="column is-half column-value">
            @{{ property.meta.property.offices.income_perc| decimal(2,'%')}}
          </div>



          {{-- AVERAGE INCOME APPARTEMETNS--}}
          <div class="column is-half column-key">@cnt('average.income') / @cnt('apartments')</div>
          <div class="column is-half column-value">
            @{{ property.meta.property.apartments.price_year | million('CHF')   }}
          </div>

          {{-- AVERAGE INCOME STORES--}}
          <div class="column is-half column-key">@cnt('average.income') / @cnt('stores')</div>
          <div class="column is-half column-value">
            @{{ property.meta.property.stores.price_year | million('CHF')   }}
          </div>

          {{-- AVERAGE INCOME OFFICES--}}
          <div class="column is-half column-key">@cnt('average.income') / @cnt('offices')</div>
          <div class="column is-half column-value">
            @{{ property.meta.property.offices.price_year | million('CHF')   }}
          </div>




          {{-- AVERAGE INCOME APPARTEMETNS SQM--}}
          <div class="column is-half column-key">@cnt('apartments') / @cnt('income') / @cnt('sqm')</div>
          <div class="column is-half column-value">
            @{{ property.meta.property.apartments.price_m2_year | million('CHF')}}
          </div>

          {{-- AVERAGE INCOME STORES SQM--}}
          <div class="column is-half column-key">@cnt('stores') / @cnt('income') / @cnt('sqm')</div>
          <div class="column is-half column-value">
            @{{ property.meta.property.stores.price_m2_year | million('CHF')}}
          </div>


          {{-- AVERAGE INCOME OFFICES SQM--}}
          <div class="column is-half column-key">@cnt('offices') / @cnt('income') / @cnt('sqm')</div>
          <div class="column is-half column-value">
            @{{ property.meta.property.offices.price_m2_year | million('CHF')}}
          </div>



        </div>
      </div>



      {{-- $property->typeProperty = $utils->PropertyTypes()[0];
      $property->typeEnvironment =  $utils->EnvPlaces()[0];
      $property->typeOwner = $utils->TypeRents()[0]; --}}
    </div>
  </div>
</div>