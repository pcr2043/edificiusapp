<faq-list :faq-data="{{ $faqs->toJson() }}" language="{{ app()->getLocale() }}" inline-template>
  <div class="columns is-multiline steps mb-5">
    <div class="column is-full">
      <div class="centered-title">@cnt('faq')</div>
      <hr>
    </div>
    <div class="column is-full">
      <div @click="setFaq(index)" v-for="(faq, index) in faqs" class="custom-panel"
        :class="{ 'is-active' :  faq.selected }">
        <p class="heading">
          @{{ faq.meta[language]['question'] }}
        </p>
        <div class="block" v-html="faq.meta[language]['answer']">
        </div>
      </div>
    </div>
  </div>
</faq-list>