<div class="tags has-addons mt-1">
  <span :class="{'is-inactive' :  mode !== 0 && mode !== -1  }" class="tag {{ $cssClass }}" @click="setMode(0)">FULL
    LEFT</span>
  <span :class="{'is-inactive' :  mode !==1 }" class="tag {{ $cssClass }}" @click="setMode(1)">TWO THIRDS</span>
  <span :class="{'is-inactive' :  mode !==2 }" class="tag {{ $cssClass }}" @click="setMode(2)">HALF</span>
  <span :class="{'is-inactive' :  mode !==3 }" class="tag {{ $cssClass }}" @click="setMode(3)">TWO THIRDS LEFT</span>
  <span :class="{'is-inactive' :  mode !==4 }" class="tag {{ $cssClass }}" @click="setMode(4)">FULL RIGHT</span>
</div>