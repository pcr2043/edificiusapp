<div class="columns is-multiline mt-5 mb-5 steps">
  <div class="column is-full">
    <div class="centered-title">@cnt('how_it_works')</div>
    <hr>
  </div>

  <div class="column is-one-quarter">
    <span>1</span>
    {{-- <img src="/images/howitworks/investor.svg" alt="start invensting with us"> --}}
    <p> You are looking to invest in Business Real State </p>
    <p> Stores, Restaurants, Entire Building, Apartements</p>

  </div>

  <div class="column is-one-quarter">
    <span>2</span>
    {{-- <img src="/images/howitworks/account.svg" alt="start invensting with us"> --}}
    <p> Start by creating an an account with us</p>
  </div>

  <div class="column is-one-quarter">
    <span>3</span>
    {{-- <img src="/images/howitworks/agent.svg" alt="start invensting with us"> --}}
    <p> Our Business Agent will get in touch with you</p>
  </div>

  <div class="column is-one-quarter">
    <span>4</span>
    {{-- <img src="/images/howitworks/investment.svg" alt="start invensting with us"> --}}
    <p> You get Access to our Business Properties Analitycs Data</p>
    <p> We will help you to find the right investment for you</p>
  </div>



  <div class="column is-full is-centered mt-3">
    <button onclick="showRegister()" type="button" class="button is-primary  is-rounded  has-text-weight-bold"
      onclick="showRegister">
      CREATE AN ACCOUNT
    </button>
  </div>
</div>