<property-list inline-template language="{{ app()->getLocale() }}" default-locale="{{ $utils->language() }}"
  :properties="{{ $utils->top(8) }}" :environment-places="{{ json_encode($utils->EnvPlaces()) }}"
  :list-cantons="{{ json_encode($utils->ListCantonsSimple()) }}">
  <div class="columns is-multiline">
    <div class="column is-full">
      <div class="centered-title">@cnt('featured.title')</div>
      <hr>
    </div>
    <property-two v-for="(property, index) in properties" :key="index" :property="property" css-class="horizontal"
      language="{{ app()->getLocale() }}" flow="page" default-language="{{ $utils->language() }}"
      :canton="listCantons[property.meta.address.canton]" />
  </div>
</property-list>