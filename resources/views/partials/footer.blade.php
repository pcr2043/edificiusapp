<footer>
  <div class="columns is-multiline">
    <div class="column is-one-third">
      <div class="centered-title has-text-white">@cnt('sitemap')</div>
      @include('partials.sitemap')
    </div>

    <div class="column is-offset-one-third is-one-third social">
      <nav class="nav">
        <a href="#">
          <i class="fas fa-envelope"></i>
        </a>
        <a href="#">
          <i class="fab fa-twitter"></i>
        </a>
        <a href="#">
          <i class="fab fa-linkedin"></i>
        </a>
      </nav>

      <div class="address text-white mt-3">
        @cnt('contact.details')
      </div>



    </div>

  </div>
  <div class="column is-full has-text-white copyright">
    all rights reserved to <strong
      class="has-text-info">{{ env('APP_NAME') }}</strong>&nbsp;&#174;&nbsp;{{ $utils->Year() }}
  </div>
</footer>