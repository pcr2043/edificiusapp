<div class="columns is-multiline is-gapless search">
  <div class="column is-full">
    <div class="title">
      @cnt('search')
      <span @click="collapseSearch()">
        <i class="fas fa-sliders-h collapse"></i>
      </span>
    </div>
  </div>
  <div class="column is-full">
    <div class="small">@cnt('choose-canton')</div>
    <div class="columns is-multiline is-gapless search">
      <div class="column is-half-mobile is-half">


        <Checkbox :inline="true" :click-event="cantonCheck" :label="listCantons.all" :val="'all'"
          v-model="table.params.cantons"></Checkbox>
        <Checkbox :inline="true" :click-event="cantonCheck" :label="listCantons.sr" :val="'sr'"
          v-model="table.params.cantons"></Checkbox>
        <Checkbox :inline="true" :click-event="cantonCheck" :label="listCantons.sa" :val="'sa'"
          v-model="table.params.cantons"></Checkbox>
        <Checkbox :inline="true" :click-event="cantonCheck" :label="listCantons.si" :val="'si'"
          v-model="table.params.cantons"></Checkbox>

        <Checkbox :click-event="cantonCheck" v-for="canton in Object.keys(listCantons['Suisse Rommande'])" :key="canton"
          :inline="true" :val="canton" :label="listCantons['Suisse Rommande'][canton]" v-model="table.params.cantons">
        </Checkbox>

        <Checkbox :click-event="cantonCheck" v-for="canton in Object.keys(listCantons['Suisse Alemanique']).slice(0,5)"
          :key="canton" :inline="true" :val="canton" :label="listCantons['Suisse Alemanique'][canton]"
          v-model="table.params.cantons">
        </Checkbox>


      </div>
      <div class="column is-half">
        <Checkbox :click-event="cantonCheck" v-for="canton in Object.keys(listCantons['Suisse Alemanique']).slice(5)"
          :key="canton" :inline="true" :val="canton" :label="listCantons['Suisse Alemanique'][canton]"
          v-model="table.params.cantons">
        </Checkbox>
        <Checkbox :click-event="cantonCheck" v-for="canton in Object.keys(listCantons['Suisse Italienne'])"
          :key="canton" :inline="true" :val="canton" :label="listCantons['Suisse Italienne'][canton]"
          v-model="table.params.cantons">
        </Checkbox>
      </div>
    </div>

  </div>
  <div class="column is-full">

    <div class="small">@cnt('price') Range: <input type="text" id="amount" readonly></div>

    <div id="slider-range"></div>
  </div>



  <div class="column is-full">

    <div class="small">@cnt('rent.owner')</div>
    <div class="columns">
      @foreach($utils->TypeRents() as $rent)
      <div class="column is-one-third">
        <Checkbox :inline="true" :click-event="search" label="{{ $rent }}" val="{{$rent}}"
          v-model="table.params.typeOwner"></Checkbox>
      </div>
      @endforeach
    </div>
  </div>

  <div class="column is-full">

    <div class="small">@cnt('environment')</div>
    <div class="columns">
      @foreach($utils->EnvPlaces() as $place)
      <div class="column is-one-third">
        <Checkbox :inline="true" :click-event="search" label="{{ $place }}" val="{{$place}}"
          v-model="table.params.typeEnvironment">
        </Checkbox>
      </div>
      @endforeach
    </div>
  </div>


  <div class="column is-full">

    <div class="small">@cnt('sort')</div>
    <div class="columns is-multiline sorts">
      <div class="column  is-half-mobile is-half">
        <div class="field is-grouped is-grouped-multiline">
          <div class="control" @click="table.sort('price', false)">
            <div class="tags has-addons">
              <span class="tag is-primary">@cnt('price')</span>
              <span class="tag is-info has-text-dark">
                <span v-show="table.sorts['price'] =='asc'">
                  <i class="fas fa-angle-up"></i>
                </span>
                <span v-show="table.sorts['price'] =='desc'">
                  <i class="fas fa-angle-down"></i>
                </span>
              </span>
            </div>
          </div>
        </div>
      </div>


      <div class="column  is-half-mobile is-half">
        <div class="field is-grouped is-grouped-multiline">
          <div class="control" @click="table.sort('gross_yield', false)">
            <div class="tags has-addons">
              <span class="tag is-primary">@cnt('gross.yield')</span>
              <span class="tag is-info has-text-dark">

                <span v-show="table.sorts['gross_yield'] =='asc'">
                  <i class="fas fa-angle-up"></i>
                </span>
                <span v-show="table.sorts['gross_yield'] =='desc'">
                  <i class="fas fa-angle-down"></i>

                </span>
              </span>
            </div>
          </div>
        </div>
      </div>

      <div class="column is-half-mobile is-half">
        <div class="field is-grouped is-grouped-multiline">
          <div class="control" @click="table.sort('created_at', false)">
            <div class="tags has-addons">
              <span class="tag is-primary">@cnt('published.date')</span>
              <span class="tag is-info has-text-dark">

                <span v-show="table.sorts['created_at'] =='asc'">
                  <i class="fas fa-angle-up"></i>
                </span>
                <span v-show="table.sorts['created_at'] =='desc'">
                  <i class="fas fa-angle-down"></i>
                </span>

              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="column  is-half-mobile is-half">
        <div class="field is-grouped is-grouped-multiline">
          <div class="control" @click="table.sort('year', false)">
            <div class="tags has-addons">
              <span class="tag is-primary">@cnt('construction.date')</span>

              <span class="tag is-info has-text-dark">
                <span v-show="table.sorts['year'] =='asc'">
                  <i class="fas fa-angle-up"></i>
                </span>
                <span v-show="table.sorts['year'] =='desc'">
                  <i class="fas fa-angle-down"></i>
                </span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <div class="column is-full">

    <div class="small">@cnt('page.size')</div>

    <nav class="pagination is-rounded" role="navigation" aria-label="pagination">
      <ul class="pagination-list">
        <li v-for="size in table.sizes" @click="table.setPaging(size)"><a class="pagination-link"
            :class="{'is-current' : table.paging == size }" v-text="size"></a>
        </li>
      </ul>
      <a @click="table.prev()" :disabled=" 1 == table.paginate.current_page "
        class="pagination-previous button is-small  has-text-dark"
        :class="{'is-info' :  table.paginate.current_page > 1 }">Previous</a>
      <a @click="table.next()" class="pagination-next  button is-small has-text-dark"
        :disabled=" table.paginate.last_page == table.paginate.current_page "
        :class="{'is-info' :  table.paginate.last_page > table.paginate.current_page }">Next</a>
    </nav>

    <p class="results" v-show="table.paginate.total !== undefined">
      @{{ pagingText  | paging(table.paginate)}}
    </p>
  </div>

  <div class="column is-full mt-2">

    <button @click="searchHidden = true" class="button is-primary is-rounded is-fullwidth">CLOSE</button>
  </div>
</div>