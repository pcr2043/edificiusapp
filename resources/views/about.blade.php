@if(request()->ajax() === false)
@extends('master')
@section('content')
@endif

@section('content')
<div class="columns is-multiline">
  <div class="column is-full">
    <div class="centered-title">@cnt('about.us')</div>
  </div>
  <div class="column is-full">
    @cnt('about.html')
  </div>
</div>
@if(request()->ajax() === false)
@endsection
@endif