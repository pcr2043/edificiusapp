@if(request()->ajax() === false)
@extends('master')
@section('content')
@endif

@section('content')
<search inline-template language="{{ $utils->language() }}" :list-cantons="{{ $utils->ListCantons(true) }}"
  :cantons="{{ $utils->ListCantonsSimple(true) }}" paging-text="@cnt('paging')"
  :property-data="{{ isset($property) ? $property->toJson() : json_encode((object)[]) }}">
  <div class="columns is-multiline">

    {{-- SEARCH CONTROL --}}
    <div v-show="property === null" class="column is-one-quarter search-column" :class="{ 'hidden' : searchHidden }">
      @include('partials.search')
    </div>
    {{-- PROPERTY LIST --}}
    <div v-show="property === null" class="column is-third-quarters">
      <div class="columns is-multiline">
        <div class="column is-half-tablet is-one-third-desktop is-one-quarter-widescreen"
          v-for="property in table.paginate.data" :key="property.id">
          <property-v :canton="cantons[property.meta.address.canton]" :property="property"></property-v>
        </div>
      </div>
    </div>
    <div v-if="property !== null" class="column is-full">
      @include('partials.property')
    </div>
  </div>
</search>
@if(request()->ajax() === false)
@endsection
@endif