@if(request()->ajax() === false)
@extends('master')
@section('content')
@endif

@section('content')
<div>
  @include('partials.opportunities')
  @include('partials.steps')
  @include('partials.faq')
</div>
@if(request()->ajax() === false)
@endsection
@endif