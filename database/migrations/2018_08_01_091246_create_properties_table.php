<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {

            $table->increments('id');

            $table->string("title");

            $table->text("alias")->nullable();
            $table->text("description")->nullable();



            $table->tinyInteger('isActive')->default(0);
            $table->tinyInteger('isPublished')->default(0);
            $table->tinyInteger('isFeatured')->default(0);

            //building
            //store
            //office
            //house
            //garage

            $table->string("typeProperty");

            //city, village, country side
            $table->string("typeEnvironment");

            //Type of rent  private
            $table->string("typeOwner");

            // Price Output and Revenue

            $table->decimal("price", 16, 2)->default(0);

            //gross income

            $table->decimal("gross_income", 16, 2)->default(0);

            $table->decimal("gross_yield", 16, 2)->default(0);

            $table->decimal("gross_income_m2", 16, 2)->default(0);

            $table->decimal("gross_income_manual", 16, 2)->default(0);

            $table->decimal("gross_yield_manual", 16, 2)->default(0);


            //charges

            $table->decimal("charges", 16, 2)->default(0);

            $table->decimal("charges_yield", 16, 2)->default(0);

            $table->decimal("charges_m2", 16, 2)->default(0);

            $table->decimal("charges_manual", 16, 2)->default(0);

            $table->decimal("charges_yield_manual", 16, 2)->default(0);



            //net income

            $table->decimal("net_income", 16, 2)->default(0);

            $table->decimal("net_yield", 16, 2)->default(0);

            $table->decimal("net_income_m2", 16, 2)->default(0);

            $table->decimal("net_income_manual", 16, 2)->default(0);

            $table->decimal("net_yield_manual", 16, 2)->default(0);


            //properitary field
            $table->string('proprietary', 200)->nullable();

            $table->text("price_log")->nullable();

            $table->string("intervation_log", 5000)->nullable()->defaut('[]');

            $table->string("contruction_date", 4)->nullable();

            //tax of gallery %
            $table->decimal("galleries_rent", 16, 2)->default(0);

            //private rental  %
            $table->decimal("private_rent", 16, 2)->default(0);

            //professional  rental %
            $table->decimal("professional_rent", 16, 2)->default(0);


            // Charges
            $table->decimal("annual_charges", 16, 2)->default(0);

            $table->string("annual_charges_details", 1000)->default('[]');

            $table->string("total_m2", 16, 2)->default(0);



            // Apartements
            $table->string("apartments")->default(0);
            $table->string("apartments_m2")->default(0);

            // Stores
            $table->string("stores")->default(0);
            $table->string("stores_m2")->default(0);

            // Offices
            $table->string("offices")->default(0);
            $table->string("offices_m2")->default(0);

            //Garages
            $table->string("garages")->default(0);

            // Car Places
            $table->string("car_places")->default(0);

            // Property Details
            $table->string("meta", 1000)->default('[]');

            //Construction date
            $table->string("construction_date")->nullable();


            // remarks
            $table->string("notes", 500)->nullable();

            // documents 
            $table->string("documents", 1000)->default('[]');

            // Shared Documents 
            $table->string("shared", 2000)->default('[]');

            // documents 
            $table->string("documents_status", 100)->default('[]');

            // pictures
            $table->string("images", 2000)->default('[]');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
