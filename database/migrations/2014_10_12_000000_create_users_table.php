<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('firstname');
            $table->string('lastname');
            $table->string('remember_token')->nullable();
            $table->string('password');
            $table->string('email');


            //api
            $table->string('api_token')->nullable();

            //status
            $table->tinyInteger('isActive')->default(0);

            //meta
            $table->text('meta')->nullable();

            //documents
            $table->text('documents')->nullable();


            $table->string('roles', 20)->default('[]');

            // company integer
            $table->bigInteger('company_id')->unsigned()->nullable();

            $table->timestamps();

            //add relation to compnay
            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
