<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->tinyInteger('status');
            $table->string('meta', 2000);

            $table->bigInteger('company_id')->unsigned()->nullable();
            $table->string('documents', 5000)->default('[]');

            $table->timestamps();

            //add relation to compnay
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
