<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(WordsTableSeeder::class);
        $this->call(PropertiesTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(CommunicationsTableSeeder::class);
    }
}
