<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return  [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'password' => bcrypt('secret'),
        'email' => $faker->email,
        'api_token' => str_random(8),
        'isActive' => $faker->boolean,
        'documents' => [],
        'roles' => [1],
        'meta' => [
            'title' => '',
            'gender' => 1,
            'birthdate' => '',
            'address' => [
                'country' => 'CH',
                'street' => '',
                'number' => '',
                'postcode' => '',
                'town' => '',
            ],
            'mobile' => '',
            'phone' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'position' => '',
            'company' => '',
            'bestTimeToCall' => [],
            'message' => ''
        ]
    ];
});
