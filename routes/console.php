<?php

use Carbon\Carbon;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('db:backup', function () {

    $today = Carbon::now();

    $filename = $today->toDateString() . '.sql';

    $backup_path = base_path() . "/storage/app/backups/{$filename}";


    $cmd = "mysqldump -u " . env('DB_USERNAME') . " -p" . env('DB_PASSWORD') . " -h" . env("DB_HOST") . " " . env('DB_DATABASE') . " > $backup_path";

    echo $cmd . "\n";
    exec($cmd, $output, $return_value);


    $sevenDaysAgo = $today->addDay(-7);
    $removePath = "backups/";

    //delete backup from 7 days ago local and on S3
    $sevenDaysAgoBackupPath = $backup_path . '/' . $sevenDaysAgo->toDateString() . ".sql";

    if (Storage::disk('backups')->exists($sevenDaysAgoBackupPath)) {
        echo "deleting previos backup : {$sevenDaysAgoBackupPath} \n";
        Storage::disk('backups')->delete($sevenDaysAgoBackupPath);
    }

    $sevenDaysAgoBackupPath = $removePath . $sevenDaysAgo->toDateString() . ".sql";

    if (Storage::disk('dropbox')->exists($sevenDaysAgoBackupPath)) {
        echo "deleting previos backup : {$sevenDaysAgoBackupPath} \n";
        Storage::disk('dropbox')->delete($sevenDaysAgoBackupPath);
    }

    $today->addDays(7);

    if (Storage::disk('backups')->exists($filename)) {

        $todayBackup = $removePath . $today->toDateString() . ".sql";

        $file = Storage::disk('backups')->get($filename);

        echo "saving new backup : {$todayBackup} \n";
        Storage::disk('dropbox')->put($todayBackup, $file);
    }
})->describe('Backup the database');
