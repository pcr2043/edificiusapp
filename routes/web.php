<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


DB::listen(function ($sql) {


  Log::info($sql->sql);
  Log::info($sql->bindings);
  Log::info($sql->time);
});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DefaultController@home')->name('home');
Route::get('search', 'DefaultController@search')->name('search');
Route::get('about', 'DefaultController@about')->name('about');
Route::get('services', 'DefaultController@services')->name('services');
Route::get('faq', 'DefaultController@faq')->name('faq');
Route::get('contact', 'DefaultController@contact')->name('contact');
Route::get('admin', 'DefaultController@admin')->name('admin');
Route::get('communicate', 'DefaultController@communicate')->name('communicate');
Route::get('profile', 'DefaultController@profile')->name('profile');

// LANGUAGE 
Route::get('language/{locale}', 'DefaultController@language')->name('language');

// USER CONTROLLER
Route::get('users/paginate', 'UserController@paginate');
Route::resource('users', 'UserController');
Route::resource('communications', 'CommunicationController');


// FAQ CONTROLLER
Route::get('faqs/paginate', 'FaqController@paginate');
Route::resource('faqs', 'FaqController');


// LERLHINL CONTROLLER
Route::get('words/paginate', 'WordController@paginate');
Route::resource('words', 'WordController');

// WORD CONTROLLER
Route::get('words/paginate', 'WordController@paginate');
Route::resource('words', 'WordController');

// AUTHCONTROLLER
Route::get('logout', 'AuthController@logout');
Route::get('login', 'AuthController@login')->name('login');
Route::post('login', 'AuthController@authLogin');
Route::post('register', 'UserController@register');
Route::post('contact', 'UserController@contact');

// STORAGECONTROLLER
Route::get('storage/{entity}/{field}/{id}/{index}', 'StorageController@storage');


// PROPERTY CONTROLLER
Route::get('properties/search', 'PropertyController@search');
Route::get('properties/paginate', 'PropertyController@paginate');
Route::resource('properties', 'PropertyController');


// FIRMS CONTROLLER
Route::get('firms/paginate', 'CompaniesController@paginate');
Route::resource('firms', 'CompaniesController');


// COMMUNICAITONS CONTROLLER
Route::get('communication/export', 'CommunicationController@export');
Route::get('communication/paginate', 'CommunicationController@paginate');
Route::resource('communication', 'CommunicationController');

// COMPANIES CONTROLLER
Route::get('companies/paginate', 'CompaniesController@paginate');
Route::resource('companies', 'CompaniesController');

// CONTACTS CONTROLLER
Route::get('contacts/paginate', 'ContactController@paginate');
Route::resource('contacts', 'ContactController');

// CAPTURE ALL ALIASES FOR PROPERTIES AND NOT
Route::get('{alias}', 'PropertyController@alias');
