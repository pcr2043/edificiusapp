/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
	// Define changes to default configuration here. For example:

	//config.language = 'gb';
	config.uiColor = '#e7e7e7';

	config.extraPlugins = 'maximize';

	config.extraPlugins = 'sharedspace';

	config.height = '100px';


	config.toolbar_basic = [
		{ name: 'paragraph', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
		{ name: 'styles', items: ['Font', 'FontSize'] },
		{ name: 'others', items: ['lineheight'] },
		{ name: 'basicstyles', items: ['TextColor', '-', 'Bold', 'Italic', 'Underline', 'BulletedList', 'Maximize'] }]


	config.toolbar_verybasic = [
		{ name: 'styles', items: ['Font', 'FontSize'] },
		{ name: 'basicstyles', items: ['TextColor', '-', 'Bold', 'Italic', 'Underline', 'BulletedList'] }]
};
