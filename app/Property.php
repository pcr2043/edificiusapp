<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Utils;
use Illuminate\Support\Carbon;

class Property extends Model
{

    public $guarded = ['id', 'selected', 'loading', 'documents', 'images', 'shared'];
    public $fillable = [];


    public $appends = ['selected', 'loading'];

    protected $casts = [
        'title' => 'array',
        'alias' => 'array',
        'description' => 'array',
        'notes' => 'array',
        'meta' => 'array',
        'documents' => 'array',
        'shared' => 'array',
        'images' => 'array',
        'price_log' => 'array',
        'intervation_log' => 'array',
        'annual_charges_details' => 'array'
    ];


    public function scopeTop($query, $take = 4)
    {
        return $query->where('isActive', 1)->where('isPublished', 1)->orderBy('created_at', 'desc')->take($take)->get();
    }



    public function getLoadingAttribute()
    {
        return false;
    }

    public function getSelectedAttribute()
    {
        return false;
    }

    public static function model()
    {
        $utils = new Utils();

        $property = new Property();
        $property->title = $utils->languageFieds();
        $property->alias = $utils->languageFieds();
        $property->description = $utils->languageFieds();
        $property->isActive = 1;
        $property->isPublished = 1;
        $property->isFeatured = 1;

        //building
        //store
        //office
        //house
        //garage

        $property->typeProperty = $utils->PropertyTypes()[0];
        $property->typeEnvironment =  $utils->EnvPlaces()[0];
        $property->typeOwner = $utils->TypeRents()[0];

        $year = Carbon::now()->year;
        // Location
        $property->meta = [
            'property' => [
                'year' => "{$year}",
                'apartments' => [
                    'income_perc' => '0',
                    'price_year' => '0',
                    'price_month' => '0',
                    'price_m2_year' => '0',
                    'price_m2_month' => '0'
                ],
                'offices' => [
                    'income_perc' => '0',
                    'price_year' => '0',
                    'price_month' => '0',
                    'price_m2_year' => '0',
                    'price_m2_month' => '0'
                ],
                'stores' => [
                    'income_perc' => '0',
                    'price_year' => '0',
                    'price_month' => '0',
                    'price_m2_year' => '0',
                    'price_m2_month' => '0'
                ]
            ],
            'proprietary' => ['name' => '', 'mobile' => '', 'email' => ''],
            'address' => [
                'country' => 'CH',
                'canton' => null,
                'street' => '',
                'number' => '',
                'postcode' => '',
                'town' => '',
            ],

        ];



        // Price Output and Revenue
        $property->price = '0';

        $property->gross_income = '0';
        $property->gross_yield = '0';
        $property->gross_income_m2 = '0';
        $property->gross_income_manual = '0';
        $property->gross_yield_manual = '0';

        // charges
        $property->charges = '0';
        $property->charges_yield = '0';
        $property->charges_m2 = '0';
        $property->charges_manual = '0';
        $property->charges_yield_manual = '0';

        // net income
        $property->net_income = '0';
        $property->net_yield = '0';
        $property->net_income_m2 = '0';
        $property->net_income_manual = '0';
        $property->net_yield_manual = '0';

        // rent
        $property->galleries_rent = '0';
        $property->private_rent = '0';
        $property->professional_rent = '0';
        $property->annual_charges = '0';
        $property->annual_charges_details = [];

        $property->apartments_m2 = '0';
        $property->stores_m2 = '0';
        $property->offices_m2 = '0';
        $property->total_m2 = '0';
        $property->apartments = '0';
        $property->stores = '0';
        $property->offices = '0';
        $property->garages = '0';
        $property->car_places = '0';


        // properitary field
        $property->proprietary = '';
        $property->price_log = [];
        $property->intervation_log = [];
        $property->contruction_date = null;


        //extra
        $property->notes = Utils::languageFieds();
        $property->documents = [];
        $property->shared = [];
        $property->images = [];

        return $property;
    }

    public function scopeAlias($query, $alias)
    {
        $locale =  app()->getLocale();
        $alias = urldecode($alias);
        $sql = "alias->\"$.{$locale}\" like '%{$alias}%'";

        return $query->whereRaw($sql)->get()->first();
    }
}
