<?php

namespace App\BLogic;

use App\Property;
use App\Helpers\Utils;
use App\Helpers\Response;
use App\Helpers\FileManager;
use Illuminate\Support\Facades\Storage;

class Properties
{
  protected $utils;

  public function __construct(Utils $utils)
  {
    $this->utils = $utils;
  }

  public function paginate()
  {

    $search = request()->search;
    $data = Property::where('title', 'like', "%{$search}%");

    $sorts = [];

    if (request()->has("sorts"))
      $sorts = json_decode(request()->sorts, true);

    foreach ($sorts as $key => $value) {
      if (strlen($value) === 0)
        continue;
      // $lang = app()->getLocale();;

      switch ($key) {

        case 'canton':
          $raw = 'meta->"$.address.canton" ' . $value;
          $data = $data->orderByRaw($raw);
          break;

        default:
          $data = $data->orderBy($key, $value);
          break;
      }
    }


    $paging = request()->has('paging')  ? intval(request()->get('paging')) : 20;

    return $data->paginate($paging);
  }


  public function search()
  {

    $search = request()->search;
    $data = Property::where('title', 'like', "%{$search}%");

    $params = json_decode(request()->params);

    // Price filter
    $data->where('price', '>=', $params->price->min)->where('price', '<=', $params->price->max);

    // Cantons filter
    if (count($params->cantons) >  0) {
      $canton = $params->cantons[0];

      $sql =  "meta->\"$.address.canton\" like  '%{$canton}%'";
      $data->whereRaw($sql);

      foreach ($params->cantons as $canton) {
        $sql = "meta->\"$.address.canton\" like  '%{$canton}%'";
        $data->orWhereRaw($sql);
      }
    }

    if (count($params->typeEnvironment) > 0) {
      $data->whereIn('typeEnvironment', $params->typeEnvironment);
    }

    if (count($params->typeOwner) > 0) {
      $data->whereIn('typeOwner', $params->typeOwner);
    }


    $sorts = [];

    if (request()->has("sorts"))
      $sorts = json_decode(request()->sorts, true);

    foreach ($sorts as $key => $value) {
      if (strlen($value) === 0)
        continue;
      // $lang = app()->getLocale();;

      switch ($key) {

        case 'canton':
          $raw = 'meta->"$.address.canton" ' . $value;
          $data = $data->orderByRaw($raw);
          break;

        case 'year':
          $raw = 'meta->"$.property.year" ' . $value;
          $data = $data->orderByRaw($raw);
          break;

        default:
          $data = $data->orderBy($key, $value);
          break;
      }
    }

    return $data;
  }

  public function save()
  {

    $language = $this->utils->language();

    $rules = [
      "title.{$language}" => 'required|min:1',
      "description.{$language}" => 'required|min:1',
      "price" => 'required',
      "meta.address.canton" => 'required',

    ];

    request()->validate($rules, [
      "title.{$language}.required" => "The Title field is required at least in {$language}",
      'meta.address.canton.required' => 'The Canton field is required'
    ]);


    try {

      $property = new Property();

      if (request()->has('id'))
        $property = Property::find(request()->id);

      $property->fill(request()->all());


      $property->documents = FileManager::saveField($property->documents, request()->get('documents'), "properties", "properties/{$property->id}/documents/");
      FileManager::saveField($remoteImages = [], request()->get('images'), "properties", "/properties/{$property->id}/documents/");
      $property->images = FileManager::saveField($property->images, request()->get('images'), "property-assets", "/{$property->id}/images/", true);
      $property->shared = FileManager::saveField($property->shared, request()->get('shared'), "properties", "properties/{$property->id}/shared/");

      $property->save();

      return  Response::success('Property saved with Success', $property);
    } catch (\Exception $ex) {

      return Response::error($ex->getMessage());
    }
  }
  public function destroy($ids)
  {
    try {
      $ids = explode(',', $ids);

      foreach ($ids as $id) {
        Storage::disk('properties')->deleteDirectory("properties/{$id}");
        Storage::disk('images')->deleteDirectory("/{$id}");
      }

      Property::destroy($ids);

      return Response::success(count($ids) > 1 ? 'Properties Deleted' : 'Property Deleted');
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }
}
