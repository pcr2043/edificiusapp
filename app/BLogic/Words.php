<?php

namespace App\BLogic;

use App\Word;
use App\Helpers\Utils;
use App\Helpers\Response;


class Words
{
  protected $utils;

  public function __construct(Utils $utils)
  {
    $this->utils = $utils;
  }


  public function paginate()
  {

    $params = json_decode(request()->params);

    $data = Word::where('reference', 'like', '%' . $params->search . '%')
      ->orWhere('meta', 'like', '%' . $params->search . '%');

    $sorts = [];
    if (request()->has("sorts"))
      $sorts = json_decode(request()->sorts, true);


    foreach ($sorts as $key => $value) {

      if (strlen($value) === 0)
        continue;

      // $lang = app()->getLocale();;

      switch ($key) {

        default: {

            $data = $data->orderBy($key, $value);
          }
          break;
      }
    }


    if (request()->has("paging"))
      return $data->paginate(request()->get("paging"));

    return $data->paginate();
  }

  public function save()
  {
    $language = $this->utils->language();

    $rules = [
      "meta.{$language}" => 'required'
    ];

    request()->validate($rules, [
      "meta.{$language}.required" => "This value field {$language} is required",
    ]);

    try {
      $word = new Word();

      if (request()->has('id'))
        $word = Word::find(request()->get('id'));

      $word->fill(request()->all());
      $word->save();

      return Response::success('Word Saved', $word);
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }


  public function destroy($ids)
  {
    try {
      $ids = explode(',', $ids);

      Word::destroy($ids);

      return Response::success(count($ids) > 1 ? 'Words Deleted' : 'Word Deleted');
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }
}
