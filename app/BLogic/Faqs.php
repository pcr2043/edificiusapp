<?php

namespace App\BLogic;

use App\Faq;
use App\Helpers\Utils;
use App\Helpers\Response;

class Faqs
{
  protected $utils;

  public function __construct(Utils $utils)
  {
    $this->utils = $utils;
  }


  public function paginate()
  {
    $data = Faq::where([['meta', 'like', '%' . request()->search . '%']]);

    $sorts = [];
    if (request()->has("sorts"))
      $sorts = json_decode(request()->sorts, true);


    foreach ($sorts as $key => $value) {

      if (strlen($value) === 0)
        continue;

      // $lang = app()->getLocale();;

      switch ($key) {

        case 'question':
          $raw = 'meta->"$.question" ' . $value;
          $data = $data->orderByRaw($raw);
          break;

        case 'awnser':
          $raw = 'meta->"$.awnser" ' . $value;
          $data = $data->orderByRaw($raw);
          break;

        default: {

            $data = $data->orderBy($key, $value);
          }
          break;
      }
    }


    if (request()->has("paging"))
      return $data->paginate(request()->get("paging"));

    return $data->paginate();
  }

  public function save()
  {
    $language = $this->utils->language();

    $rules = [
      "meta.{$language}.question" => 'required',
      "meta.{$language}.answer" => 'required'
    ];

    request()->validate($rules, [
      "meta.{$language}.question.required" => 'This question field  is required',
      "meta.{$language}.awnser.required" => 'The awnser field is required'
    ]);

    try {
      $faq = new Faq();

      if (request()->has('id'))
        $faq = Faq::find(request()->get('id'));

      $faq->fill(request()->all());
      $faq->save();

      return Response::success('Faq Saved', $faq);
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }


  public function destroy($ids)
  {
    try {
      $ids = explode(',', $ids);

      Faq::destroy($ids);

      return Response::success(count($ids) > 1 ? 'Faqs Deleted' : 'Faq Deleted');
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }
}
