<?php

namespace App\BLogic;

use App\Mail\Recover;
use App\Msg;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailPasswordRecover;
use App\Helpers\Response;

class Authorization
{

  public function login()
  {

    $validData = request()->validate([
      'email' => 'required',
      'password' => 'required'
    ]);


    if (Auth::attempt($validData, true)) {

      if (!auth()->user()->isActive) {
        auth()->logout();
        return Msg::error('Your Account is Inactive, please call us or send us a message to activate your account');
      }

      //default route
      $data = (request()->has('route') && strlen(request()->get('route')) > 0) ?  ['route' => request()->get('route')] : ['route' => ''];


      if (auth()->user()->isAdmin)
        $data = ['route' => 'admin'];

      return Response::success("Login Successfull", auth()->user());
    } else
      return Response::error('Wrong Credentials');
  }

  public function recover()
  {

    $validData = request()->validate(['email' => 'required|exists:users,email']);

    try {
      $user = User::where('email', $validData['email'])->first();

      Mail::to($user->email)->send(new MailPasswordRecover($user));

      return Msg::success('A email has been sent to your mailbox containing the instructions to change you password! <br> Thanks');
    } catch (\Exception $ex) {


      return Msg::error('Our Server is currently down, please try again later');
    }
  }

  public function recoverByToken($token)
  {

    auth()->logout();

    $user = User::where('api_token', $token)->first();

    if (isset($user)) {
      auth()->login($user, true);
      $user->setToken();
      return redirect()->route('home');
    } else
      return redirect()->route('feedback')->with("flash", "expired-token");
  }

  public function office($token)
  {
    auth()->logout();

    $user = User::where('api_token', $token)->first();

    if (isset($user)) {
      auth()->login($user, true);
      $user->setToken();
      return redirect()->route('admin');
    } else
      return redirect()->route('home')->with("flash", "toastr['error']('Expired Token, make a new request please.')");
  }


  public function logout()
  {
    auth()->logout();
    return redirect()->route('home');
  }


  public static function logoutS()
  {
    auth()->logout();
    return redirect()->route('home');
  }
}
