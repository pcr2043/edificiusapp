<?php

namespace App\BLogic;



use App\Mail\MailMessage;

use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\Activate;
use Illuminate\Support\Str;
use App\Helpers\FileManager;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Response;
use App\Mail\Register;

class Users
{


  public function paginate()
  {

    $params = json_decode(request()->params);

    $sql = "CONCAT(firstname,' ', lastname) like '%" . $params->search . "%'";

    $data = User::whereRaw($sql)->orWhere('email', 'like', "%{$params->search}%");

    $sorts = [];
    if (request()->has("sorts"))
      $sorts = json_decode(request()->sorts, true);


    foreach ($sorts as $key => $value) {

      if (strlen($value) === 0)
        continue;

      // $lang = app()->getLocale();;

      switch ($key) {

        case 'name':
          $sql = "CONCAT(firstname,' ', lastname) " . $value;
          $data = $data->orderByRaw($sql);
          break;


        default:
          $data = $data->orderBy($key, $value);
          break;
      }
    }


    if (request()->has("paging"))
      return $data->paginate(request()->get("paging"));

    return $data->paginate();
  }

  public function register()
  {

    $rules = [
      'firstname' => 'required',
      'lastname' => 'required',
      'email' => 'required|email|unique:users,email,' . request()->id . ',id',
      'meta.mobile' => 'required',
      'password' => 'required|confirmed|min:6',
      'meta.motivation' => 'required|min:20'
    ];

    request()->validate($rules, [
      'email.unique' => 'This user already exists in the Remote System',
      'meta.mobile.required' => 'The mobile field is required',
      'meta.motivation.required' => 'The message field is required'
    ]);

    try {

      $user = new User();
      $user->roles = [];
      $user->documents = [];
      $user->fill(request()->all());

      $meta = User::model()->meta;
      $meta['mobile'] = request()->get('meta')['mobile'];
      $meta['motivation'] = request()->get('meta')['motivation'];
      $meta['company'] = request()->get('meta')['company'];
      $meta['bestTimeToCall'] = request()->get('meta')['bestTimeToCall'];

      $user->meta = $meta;
      $password = Str::random(10);
      $user->password = bcrypt($password);
      $user->isActive = 0;
      $user->setAsClient();

      $user->setToken();
      $user->save();

      //send email to the the client
      Mail::to($user->email)->send(new Register($user));

      //send email to admins
      foreach (User::Admins() as $admin)
        Mail::to($admin->email)->send(new Register($user, $admin));

      return Response::success('Your registration was successfull, an email will sent to you containing the next steps.. Thanks', $user);
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }



  public function save()
  {

    $rules = [
      'firstname' => 'required',
      'lastname' => 'required',
      'email' => 'required|email|unique:users,email,' . request()->id,
      'roles' => 'array|min:1'
    ];

    // create a new instance of user here because we are going to use it to find the user and to update chat
    $user = new User();

    if (request()->id === null) {
      $rules['password'] = 'required|min:6|max:10';
      $user->setToken();
    } else
      $user = User::find(request()->id);

    // validate data before going to database commit
    request()->validate($rules);


    try {

      $user->fill(request()->all());

      // Set roles manually
      if (request()->has('roles')) {
        $roles = request()->get("roles");
        $user->roles = $roles;
      }

      if (request()->get('password') && strlen(request()->get('password')) > 0)
        $user->setPassword(request()->get('password'));

      // Save the User
      $user->save();


      //Set Documents
      $user->documents = FileManager::saveField($user->documents, request()->get('documents'), 'dropbox', "/users/{$user->id}/");

      //Ssve doments
      $user->save();

      //check if save cames from activation and activate user account
      if (request()->workflow == 'activate-client') {
        Mail::to($user->email)->send(new Activate($user));
        return Response::success('User Activated', $user);
      }

      return Response::success('User saved', $user);
    } catch (\Exception $ex) {

      return Response::saveError($ex->getMessage() . $ex->getLine() . $ex->getFile(), $ex->getTrace());
    }
  }

  public function destroy($ids)
  {
    try {
      $ids = explode(',', $ids);

      foreach ($ids as $id)
        Storage::disk('users')->deleteDirectory("users/{$id}");

      User::destroy($ids);


      return Response::success(count($ids) > 1 ? 'Users Deleted' : 'User Deleted');
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }

  public function insurances($year)
  {


    return Product::whereHas('documents', function ($query) use ($year) {

      $query->where('customer_id', '=', auth()->user()->id)->where('year', $year);
    })->with(['documents' => function ($query) use ($year) {
      $query->where('customer_id', '=', auth()->user()->id)->where('year', $year);
    }, 'documents.customer'])

      ->get();
  }


  public function sendMessage()
  {

    $rules = [
      'name' => 'required',
      'email' => 'required|email',
      'phone' => 'required',
      'message' => 'required|min:10|max:500',
      'time_call' => 'required'
    ];

    if (request()->has("property"))
      $rules['property'] = 'required|exists:properties,id';

    $validData = request()->validate($rules);

    try {

      // send the email to the customer or client on website
      // Mail::to($validData['email'])->send(new MailMessage($validData, false));
      $validData["admin"] = true;

      // send the message to the quotation mail
      //  Mail::to(env('MAIL_QUOTATION'))->send(new MailMessage($validData, true));

      return Msg::success('Message Sent');
    } catch (\Exception $ex) {
      return Msg::error($ex->getMessage());
    }
  }

  public function contact()
  {

    $rules = [
      'name' => 'required',
      'email' => 'required|email',
      'phone' => 'required',
      'message' => 'required|min:10|max:500',
      'bestTimeToCall' => 'required|array|min:1'
    ];

    if (request()->has("property"))
      $rules['property'] = 'required|exists:properties,id';

    $validData = request()->validate($rules);

    try {

      $validData = request()->all();
      // send the email to the customer or client on website
      Mail::to($validData['email'])->send(new MailMessage($validData, false));

      // Senc copy of the message to Admin
      $validData["admin"] = true;
      // send the message to the quotation mail
      Mail::to(env('MAIL_QUOTATION'))->send(new MailMessage($validData, true));

      return Response::success('Message Sent');
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }
}
