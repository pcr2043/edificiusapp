<?php

namespace App\BLogic;

use App\Contact;
use App\Helpers\Response;


class Contacts
{
  public function paginate()
  {
    $params = json_decode(request()->params);

    $sql = "CONCAT(firstname,' ', lastname) like '%" . $params->search . "%'";

    $data = Contact::with('Company')->whereRaw($sql)->orWhere('email', 'like', "%$params->search%");

    $sorts = [];
    if (request()->has("sorts"))
      $sorts = json_decode(request()->sorts, true);


    foreach ($sorts as $key => $value) {

      if (strlen($value) === 0)
        continue;

      // $lang = app()->getLocale();;

      switch ($key) {

        case 'phone':
          $raw = 'meta->"$.phone" ' . $value;
          $data = $data->orderByRaw($raw);
          break;


        default:
          $data = $data->orderBy($key, $value);
          break;
      }
    }


    if (request()->has("paging"))
      return $data->paginate(request()->get("paging"));

    return $data->paginate();
  }

  public function save()
  {


    $rules = [
      "firstname" => 'required',
      "lastname" => 'required',
      "email" => 'required|email',
      "meta.phone" => 'required',


    ];

    request()->validate($rules, [
      'meta.phone.required' => 'The phone field is required.'
    ]);

    try {
      $contact = new Contact();

      if (request()->has('id'))
        $contact = Contact::find(request()->get('id'));

      $contact->fill(request()->all());
      $contact->save();

      return Response::success('Contact Saved', $contact);
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }


  public function destroy($ids)
  {
    try {
      $ids = explode(',', $ids);

      Contact::destroy($ids);

      return Response::success(count($ids) > 1 ? 'Contacts Deleted' : 'Contact Deleted');
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }
}
