<?php

namespace App\BLogic;

use App\Communication;
use App\Helpers\Response;
use App\Helpers\FileManager;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PDF;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class Communications
{
  public function paginate()
  {
    $params = json_decode(request()->params);

    $data = Communication::with(['Employee', 'Company', 'Contact'])->where('resume', 'like', '%' . $params->search . '%');

    $sorts = [];
    if (request()->has("sorts"))
      $sorts = json_decode(request()->sorts, true);


    foreach ($sorts as $key => $value) {

      if (strlen($value) === 0)
        continue;

      // $lang = app()->getLocale();;

      switch ($key) {

          // case 'phone':
          //   $raw = 'meta->"$.phone" ' . $value;
          //   $data = $data->orderByRaw($raw);
          //   break;


        default:
          $data = $data->orderBy($key, $value);
          break;
      }
    }


    if (request()->has("paging"))
      return $data->paginate(request()->get("paging"));

    return $data->paginate();
  }

  public function save()
  {


    $rules = [
      "employee_id" => 'required|exists:users,id',
      "company_id" => 'exists:companies,id',
      "contact_id" => 'exists:contacts,id',
      "schedule_at" => 'required|date',
      "contact_type" => 'required',
      "resume" => 'required|max:100',
      "content" => 'required|max:2000',
    ];

    request()->validate($rules, [
      'meta.phone.required' => 'The phone field is required.'
    ]);

    try {
      $communication = new Communication();

      if (request()->has('id'))
        $communication = Communication::find(request()->get('id'));

      $communication->fill(request()->all());
      $communication->save();

      //Set Documents
      $communication->documents = FileManager::saveField($communication->documents, request()->get('documents'), 'communications', "/communications/{$communication->id}/");
      $communication->save();

      return Response::success('Communication Saved', Communication::with(['Employee', 'Company', 'Contact'])->find($communication->id));
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }


  public function destroy($ids)
  {
    try {

      $ids = explode(',', $ids);

      foreach ($ids as $id)
        Storage::disk('communications')->deleteDirectory("/communications/{$id}");

      Communication::destroy($ids);

      return Response::success(count($ids) > 1 ? 'Communications Deleted' : 'Communication Deleted');
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }

  public function export()
  {

    if (!request()->has('ids') || !request()->has('type'))
      return Response::error('invalid records');

    $ids = request()->get('ids');

    $data = Communication::with(['Employee', 'Company'])->whereIn('id', explode(',', $ids))->get();

    if (request()->get('type') === 'pdf') {

      // return view('communication.report')->with('data', $data)->render();
      $pdf = PDF::loadView('communication.report', ['data' => $data])->setPaper('a4', 'landscape')->setWarnings(false);

      $filename = 'communications-report-' . date('Y-m-d') . '.pdf';

      return $pdf->download($filename);
    } else
      return $this->excel($data);
  }

  public function excel($data)
  {

    $name =  'communications-report-' . date('Y-m-d') . '.pdf';

    $spreadsheet = new Spreadsheet();

    $spreadsheet->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

    // Set document properties
    $spreadsheet->getProperties()
      ->setTitle($name);

    $style = array(
      'alignment' => array(
        'horizontal' => Alignment::HORIZONTAL_CENTER,
        'vertical' => Alignment::VERTICAL_CENTER
      ),

    );

    $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'U', 'V', 'X', 'Y', 'X'];



    // ID
    $spreadsheet->getActiveSheet()->setCellValue('A1', 'ID');
    // USER
    $spreadsheet->getActiveSheet()->setCellValue('B1', 'DATE');
    // WHEN
    $spreadsheet->getActiveSheet()->setCellValue('C1', 'EMPLOYEE');
    // TYPE
    $spreadsheet->getActiveSheet()->setCellValue('D1', 'FIRM');
    // STATUS
    $spreadsheet->getActiveSheet()->setCellValue('E1', 'PERSON');
    // FLOW
    $spreadsheet->getActiveSheet()->setCellValue('F1', 'CONTACT TYPE');
    // NAME
    $spreadsheet->getActiveSheet()->setCellValue('G1', 'RESUME');
    // POSITION
    $spreadsheet->getActiveSheet()->setCellValue('H1', 'DESCRIPTION');

    $spreadsheet->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold(true);

    $spreadsheet->getActiveSheet()->getStyle("A1:H1")->applyFromArray($style);


    $sheet = $spreadsheet->getActiveSheet();
    foreach ($data as $index => $communication) {


      $rowIndex = $index + 2;



      $sheet->setCellValue('A' . $rowIndex, $communication->id);
      $sheet->setCellValue('B' . $rowIndex, $communication->schedule_at);
      $sheet->setCellValue('C' . $rowIndex, $communication->Employee->name);
      $sheet->setCellValue('D' . $rowIndex, $communication->Company->name);
      $sheet->setCellValue('E' . $rowIndex, $communication->Contact->name);
      $sheet->setCellValue('F' . $rowIndex, $communication->contact_type);
      $sheet->setCellValue('G' . $rowIndex, $communication->resume);
      $sheet->setCellValue('H' . $rowIndex, $communication->content);

      // $sheet->setCellValue('D' . $rowIndex, ($subscription->cleared) ? $this->utils->translate('yes') : $this->utils->translate('no'));
      // $sheet->setCellValue('E' . $rowIndex, ($subscription->active) ? $this->utils->translate('active') : $this->utils->translate('inactive'));

      $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(10);
      $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(50);
      $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
      $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(30);
      $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
      $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);
      $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(70);
      $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(100);




      // $spreadsheet->getActiveSheet()->getStyle("A" . $index)->applyFromArray($style);
      $spreadsheet->getDefaultStyle()->applyFromArray($style);
    }



    // Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $name . '.xlsx"');

    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0


    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');


    $writer->save('php://output');
  }
}
