<?php

namespace App\BLogic;

use App\Company;
use App\Helpers\Response;
use App\Contact;
use App\Helpers\FileManager;
use Illuminate\Support\Facades\Storage;

class Companies
{
  public function paginate()
  {
    $params = json_decode(request()->params);

    $data = Company::with('Contacts')->where('name', 'like', '%' . $params->search . '%');

    $sorts = [];
    if (request()->has("sorts"))
      $sorts = json_decode(request()->sorts, true);


    foreach ($sorts as $key => $value) {

      if (strlen($value) === 0)
        continue;

      // $lang = app()->getLocale();;

      switch ($key) {

        case 'open':
          $raw = 'meta->"$.isOpen" ' . $value;
          $data = $data->orderByRaw($raw);
          break;


        case 'postcode':
          $raw = 'meta->"$.address.postcode" ' . $value;
          $data = $data->orderByRaw($raw);
          break;

        case 'town':
          $raw = 'meta->"$.address.town" ' . $value;
          $data = $data->orderByRaw($raw);
          break;
        default: {

            $data = $data->orderBy($key, $value);
          }
          break;
      }
    }


    if (request()->has("paging"))
      return $data->paginate(request()->get("paging"));

    return $data->paginate();
  }

  public function save()
  {


    $rules = [
      "name" => 'required',
      "status" => 'required',
      "meta.address.street" => 'required',
      "meta.address.number" => 'required',
      "meta.address.postcode" => 'required',
      "meta.address.town" => 'required',

    ];

    request()->validate($rules, [
      "meta.address.street.required" => 'The Address field is required.',
      "meta.address.number.required" => 'The Number field is required.',
      "meta.address.postcode.required" => 'The Postcode field is required.',
      "meta.address.town.required" => 'The Town field is required.',
    ]);

    try {
      $company = new Company();

      if (request()->has('id'))
        $company = Company::find(request()->get('id'));

      $company->fill(request()->all());
      $company->save();

      //Set Documents
      $company->documents = FileManager::saveField($company->documents, request()->get('documents'), 'companies', "/companies/{$company->id}/");
      $company->save();


      if (request()->has('contactsToDetach'))
        $contacts = Contact::whereIn('id', request()->contactsToDetach)->update(['company_id' => null]);


      return Response::success('Company Saved', Company::with('Contacts')->find($company->id));
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }


  public function destroy($ids)
  {
    try {
      $ids = explode(',', $ids);

      foreach ($ids as $id) {
        Storage::disk('companies')->deleteDirectory("/companies/{$id}");
      }

      Company::destroy($ids);

      return Response::success(count($ids) > 1 ? 'Companies Deleted' : 'Company Deleted');
    } catch (\Exception $ex) {
      return Response::error($ex->getMessage());
    }
  }
}
