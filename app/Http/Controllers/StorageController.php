<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Storage;
use App\User;
use App\Company;
use App\Property;
use App\Communication;

class StorageController extends Controller
{
    public function storage($entity, $id, $field, $index)
    {
        $download = null;
        switch ($entity) {

            case 'users':

                $model = User::find($id);
                if ($model !== null && count($model[$field]) > $index)
                    $download  = $model[$field][$index];


                break;

            case 'properties':

                $model = Property::find($id);
                if ($model !== null && count($model[$field]) > $index)
                    $download  = $model[$field][$index];

                if ($field === 'images')
                    $entity = 'images';
                break;

            case 'communications':

                $model = Communication::find($id);
                if ($model !== null && count($model[$field]) > $index)
                    $download  = $model[$field][$index];


                break;

            case 'companies':

                $model = Company::find($id);
                if ($model !== null && count($model[$field]) > $index)
                    $download  = $model[$field][$index];


                break;
        }

        if ($download === null) {
            echo 'FILE NOT FOUND, PLEASE CONTACT THE ADMINISTRATOR';
            die();
        }


        $fileContent = Storage::disk($entity)->get($download['path']);
        $mimetype = Storage::disk($entity)->mimeType($download['path']);
        $size = Storage::disk($entity)->size($download['path']);


        $response = response($fileContent, 200, [
            'Content-Type' => $mimetype,
            'Content-Length' => $size,
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => "attachment; filename={$download['name']}",
            // 'Content-Transfer-Encoding' => 'binary',
        ]);

        return $response;
    }
}
