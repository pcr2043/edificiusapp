<?php

namespace App\Http\Controllers;

use App\Communication;
use Illuminate\Http\Request;
use App\BLogic\Communications;

class CommunicationController extends Controller
{
    protected $communications;

    public function __construct(Communications $communications)
    {
        $this->communications = $communications;
        $this->middleware('roles:admin|call-center');
    }

    /**
     * Paging all results.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        return $this->communications->export();
    }


    /**
     * Paging all results.
     *
     * @return \Illuminate\Http\Response
     */
    public function paginate()
    {
        return $this->communications->paginate();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('communication.index')->with('mode', 0)->with('page', 'communications');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  Communication::Model();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->communications->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Communication  $communication
     * @return \Illuminate\Http\Response
     */
    public function show(Communication $communication)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Communication  $communication
     * @return \Illuminate\Http\Response
     */
    public function edit(Communication $communication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Communication  $communication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Communication $communication)
    {
        return $this->communications->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Communication  $communication
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->communications->destroy($ids);
    }
}
