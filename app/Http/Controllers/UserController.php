<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\BLogic\Users;

class UserController extends Controller
{
    protected $users;

    public function __construct(Users $users)
    {
        $this->users = $users;
        $this->middleware('roles:admin|employee')->except(['register', 'contact']);
    }

    /**
     * Send Contact Form FronEnd to adminsitratior emailbbox
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {

        return $this->users->contact();
    }


    /**
     * Return users records using paginate funcionality
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        return $this->users->register();
    }

    /**
     * Return users records using paginate funcionality
     *
     * @return \Illuminate\Http\Response
     */
    public function paginate()
    {
        return $this->users->paginate();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index')->with('mode', 0)->with('page', 'users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return User::model();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->users->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // Not need for now, model is passed from client table
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        return $this->users->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->users->destroy($ids);
    }
}
