<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use App\BLogic\Companies;

class CompaniesController extends Controller
{

    protected $companies;

    public function __construct(Companies $companies)
    {
        $this->companies = $companies;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function paginate()
    {
        return $this->companies->paginate();
    }

    /**
     * Display Companies Index page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('companies.index')->with('mode', 0)->with('page', 'firms');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Company::Model();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->companies->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        return $this->companies->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->companies->destroy($ids);
    }
}
