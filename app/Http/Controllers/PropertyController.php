<?php

namespace App\Http\Controllers;

use App\BLogic\Properties;
use App\Property;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
    protected $properties;

    public function __construct(Properties $properties)
    {
        $this->properties = $properties;
    }

    public function alias($alias)
    {

        $property = Property::alias($alias);


        return view('search')->with('page', 'properties')->with('property', $property);
    }
    /**
     * Display all result using paging results
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        // dd(request()->query);
        $records =  $this->properties->search()->paginate(request()->paging);
        // $records->withPath('/');
        // $records->appends(['per_page' => request()->paging, 'page' => request()->page]);
        // return $records->get();
        return $records;
    }


    /**
     * Display all result using paging results
     *
     * @return \Illuminate\Http\Response
     */
    public function paginate()
    {
        return $this->properties->paginate();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('properties.index')->with('mode', 0)->with('page', 'properties');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Property::model();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->properties->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function edit(Property $property)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Property $property)
    {
        return $this->properties->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Property  $property
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->properties->destroy($ids);
    }
}
