<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;
use App\BLogic\Faqs;

class FaqController extends Controller
{

    protected $faq;

    public function __construct(Faqs $faqs)
    {
        $this->faqs = $faqs;
    }

    /** 
     * Display a listing of faqs results from db
     *
     * @return \Illuminate\Http\Response
     */

    public function paginate()
    {
        return $this->faqs->paginate();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(env('LANGUAGE_DEFAULT'));
        return view('faqs.index')->with('mode', 0)->with('page', 'faqs');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Faq::Model();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->faqs->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        return $this->faqs->save();
    }

    /**
     * Remove the specified resource from storage.
     *p
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        return $this->faqs->destroy($ids);
    }
}
