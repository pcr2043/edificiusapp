<?php

namespace App\Http\Controllers;

use App\BLogic\Authorization;

class AuthController extends Controller
{
    protected $auth;

    public function __construct(Authorization $auth)
    {

        $this->auth = $auth;
    }

    public function logout()
    {
        if (auth()->check())
            auth()->logout();

        return redirect()->home();
    }

    public function login()
    {
        if (auth()->check())
            return redirect()->route('home');

        return view('login')->with('page', 'login');
    }

    public function authLogin()
    {
        return $this->auth->login();
    }
}
