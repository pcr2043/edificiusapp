<?php

namespace App\Http\Controllers;

use App\Helpers\Utils;
use App\Faq;
use Illuminate\Support\Facades\Route;

class DefaultController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only(['profile', 'admin']);
    }

    public function language($locale)
    {
        session()->put('locale', $locale);
        return redirect()->back();
    }

    public function home()
    {
        // return view('maintenaince');
        return view('index')->with('page', 'home')->with('faqs', Faq::all());
    }

    public function search()
    {
        return view('search')->with('page', 'properties');
    }

    public function about()
    {
        return view('about')->with('page', 'about us');
    }


    public function profile()
    {
        return view('profile');
    }


    public function services()
    {
        return view('services')->with('page', 'services');
    }

    public function faq()
    {
        return view('faq')->with('page', 'faq')->with('faqs', Faq::all());
    }

    public function contact()
    {
        return view('contact')->with('page', 'contact');
    }

    public function admin()
    {
        return view('admin.index');
    }

    public function communicate()
    {
        return view('communicate.index');
    }
}
