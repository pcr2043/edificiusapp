<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helpers\Utils;

class PropertiesList extends Mailable
{
    use Queueable, SerializesModels;

    protected $subscriber;
    protected $properties;
    protected $documents;
    protected $help = true;

    /**
     * 
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subscriber = [], $properties = [], $documents = false, $help= true)
    {
        $this->subscriber = $subscriber;
        $this->properties = $properties;
        $this->documents = $documents;
        $this->help = $help;

        $quote = count($properties) . ' ' . Utils::getInstance()->translate('new.properties.found');

        $this->from(env('MAIL_FROM'), env('APP_NAME') . " - " . $quote);
        $this->to($subscriber->email);

        $this->subject(env('APP_NAME') . " - " . $quote);

       
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.properties.list')
            ->with('properties', $this->properties)
            ->with('subscriber', $this->subscriber)
            ->with('documents', $this->documents)
            ->with('help', $this->help);
    }
}
