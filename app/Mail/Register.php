<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class Register extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $admin;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $admin = null)
    {
        $this->user = $user;
        $this->admin = $admin;
        $title = 'Registration';

        if ($this->admin === null)
            $this->to($user->email);
        else
        {
            $title = 'New Registration';
            $this->to($admin->email);
        }

        $this->from(env('MAIL_FROM'), env('APP_NAME') . " - {$title}");
        $this->subject(env('APP_NAME') . "  - {$title}");
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.register')->with('user', $this->user)->with('admin', $this->admin);
    }
}
