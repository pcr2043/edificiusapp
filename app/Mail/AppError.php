<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppError extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $dump;
    private $message;


    public function __construct($message, $dump)
    {

        $this->message = $message;
        $this->dump = $dump;

        $this->from(env('MAIL_FROM'), env('APP_NAME') . " - ERROR - " . $this->message);

        $this->to(env('MAIL_DEVELOPER'));

        $this->subject(env('APP_NAME') . " - ERROR - " . $this->message);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.error')->with('message', $this->message)->with('dump', $this->dump);
    }
}
