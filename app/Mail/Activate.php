<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class Activate extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $password = null)
    {
        $this->user = $user;
        $this->password = $password;
        $title = 'ACCOUNT ACTIVATION CONFIMATION';
        $this->from(env('MAIL_FROM'), env('APP_NAME') . " - {$title}");
       //$this->to($user->email);
        $this->subject(env('APP_NAME') . "  - {$title}");
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.activate')->with('user', $this->user)->with('password', $this->password);
    }
}
