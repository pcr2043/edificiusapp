<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Property;

class MailMessage extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance. ..
     *
     * @return void
     */
    protected $data;
    protected $property = null;
    public function __construct($data)
    {
        if (isset($data['property'])) {

            $this->property = Property::find($data["property"]);

            $title = __('client.property.message') . ' ' . $this->property->title[app()->getLocale()];

            $this->from(env('MAIL_FROM'), env('APP_NAME') . " - " . $title);

            $this->subject(env('APP_NAME') . " - " . $title);
        } else {
            $this->from(env('MAIL_FROM'), env('APP_NAME') . " - NEW MESSAGE");

            $this->subject(env('APP_NAME') . " - NEW MESSAGE");
        }

        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $message = $this->markdown('emails.message')->with('data', $this->data);

        if ($this->property != null)
            $message = $message->with('property', $this->property);

        return $message;
    }
}
