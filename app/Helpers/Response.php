<?php

namespace App\Helpers;

class Response
{

  public static function now($result, $data)
  {
    header('Content-Type: application/json');
    echo json_encode(['result' => $result, 'data' => $data]);
    exit;
  }

  public static function success($msg, $model = null)
  {
    return json_encode(['result' => true, 'msg' => $msg, 'model' => $model]);
  }


  public static function error($msg, $details = null)
  {
    return json_encode(['result' => false, 'msg' => $msg, 'details' => $details]);
  }

  public static function saveError($msg, $details = null)
  {
    return response()->json(['result' => false, 'msg' => $msg, 'details' => $details], 412);
  }
}
