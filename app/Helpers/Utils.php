<?php

namespace App\Helpers;

use function GuzzleHttp\json_encode;
use App\Property;
use App\Word;
use Carbon\Carbon;

class Utils
{
    public static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance))
            self::$instance = new Utils();

        return self::$instance;
    }

    public function top($default = 4)
    {
        return Property::top($default);
    }

    public function authUser()
    {
        return auth()->check() ? json_encode(auth()->user()) : json_encode((object) []);
    }
    public function languageDefault()
    {
        $language = env('LANGUAGE_DEFAULT');

        return $language;
    }


    public function language()
    {
        $language = env('LANGUAGE_DEFAULT');

        return $language;
    }

    public function languages()
    {
        return  explode(",", env('LANGUAGES'));
    }

    public function year()
    {
        return  Carbon::now()->year;
    }

    public function FaqMetaField()
    {
        $field = [];
        foreach ($this->getBOLanguages() as $lang) {
            $field[$lang] = ['question' => '', 'awnser' => ''];
        }

        return $field;
    }

    public function WordMetaField()
    {
        $field = [];
        foreach ($this->getBOLanguages() as $lang) {
            $field[$lang] = '';
        }

        return $field;
    }

    public static function languageFieds()
    {
        $languages =  explode(",", env('LANGUAGES_BACKOFFICE'));
        $data = collect([]);

        foreach ($languages as $language) {
            $data[$language] = $language;
        }

        return $data;
    }
    public function getBOLanguages($json = false)
    {
        $languages = explode(",", env('LANGUAGES_BACKOFFICE'));
        return $json ?  json_encode($languages) : $languages;
    }


    //  Property Helpers

    public function PropertyTypes()
    {
        return ['Building', 'Shop', 'Store', 'Office', 'Garage'];
    }

    // Env. Place

    public function EnvPlaces()
    {
        return ['City', 'Suburbs', 'Country'];
    }

    // Type Rent


    public function TypeRents()
    {

        return ['Private', 'Commercial', 'Mixed'];
    }

    public function ListCantons($json = false)
    {
        $cantons =  [
            'all' => 'Tous le Cantons',
            'sr' => 'Suisse Rommande',
            'sa' => 'Suisse Alemanique',
            'si' => 'Suisse Italienne',
            'Suisse Rommande' =>
            [
                'fr' => 'Fribourg',
                'ge' => 'Genève',
                'ju' => 'Jura',
                'ne' => 'Neuchâtel',
                'vs' => 'Valais',
                'vd' => 'Vaud'

            ],


            'Suisse Alemanique' =>
            [
                'ar' => 'Appenzell RE',
                'ai' => 'Appenzell RI',
                'ag' => 'Argovie',
                'bl' => 'Bâle campagne',
                'bs' => 'Bâle ville',
                'be' => 'Berne',
                'gl' => 'Glaris',
                'gr' => 'Grisons',
                'lu' => 'Lucerne',
                'nw' => 'Nidwald',
                'ow' => 'Obwald',
                'sh' => 'Schaffhouse',
                'sz' => 'Schwyz',
                'so' => 'Soleure',
                'sg' => 'St. Gall',
                'tg' => 'Turgovie',
                'ur' => 'Uri',
                'zg' => 'Zoug',
                'zh' => 'Zurich'
            ],

            'Suisse Italienne' => [
                'ti' => 'Tessin'
            ]

        ];


        return ($json) ?  json_encode($cantons) : $cantons;
    }

    public function ContactTypes($json = false)
    {
        $types = ['email', 'phone', 'meeting', 'other'];

        return ($json) ? json_encode($types) : $types;
    }


    public function ListCantonsSimple($json = false)
    {
        $cantons =  [
            'fr' => 'Fribourg',
            'ge' => 'Genève',
            'ju' => 'Jura',
            'ne' => 'Neuchâtel',
            'vs' => 'Valais',
            'vd' => 'Vaud',
            'ar' => 'Appenzell RE',
            'ai' => 'Appenzell RI',
            'ag' => 'Argovie',
            'bl' => 'Bâle campagne',
            'bs' => 'Bâle ville',
            'be' => 'Berne',
            'gl' => 'Glaris',
            'gr' => 'Grisons',
            'lu' => 'Lucerne',
            'nw' => 'Nidwald',
            'ow' => 'Obwald',
            'sh' => 'Schaffhouse',
            'sz' => 'Schwyz',
            'so' => 'Soleure',
            'sg' => 'St. Gall',
            'tg' => 'Turgovie',
            'ur' => 'Uri',
            'zg' => 'Zoug',
            'zh' => 'Zurich',
            'ti' => 'Tessin',


        ];

        return ($json) ?  json_encode($cantons) : $cantons;
    }


    public function words()
    {
        if (session()->has("translations"))
            return session()->get("translations");

        return Word::all();
    }

    //translate
    public function translate($reference)
    {
        $reference = strtolower($reference);

        $words = $this->words();

        $word = $words->where('reference', $reference)->first();


        if (isset($word) && isset($word->meta[app()->getLocale()]))
            return $word->meta[app()->getLocale()];
        else if (isset($word) && isset($word->meta[env('LANGUAGE_DEFAULT')]))
            return $word->meta[env('LANGUAGE_DEFAULT')];


        return 'No Tranl. ' . $reference;
    }

    //translate
    public function placeholder($reference)
    {
        $reference = strtolower($reference);

        $words = $this->words();

        $word = $words->where('reference', $reference)->first();

        if (isset($word) && isset($word->placeholder[app()->getLocale()]))
            return $word->placeholder[app()->getLocale()];
        else if (isset($word) && isset($word->placeholder[env('LANGUAGE_DEFAULT')]))
            return $word->placeholder[env('LANGUAGE_DEFAULT')];

        return 'No Placeholder ' . $reference;
    }
}
