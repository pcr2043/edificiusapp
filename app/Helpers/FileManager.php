<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class FileManager
{

  public static function saveField($field, $files, $disk = 'local', $path = '/', $thumb = false)
  {

    $files = collect($files);
    $field = collect($field);



    $field->each(function ($file, $index) use ($files, $field, $disk) {
      if (isset($file['name']))
        if ($files->where('name', $file['name'])->first() == null) {
          Storage::disk($disk)->delete($file['path']);
          $field = $field->splice($index, 1);
        }
    });

    $files->each(function ($file, $index) use ($field, $path, $disk, $thumb) {

      if (isset($file['data'])) {
        $newFile = (new File($file))->save($path, $disk, $thumb, $field->count());
        $field->push($newFile);
      }
    });

    return $field;
  }
}
