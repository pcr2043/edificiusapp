<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $guarded = ['id', 'selected', 'loading', 'name', 'company', 'documents'];

    protected $fillable = [];

    protected $appends = ['selected', 'loading', 'name'];

    protected $casts = ['meta' => 'array', 'documents' => 'array'];


    public function getLoadingAttribute()
    {
        return false;
    }

    public function getSelectedAttribute()
    {
        return false;
    }

    public function getNameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function Company()
    {
        return $this->belongsTo(Company::class);
    }

    public static function Model()
    {

        $contact = new Contact();
        $contact->firstname = '';
        $contact->lastname = '';
        $contact->email = '';
        $contact->status = 1;
        $contact->meta = [
            'phone' => '',
            'phone_2' => '',
            'email' => '',
            'comments' => ''
        ];
        $contact->company_id = null;
        $contact->documents = [];


        return $contact;
    }
}
