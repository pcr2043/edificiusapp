<?php

namespace App\Providers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use League\Flysystem\Filesystem;
use Spatie\Dropbox\Client as DropboxClient;
use Spatie\FlysystemDropbox\DropboxAdapter;
use App\Helpers\Utils;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        Storage::extend('dropbox', function ($app, $config) {
            $client = new DropboxClient($config['accessToken']);
            return new Filesystem(new DropboxAdapter($client));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('cnt', function ($reference) {
            return "<?php echo \App\Helpers\Utils::getInstance()->translate($reference) ?>";
        });

        Blade::directive('ph', function ($reference) {
            return "<?php echo \App\Helpers\Utils::getInstance()->placeholder($reference) ?>";
        });


        View::share('utils', new Utils());
    }
}
