<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Communication extends Model
{
    protected $guarded = ['id', 'selected', 'loading', 'employee', 'company', 'contact', 'documents'];

    protected $fillable = [];

    protected $appends = ['selected', 'loading'];

    protected $casts = ['meta' => 'array', 'documents' => 'array'];




    public function getLoadingAttribute()
    {
        return false;
    }

    public function getSelectedAttribute()
    {
        return false;
    }

    public function Company()
    {
        return $this->belongsTo(Company::class);
    }

    public function Contact()
    {
        return $this->belongsTo(Contact::class);
    }


    public function Employee()
    {
        return $this->belongsTo(User::class, 'employee_id');
    }


    public static function Model()
    {


        $communication = new Communication();

        $communication->employee_id = auth()->user()->isCallCenter ?  auth()->user()->id : null;
        $communication->company_id = null;
        $communication->contact_id = null;
        $communication->schedule_at  = null;
        $communication->contact_type = null;
        $communication->resume = '';
        $communication->content = '';
        $communication->documents = [];

        return $communication;
    }
}
