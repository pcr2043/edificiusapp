<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = ['id', 'selected', 'loading', 'contacts', 'documents', 'contactsToDetach'];

    protected $fillable = [];

    protected $appends = ['selected', 'loading'];

    protected $casts = ['meta' => 'array', 'documents' => 'array'];

    public function getLoadingAttribute()
    {
        return false;
    }

    public function getSelectedAttribute()
    {
        return false;
    }

    public function Contacts()
    {
        return $this->hasMany(\App\Contact::class);
    }

    public static function Model()
    {
        $company = new Company();

        $company->name = '';
        $company->status = '';
        $company->isActive = 1;
        $company->documents = [];

        $company->meta = [
            'isOpen' => 0,
            'address' => [
                'country' => 'CH',
                'street' => '',
                'number' => '',
                'postcode' => '',
                'town' => '',
            ],
            'mobile' => '',
            'phone' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
        ];

        return $company;
    }
}
