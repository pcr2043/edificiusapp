<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Utils;

class Word extends Model
{
    protected $guarded = ['id', 'selected', 'loading'];

    protected $fillable = [];

    protected $appends = ['selected', 'loading'];

    protected $casts = ['meta' => 'array', 'placeholder' => 'array'];

    public function getLoadingAttribute()
    {
        return false;
    }

    public function getSelectedAttribute()
    {
        return false;
    }

    public static function Model()
    {
        $word = new Word();
        $word->reference = '';
        $word->description = '';
        $word->type = 1;
        $word->meta = (new Utils)->WordMetaField();
        $word->placeholder = (new Utils)->WordMetaField();
        return $word;
    }
}
