<?php

namespace App;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;
    // protected $connection = 'global';

    protected $guarded = ['id', 'name', 'loading', 'workflow', 'password_confirmation', 'roles', 'documents', 'selected', 'isAdmin', 'isEmployee', 'isCallCenter', 'isClient'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public $appends = ['name', 'selected', 'isAdmin', 'workflow', 'loading', 'isEmployee', 'isCallCenter', 'isClient'];

    protected $casts = [
        'meta' => 'array',
        'roles' => 'array',
        'documents' => 'array',
        'apps' => 'array'
    ];

    public function Company()
    {
        return $this->belongsTo(\App\Company::class);
    }



    public function getIsAdminAttribute($value)
    {
        return in_array(1, $this->roles);
    }
    public function getIsEmployeeAttribute($value)
    {
        return in_array(2, $this->roles);
    }

    public function getIsCallCenterAttribute($value)
    {
        return in_array(3, $this->roles);
    }

    public function getIsClientAttribute($value)
    {
        return in_array(4, $this->roles);
    }

    public function getworkflowAttribute($value)
    {
        return '';
    }

    public function getLoadingAttribute($value)
    {
        return 0;
    }


    public function getSelectedAttribute($value)
    {
        return 0;
    }

    public function getNameAttribute($value)
    {

        return $this->firstname . ' ' . $this->lastname;
    }

    public function recoverUrl()
    {
        return env('APP_URL') . '/recover/' . $this->api_token;
    }

    public function authUrl()
    {
        return env('APP_URL') . '/auth/' . $this->api_token;
    }

    public static function model()
    {

        $user = new User();
        $user->id = null;
        $user->firstname = '';
        $user->lastname = '';
        $user->email = '';
        $user->password = '';
        $user->isActive = false;
        $user->roles = [];
        $user->documents = [];
        $user->meta = [
            'title' => '',
            'gender' => null,
            'birthdate' => '',
            'address' => [
                'country' => 'CH',
                'street' => '',
                'number' => '',
                'postcode' => '',
                'town' => '',
            ],
            'mobile' => '',
            'phone' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'position' => '',
            'company' => '',
            'bestTimeToCall' => []
        ];

        return $user;
    }

    public function setPassword($password)
    {
        $this->password = Hash::make($password);
    }

    public function setToken()
    {
        $this->api_token = Str::random(10);
    }

    public function refreshToken()
    {
        $this->api_token = Str::random(10);
        $this->save();
    }

    public function scopeClients($query)
    {
        return $query->where('roles', 'like', '%4%')->where('isActive', 0)->get();
    }

    public function scopeCallCenter($query)
    {
        return $query->where('roles', 'like', '%3%')->where('isActive', 0)->get();
    }

    public function scopeByEmail($query, $email)
    {
        return $query->where('email', $email)->first();
    }



    public function hasRoles($roleNames)
    {

        $rolesExist =  array_filter($roleNames, function ($roleName) {
            return in_array($this->getRoleIndexByName($roleName), auth()->user()->roles) === true;
        });

        return $rolesExist;
    }

    public function scopeByRoles($query, $roles = ['customer'])
    {

        $roles = $this->parseRoles($roles);

        $sql = "(isActive =1 and JSON_CONTAINS(roles,'{$roles[0]}'))";

        foreach ($roles as $role) {

            $sql .= "or (isActive =1 and JSON_CONTAINS(roles,'{$role}'))";
        }

        return $query->whereRaw($sql)->get();
    }

    public function parseRoles($roles)
    {
        $rolesIndex = collect([]);
        foreach ($roles as $role)
            $rolesIndex->push($this->getRoleIndexByName($role));

        return $rolesIndex;
    }

    public function scopeAdmins($query)
    {
        return $query->whereIn('roles', [1])->get();
    }

    public function getRoleIndexByName($role)
    {
        switch ($role) {
            case 'admin':
                return 1;

            case 'employee':
                return 2;

            case 'call-center':
                return 3;

            case 'client':
                return 4;
        }
    }

    public function setAsClient()
    {
        $roles =  collect($this->roles);
        $roles->push(4);
        $this->roles = $roles;
    }
}
