<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Utils;

class Faq extends Model
{
    protected $guarded = ['id', 'selected', 'loading'];

    protected $fillable = [];

    protected $appends = ['selected', 'loading'];

    protected $casts = ['meta' => 'array'];

    public function getLoadingAttribute()
    {
        return false;
    }

    public function getSelectedAttribute()
    {
        return false;
    }

    public static function Model()
    {
        $faq = new Faq();
        $faq->meta = (new Utils)->FaqMetaField();
        return $faq;
    }
}
